<?php 

$view = JRequest::getVar('view');
$user = JFactory::getUser();
$app = JFactory::getApplication();

if (!$user->guest) {
    $app->redirect('index.php/mi-cuenta');
}

$layout = JRequest::getVar('layout');
$view = JRequest::getVar('view');
$component = JRequest::getVar('option');

$component = preg_replace('/[^\x{002D}\x{0030}-\x{0039}\x{0041}-\x{005A}\x{005F}\x{0061}-\x{007A}\x{00A1}-\x{FFFF}]/u', '', strtolower($component));
$view = preg_replace('/[^\x{002D}\x{0030}-\x{0039}\x{0041}-\x{005A}\x{005F}\x{0061}-\x{007A}\x{00A1}-\x{FFFF}]/u', '', strtolower($view));
$layout = preg_replace('/[^\x{002D}\x{0030}-\x{0039}\x{0041}-\x{005A}\x{005F}\x{0061}-\x{007A}\x{00A1}-\x{FFFF}]/u', '', strtolower($layout));


$class = $component.'-'.$view.'-'.$layout;


?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
    	<meta charset="utf-8" />
		<jdoc:include type="head" />

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Arimo:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,700italic,900italic' rel='stylesheet' type='text/css'>
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/reset.css">
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=home">



	</head>

    <body class="<?php echo $class; ?>">

        <header>
            <div class="wrapper">
                <div class="logo">
                    <a href="index.php">
                        <img src="images/logo.png">
                    </a>
                </div>
               
            </div>
        </header>

        <main>
            <div class="wrapper-all">
                <?php if ($view == 'featured'): ?>
                     <jdoc:include type="modules" name="login" style="xhtml"/>
                <?php else: ?>
                    <jdoc:include type="message"/>
                    <jdoc:include type="component"/>
                <?php endif ?>
            </div>
        </main>



        <div class="global-notification"></div>
        <!-- Overlay and modal box -->
        <div class="em-over-screen" id="over-screen"></div>
        <div class="em-modal-box">
            <div class="header">
                <div class="masc-header"></div>
                <i class="close-modal-button">x</i>
            </div>
            <div class="body">
                Hello modal box
            </div>
        </div>
        <script type="text/javascript">

            var url = "<?php echo JFactory::getURI()->root(); ?>";
        </script>
        <script type="text/javascript" src="js/load-scripts.php"></script>
	</body>
</html>
