<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
    	<meta charset="utf-8" />
		<jdoc:include type="head" />
        <meta name="viewport" content="width=device-width, initial-scale=0.1">

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Arimo:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,700italic,900italic' rel='stylesheet' type='text/css'>
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/reset.css">
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=home">


	</head>

    <body class="no-background">

        <main>
            <div class="wrapper-all">
                <nav>
                    <div class="logo">
                        <img src="images/menu/logo-menu.png">
                    </div>
                    <jdoc:include type="modules" name="menu" style="xhtml"/>
                    <jdoc:include type="modules" name="login" style="xhtml"/> 

                </nav>
                <div class="content-all">
                    <jdoc:include type="message"/>
                    <jdoc:include type="component"/>
                </div>
            </div>
        </main>

        <div class="global-notification"></div>
        <!-- Overlay and modal box -->
        <div class="em-over-screen" id="over-screen"></div>
        <div class="em-modal-box">
            <div class="header">
                <div class="masc-header"></div>
                <i class="close-modal-button">x</i>
            </div>
            <div class="body">
                Hello modal box
            </div>
        </div>
        <script type="text/javascript">

            var url = "<?php echo JFactory::getURI()->root(); ?>";
        </script>
        <script type="text/javascript" src="js/load-scripts.php"></script>
	</body>
</html>
