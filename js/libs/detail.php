<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;

jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_menu','tienda');

$estados = array(
	'P' => 'Pendiente',
	'U' => 'Confirmado por el comprador',
	'C' => 'Confirmado',
	'X' => 'Cancelado',
	'R' => 'Devolución',
	'S' => 'Enviado'
);

?>

<div id="pedido">

	<div class="head_detail">
		<div class="product_name">
			<h1>Mi cuenta - Información de pedido</h1>
			<div class="ruta">
				<span class="ruta_page"></span>	
			</div>
		</div>
	</div>

	<div class="content-tienda">
		<div class="content-detail">
			<table class="cabezote-pedido">
				<tr>
					<td>
						<!-- <p><a href="javascript:void window.open('../?option=com_virtuemart&view=orders&layout=details&tmpl=component&virtuemart_order_id=13', 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no');">Imprimir pedido</a></p> -->
						<p></p>
						<p>T-Shirt Lab S.A.S.</p>
						<p>Carrera 11 No. 92 - 10</p>
						<p>Bogotá - Colombia</p>
					</td>

					<td>
						<img src="images/logo.png">
					</td>
				</tr>
			</table>
			 
		</div>
		<div class="pagos-online">
			<img src="images/dimension_estampados/logo-pago-1.png">
		</div>

		<div class="detail-pedido">
			<h2>Información pedido</h2>

			<table>
				<tr>
					<td>Número de pedido:</td>
					<td><?= $this->orden->order_number ?></td>
				</tr>

				<tr>
					<td>Fecha:</td>
					<td><?= Misc::formatDateSpanish($this->orden->created_on) ?></td>
				</tr>

				<tr>
					<td>Estado:</td>
					<td><?= $estados[$this->orden->order_status] ?></td>
				</tr>
			</table>

			<h2>Información del cliente</h2>

			<div class="info-cliente">
				<div class="facturacion">
					<h2>Emitir comprobante de pago a</h2>

					<table>
						<tr>
							<td>Departamento:</td>
							<td>
							<?php 
								$departamento = TiendaHelper::getNameCityByName($this->facturacion->city);

								echo $departamento->departamento; 
							?>
							</td>
						</tr>
						<tr>
							<td>Email:</td>
							<td>
								<?php 

									$user = TiendaHelper::getUserInfo($this->facturacion->virtuemart_user_id);

									echo $user->email;
								?>
							</td>
						</tr>
						<tr>
							<td>Primer nombre / Empresa:</td>
							<td><?= $user->name ?></td>
						</tr>

						<tr>
							<td>Apellidos / Contacto:</td>
							<?php 
							if ($user->tipo == '1'): ?>
								<td><?= $user->contacto ?></td>
							<?php else: ?>
								<td><?= $user->apellidos ?></td>
							<?php endif ?>
							
						</tr>

						<tr>
							<td>Cédula o NIT:</td>
							<td><?= $user->identificacion ?></td>
						</tr>

						<tr>
							<td>Dirección principal:</td>
							<td><?= $this->facturacion->address_1 ?></td>
						</tr>

						<tr>
							<td>Ciudad:</td>
							<td><?= $this->facturacion->city ?></td>
						</tr>

						<tr>
							<td>País:</td>
							<td>Colombia</td>
						</tr>

						<tr>
							<td>Teléfono:</td>
							<td><?= $this->facturacion->phone_1 ?></td>
						</tr>

						<tr>
							<td>Celular:</td>
							<td><?= $this->facturacion->phone_2 ?></td>
						</tr>
					</table>
				</div>

				<div class="envio">
					<h2>Enviar a</h2>

					<table>
						<tr>
							<td>Primer nombre / Empresa:</td>
							<td><?= $this->envio->first_name ?></td>
						</tr>

						<tr>
							<td>Dirección principal:</td>
							<td><?= $this->envio->address_1 ?></td>
						</tr>

						<tr>
							<td>Ciudad:</td>
							<td><?= $this->envio->city ?></td>
						</tr>

						<tr>
							<td>País:</td>
							<td>Colombia</td>
						</tr>

						<tr>
							<td>Teléfono:</td>
							<td><?= $this->envio->phone_1 ?></td>
						</tr>

						<tr>
							<td>Celular:</td>
							<td><?= $this->envio->phone_2 ?></td>
						</tr>
					</table>
				</div>

			</div>
			
			<div class="info-envio">
				<h2>Información del envío</h2>

				<table>
					<tr>
						<td>Transporte</td>
						<td>Modo de envío</td>
						<td>Precio</td>
					</tr>
					<tr>
						<td><?= $this->metodo->shipment_name ?></td>
						<td><?= $this->metodo->shipment_desc ?></td>
						<td>$0</td>
					</tr>
				</table>

			</div>


			<div class="info-productos">
				<h2>Artículos</h2>

				<table>
					<tr>
						<td>Ctd.</td>
						<td>SKU</td>
						<td>Nombre</td>
						<td>Precio</td>
						<td>Total</td>
					</tr>
					<?php 
					$totalPedido = 0;
					foreach ($this->items as $key => $item) {
					?>
						<tr>
							<td><?= $item->product_quantity ?></td>
							<td><?= $item->order_item_sku ?></td>
							<td><?= $item->product_name ?></td>

							<td><?= '$'.Misc::numberDots($item->product_item_price) ?></td>
							<td>
								<?php 
									$total = (int)$item->product_quantity * $item->product_item_price; 
									$totalPedido += $total;

									$total = Misc::numberDots($total);

									$total = '$'.$total;

									echo $total;
								?>
							</td>
						</tr>

					<?php					
					} ?>
				</table>
			</div>

			<div class="totales">

				<table>
					<tr>
						<td>Subtotal:</td>
						<td><?= '$'.Misc::numberDots($totalPedido) ?></td>
					</tr>
					<tr>
						<td>IVA (16%):</td>
						<td>
							<?php 
								$iva = $totalPedido * 0.16;

								$totalPedido = $totalPedido + $iva;

								$totalPedido = '$'.$totalPedido;

								$iva = '$'.Misc::numberDots($iva);

								echo $iva;
							?>

						</td>
					</tr>
					<tr>
						<td>Envío:</td>
						<td>$0</td>
					</tr>
				</table>

				<hr>

				<table>
					<tr>
						<td>Total:</td>
						<td><?= '$'.Misc::numberDots($this->orden->order_total) ?></td>
					</tr>
				</table>
			</div>



		</div>

	</div>

	<a href="javascript:history.back()" class="back-button">Volver</a>

</div>	