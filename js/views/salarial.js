/**
* Estados de Resultados Historicos Extendido View
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información de los resultados históricos extendidos
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var SalarialView = {};

	// Extends my object from Backbone events
	SalarialView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'submit .calculadora-salarial': 'downloadPDFSalarial'
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'downloadPDFSalarial'
				);

				this.empleados = new EmpleadosModel();

			}

		,	downloadPDFSalarial: function(e){

				e.preventDefault();

				var data = utilities.formToJson( '.calculadora-salarial' )
				,	required = [
					'sueldo',
					'otros_ingresos',
					'otros_ingresos_no'
				]
				,	errors = [];

				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente.', 1000 );

				if (!utilities.justNumbers(data.sueldo))
					return utilities.showNotification('error', 'El sueldo debe ser un número', 2000);

				if (!utilities.justNumbers(data.otros_ingresos))
					return utilities.showNotification('error', 'Otros Ingresos Salariales debe ser un número', 2000);

				if (!utilities.justNumbers(data.otros_ingresos))
					return utilities.showNotification('error', 'Otros Ingresos No Salariales debe ser un número', 2000);

				this.empleados.generatePDFSalarial(data, function(response){

					window.open(url+'/index.php/?option=com_nomina&task=empleados.downloadFile&filename='+response.filename);

					return utilities.showNotification( 'error', 'Descargando el archivo...', 1000 );

				}, this.onError);

			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.SalarialView = new SalarialView();

})( jQuery, this, this.document, this.Misc, undefined );