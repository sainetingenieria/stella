/**
* Estados de Resultados Historicos Extendido View
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información de los resultados históricos extendidos
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var RetencionesView = {};

	// Extends my object from Backbone events
	RetencionesView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click #retenciones-form .btn-primary': 'verifyNit',
				'click #back-button': 'showForm'
				
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'verifyNit',
					'showForm'
				);

				this.retenciones = new RetencionesModel();

			}

		,	showForm: function(e){

				e.preventDefault();

				$('#retenciones-form').show('slow', function() {});
				$('.page-header h2').show('slow', function() {});
				$('.message .error').hide('slow', function() {});
				$('.message .success').hide('slow', function() {});
				$('#back-button').hide('slow', function() {});

				$('.wrapper-retefuente').css('padding-top', '0');


			}


		,	verifyNit: function(e){

				e.preventDefault();

				var data = utilities.formToJson( '#retenciones-form' )
				,	required = ['nit']
				,	errors = [];

				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Digite su Nit.', 400 );

				if (!utilities.justNumbers(data.nit))
					return utilities.showNotification('error', 'El nit debe ser un número', 2000);

				this.retenciones.verifyNit( data, function(response){

					$('.wrapper-retefuente').css('padding-top', '18px');

					if (response.status == 500){

						var html = '<p>No hemos encontrado el Nit <span class="text-y">'+data.nit+'</span>, si crees que existe algún error comunicate con nosotros.<br><br>Teléfono: <span class="text-y">+57 1 611 0675</span></p>';
						$('.message .error').html('');
						$('.message .error').append(html);
						$('#retenciones-form')[0].reset();
						$('#retenciones-form').hide('slow', function() {});
						$('.page-header h2').hide('slow', function() {});
						$('.message .error').show('slow', function() {});
						$('#back-button').show('slow', function() {});
						return;

					}

					var html = new EJS( { url: url + 'js/templates/retenciones.ejs' } ).render(response);
					$('.message .success').html('');
					$('.message .success').append(html);
					$('#retenciones-form')[0].reset();
					$('#retenciones-form').hide('slow', function() {});
					$('.page-header h2').hide('slow', function() {});
					$('.message .success').show('slow', function() {});
					$('#back-button').show('slow', function() {});

				}, this.onError );



			}	

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.RetencionesView = new RetencionesView();

})( jQuery, this, this.document, this.Misc, undefined );