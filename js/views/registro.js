/**
* Estados de Resultados Historicos Extendido View
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información de los resultados históricos extendidos
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var RegistroView = {};

	// Extends my object from Backbone events
	RegistroView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click #member-registration .btn-primary': 'verifyNit',
				'focusout #jform_username': 'verifyUser',
				'focusout #jform_password1': 'verifyPassword',
				'click .tooltip-password': 'closeTooltip'
				
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'verifyNit',
					'verifyUser',
					'verifyPassword',
					'closeTooltip'
				);

				this.registro = new RegistroModel();

				if ($('.alert-message').length){
					$('.login fieldset').addClass('animated shake');
					$('.login button[type="submit"]').addClass('animated shake');

					if ($('.login fieldset').hasClass('animated')){
						$('.alert-message').css('display', 'none');
					}

					var html = '<div class="tooltip-password">'+$('.alert-message').html()+'</div>';

					$('.reset fieldset #jform_email').parent().append(html);

					$('.tooltip-password').show('slow', function() {});

					if ($('.tooltip-password').length){
						$('.alert-message').css('display', 'none');
					}

				}

				utilities.closeOnClickOut('.com_users-reset-edit .tooltip-password');

			}
		,	closeTooltip: function(e){

				e.preventDefault();

				$('.tooltip-password').hide('slow', function() {});

			}

		,	verifyUser: function(e){

				e.preventDefault();

				if ($('#jform_username').val().length <= 4 || /^[a-zA-Z0-9- ]*$/.test($('#jform_username').val()) == false || $('#jform_username').val().indexOf(' ') != -1){
					
					if (!$('.registration fieldset #jform_username').parent().find('.tooltip-password').length){

						console.log('hola1');

						var html = '<div class="tooltip-password">Usuario no valido. Ingresa un usuario sin espacios al principio o al final, por lo menos con 4 caracteres y que no contenga los siguientes caracteres: < > / "  % ; ( ) &</div>';
						$('.registration fieldset #jform_username').parent().append(html);
					}
					return $('.registration fieldset #jform_username').parent().find('.tooltip-password').show('slow', function() {});
				}else{
					return $('.registration fieldset #jform_username').parent().find('.tooltip-password').hide('slow', function() {});

				}

			}

		,	verifyPassword: function(e){

				e.preventDefault();

				if ($('#jform_password1').val().length < 6){
					var html = '<div class="tooltip-password">La contraseña debe tener mínimo 6 caracteres</div>';
					$('.registration fieldset #jform_password1').parent().append(html);
					return $('.registration fieldset #jform_password1').parent().find('.tooltip-password').show('slow', function() {});

				}
				else{
					$('.tooltip-password').hide('slow', function() {});

				}

			}

		,	verifyNit: function(e){

				e.preventDefault();

				var data = utilities.formToJson( '#member-registration' )
				,	errors = [];

				if ($('#jform_username').val().length <= 4 || /^[a-zA-Z0-9- ]*$/.test($('#jform_username').val()) == false || $('#jform_username').val().indexOf(' ') != -1){
					if (!$('.registration fieldset #jform_username').parent().find('.tooltip-password').length){
						var html = '<div class="tooltip-password">Usuario no valido. Ingresa un usuario sin espacios al principio o al final, por lo menos con 4 caracteres y que no contenga los siguientes caracteres: < > / "  % ; ( ) &</div>';
						$('.registration fieldset #jform_username').parent().append(html);
					}
					return $('.registration fieldset #jform_username').parent().find('.tooltip-password').show('slow', function() {});
				}


				if ($('#jform_password1').val().length < 6){
					var html = '<div class="tooltip-password">La contraseña debe tener mínimo 6 caracteres</div>';
					$('.registration fieldset #jform_password1').parent().append(html);
					return $('.registration fieldset #jform_password1').parent().find('.tooltip-password').show('slow', function() {});

				}


				this.registro.verifyNit( data, function(response){

					if (response.status == 500){
						return utilities.showNotification( 'error', 'El nit no existe', 2000);
					}else{
						$( '#member-registration' ).submit();

					}
				}, this.onError );



			}	

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.RegistroView = new RegistroView();

})( jQuery, this, this.document, this.Misc, undefined );