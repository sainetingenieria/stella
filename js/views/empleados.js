/**
* Estados de Resultados Historicos Extendido View
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información de los resultados históricos extendidos
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var EmpleadosView = {};

	// Extends my object from Backbone events
	EmpleadosView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click .show-info': 'showInfo',
				'click .hide-info': 'hideInfo',
				'click .show-info-normal': 'showInfoNormal',
				'click .carpeta a' : 'showSubCarpetas',
				'click #volver-button': 'showPrevious',
				'click #actualizar': 'refreshFolders',
				'click .documento a': 'getFile',
				'click .download-pdf-liquidacion a': 'downloadPDFLiquidacion'


			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'showInfo',
					'hideInfo',
					'showInfoNormal',
					'showSubCarpetas',
					'refreshFolders',
					'getFile',
					'showPrevious',
					'downloadPDFLiquidacion'
				);

				this.empleados = new EmpleadosModel();

				$('.selected').parent().parent().addClass('selected-organize');

				this.contador = 0;


			}

		,	getFile: function(e){

				e.preventDefault();

				var target = e.currentTarget;

				var codigo = $(target).data('codigo');

				this.empleados.getFile(codigo, function(response){

					window.open(url+'/index.php/?option=com_nomina&task=empleados.downloadFile&filename='+response.filename);

					return utilities.showNotification( 'error', 'Descargando el archivo...', 1000 );


				},this.onError);
			}

		,	refreshFolders: function(e){

				e.preventDefault();

				var target = e.currentTarget;

				var empleado = $(target).data('empleado');

				this.empleados.refreshFolders(empleado, function(response){

					var html = new EJS( { url: url + 'js/templates/files.ejs' } ).render(response);

					$('#volver-button').fadeOut('slow');

					$('.list-documentos ul li').remove();

					$('.list-documentos ul').addClass('animated lightSpeedIn');
					$('.list-documentos ul').append(html);

					$('.list-documentos ul').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
						$('.list-documentos ul').removeClass('animated');
						$('.list-documentos ul').removeClass('lightSpeedIn');
					});


				},this.onError);

			}

		,	showPrevious: function(e){

				e.preventDefault();

				var codigo = $('#codigo').val();

				var empleado = $('#empleado').val();

				var _this = this;

				this.empleados.getSubCarpetas(codigo, empleado, function(response){

					var html = new EJS( { url: url + 'js/templates/files.ejs' } ).render(response);

					$('#codigo').val(response.codigo);
					$('#empleado').val(response.empleado);

					if (response.padre){
						$('#volver-button').fadeOut('slow');
					}else{
						$('#volver-button').fadeIn('slow');
					}
			
					
					$('.list-documentos ul li').remove();

					$('.list-documentos ul').addClass('animated lightSpeedIn');
					$('.list-documentos ul').append(html);

					$('.list-documentos ul').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
						$('.list-documentos ul').removeClass('animated');
						$('.list-documentos ul').removeClass('lightSpeedIn');
					});



				},this.onError);

			}


		,	showSubCarpetas: function(e){

				e.preventDefault();

				var target = e.currentTarget;

				var codigo = $(target).data('codigo');

				var empleado = $(target).data('empleado');


				this.empleados.getSubCarpetas(codigo, empleado, function(response){

					var html = new EJS( { url: url + 'js/templates/files.ejs' } ).render(response);

					$('#codigo').val(response.codigo);
					$('#empleado').val(response.empleado);
					$('#volver-button').fadeIn('slow');

					$('.list-documentos ul li').remove();

					$('.list-documentos ul').addClass('animated lightSpeedIn');
					$('.list-documentos ul').append(html);

					$('.list-documentos ul').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
						$('.list-documentos ul').removeClass('animated');
						$('.list-documentos ul').removeClass('lightSpeedIn');
					});


				},this.onError);

			}


		,	showInfo: function(e){

				e.preventDefault();

				var target = e.currentTarget;

				this.empleados.getEmpleados($(target).data('id'), function(response){

					$('.empleado-info-tr').hide('slow');
					$('.hide-info').removeClass('background-amarillo');
					$('tr').removeClass('hide-info');


					var html = new EJS( { url: url + 'js/templates/empleados.ejs' } ).render(response);

					$(target).after(html);

					var selector = ".wrapper-info"+ $(target).data('id');
					$(selector).show('slow');


					$( "#accordion"+ $(target).data('id') ).accordion( {heightStyle: 'content'} );
					$( "#tabs"+ $(target).data('id') ).tabs();


					$('.expanded').addClass('not-expanded');
					$('.not-expanded').removeClass('expanded');

					$('.not-expanded', target).addClass('expanded');
					$('.expanded', target).removeClass('not-expanded');


					$(target).removeClass('show-info');
					$(target).addClass('hide-info');
					$(target).addClass('background-amarillo');
					
				}, this.onError);




			}

		,	hideInfo: function(e){

				e.preventDefault();

				var target = e.currentTarget;

				var selector = ".empleado-info-tr"+ $(target).data('id');


				$(selector).hide('slow');

				$(target).removeClass('hide-info');
				$(target).removeClass('background-amarillo');
				$(target).addClass('show-info-normal');
				$('.expanded',target).addClass('not-expanded');
				$('.not-expanded',target).removeClass('expanded');

			}	

		,	showInfoNormal: function(e){

				e.preventDefault();

				var target = e.currentTarget;
				$('.hide-info').addClass('show-info-normal');


				$('tr').removeClass('hide-info');
				$('.empleado-info-tr').hide('slow');


				var selector = ".empleado-info-tr"+ $(target).data('id');
				$(selector).show('slow');

				$('tr').removeClass('background-amarillo');

				$('.expanded').addClass('not-expanded');
				$('.not-expanded').removeClass('expanded');

				$('.not-expanded', target).addClass('expanded');
				$('.expanded', target).removeClass('not-expanded');


				$(target).addClass('hide-info');
				$(target).removeClass('show-info-normal');
				$(target).addClass('background-amarillo');

			}

		,	downloadPDFLiquidacion: function(e){

				e.preventDefault();

				var target = e.currentTarget;

				var id = $(target).data('id');


				this.empleados.downloadPDFLiquidacion(id, function(response){

					window.open(url+'/index.php/?option=com_nomina&task=empleados.downloadFile&filename='+response.filename);

					return utilities.showNotification( 'error', 'Descargando el archivo...', 1000 );


				}, this.onError);



			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.EmpleadosView = new EmpleadosView();

})( jQuery, this, this.document, this.Misc, undefined );