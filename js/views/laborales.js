/**
* Estados de Resultados Historicos Extendido View
* version : 1.0
* package: latianalisis.frontend
* package: latianalisis.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
* Controla la interactividad de la información de los resultados históricos extendidos
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var LaboralesView = {};

	// Extends my object from Backbone events
	LaboralesView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click #laboral-form .btn-primary': 'verifyCedula',
				'click #back-button': 'showForm'
				
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'verifyCedula',
					'showForm'
				);

				this.laborales = new LaboralesModel();

			}

		,	showForm: function(e){

				e.preventDefault();

				$('#laboral-form').show('slow', function() {});
				$('.page-header h2').show('slow', function() {});
				$('.message .error').hide('slow', function() {});
				$('.message .success').hide('slow', function() {});
				$('#back-button').hide('slow', function() {});

				$('.wrapper-retefuente').css('padding-top', '0');


			}


		,	verifyCedula: function(e){

				e.preventDefault();

				var data = utilities.formToJson( '#laboral-form' )
				,	required = ['cedula']
				,	errors = [];

				utilities.validateEmptyFields( required, data, errors );

				$('.global-notification').css({
					'top': '22%',
					'left': '73%'
				});

				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Digite su cedula.', 400 );

				if (!utilities.justNumbers(data.cedula))
					return utilities.showNotification('error', 'La cedula debe ser un número', 2000);

				this.laborales.verifyCedula( data, function(response){


					$('.wrapper-retefuente').css('padding-top', '30px');

					if (response.status == 500){

						var html = '<p>No hemos encontrado la cedula <span class="text-y">'+data.cedula+'</span>, si crees que existe algún error comunicate con nosotros.<br><br>Teléfono: <span class="text-y">+57 1 611 0675</span></p>';
						$('.message .error').html('');
						$('.message .error').append(html);
						$('#laboral-form')[0].reset();
						$('#laboral-form').hide('slow', function() {});
						$('.page-header h2').hide('slow', function() {});
						$('.message .error').show('slow', function() {});
						$('#back-button').show('slow', function() {});
						return;

					}

					var html = new EJS( { url: url + 'js/templates/laboral.ejs' } ).render(response);
					$('.message .success').html('');
					$('.message .success').append(html);
					$('#laboral-form')[0].reset();
					$('#laboral-form').hide('slow', function() {});
					$('.page-header h2').hide('slow', function() {});
					$('.message .success').show('slow', function() {});
					$('#back-button').show('slow', function() {});

				}, this.onError );



			}	

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.LaboralesView = new LaboralesView();

})( jQuery, this, this.document, this.Misc, undefined );