/**
* Sample Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	EmpleadosModel = Backbone.Model.extend({
	        defaults: {

	        }

	    ,   initialize: function(){

	            
	        }

	        /**
	        * Gets the event from id
	        *
	        */
	    ,	getEmpleados: function( id, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_nomina'
					,	task: 'empleados.getEmpleados'
					,	id: id

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	getSubCarpetas: function( id, empleado, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_nomina'
					,	task: 'empleados.getSubCarpetas'
					,	id: id
					,	empleado: empleado

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	refreshFolders: function( empleado, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_nomina'
					,	task: 'empleados.refreshFolders'
					,	empleado: empleado

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	getFile: function( codigo, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_nomina'
					,	task: 'empleados.getFile'
					,	codigo: codigo

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	downloadPDFLiquidacion: function( id, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_nomina'
					,	task: 'empleados.downloadPDFLiquidacion'
					,	id: id

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	generatePDFSalarial: function( data, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_nomina'
					,	task: 'empleados.generatePDFSalarial'
					,	data: data

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}
    });

})( jQuery, this, this.document, this.Misc, undefined );