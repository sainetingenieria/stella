<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class NominaControllerRetenciones extends JControllerLegacy{

	/**
	 * send retenciones to specific company
	 * @return [type] [description]
	 */
	public function sendRetencionesPDF(){

		$response = new stdClass();

		$data = JRequest::getVar( 'data' );


		$model = $this->getModel('retenciones');

		$result = $model->verifyNit( $data['nit'] );
	

		if (empty($result)) {
			$response->status = 500;
			echo json_encode($response);
			die;
		}

		$retenciones = $model->getRetenciones( $data['nit'] );

		foreach ($retenciones as $key => $retencion) {

			$ret_aplicadas = array();

			for ($i=1; $i < 43 ; $i++) { 

				$base = 'base_'.$i;
				$ret = 'retencion_'.$i;

				if (!empty($retencion->$base)) {
					$ret_aplicadas[] = array(
						'concepto' => $model->nombreRetenciones[$i],
						'base' => $retencion->$base,
						'retencion' => $retencion->$ret,
					);

					$retencion->retenciones = $ret_aplicadas;
				}
			}

			$file_name = certGen::getPdf($retencion);
			
			$this->sendMail($file_name, $retencion);

		}

		$response->retenciones = $retenciones;
		$response->status = 200;
		echo json_encode($response);
		die;

	}	

	public function sendMail($fileName,$retencion){

		$config = JFactory::getConfig();
		$mail = JFactory::getMailer();

		$sender = array(
        	$config->get('mailfrom'),
        	$config->get('fromname'),
        );

        $mail->setSender($sender);
        $mail->addRecipient($retencion->correo_electronico_empresa_beneficiaria);
        $mail->setSubject('Certificado Retención en la Fuente '.$retencion->razon_social_empresa_beneficiaria);
        $mail->Encoding = 'base64';
        $mail->isHtml($ishtml = true);

        $contenido = "<div style='font-weight: 200;font-family: Roboto, sans-serif;font-size:15px; color: #7B7B7B; width: 100%;background-image: url(".JURI::root()."images/correo/fondo-correo.png)'><div style='background: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:13px;display:inline-block;'></div><br><br><img src='".JURI::root()."images/correo/logo-correo.png'><div style='padding-left:14px;padding-right:14px;text-align:justify;'><br><br>

        Señores ".$retencion->razon_social_empresa_beneficiaria.", <br><br>";

        $contenido .= "Atendiendo tu solicitud de Certificado de Retenciones durante el año ".(date('Y')-1)." adjunto encontrarás el certificado de las retenciones realizadas a ".$retencion->razon_social_empresa_beneficiaria." por parte de ".$retencion->razon_social_empresa_retenedora.", <br><br><br>";
        $contenido .= "Si no solicitaste esta información por favor ignorar y eliminar este correo electrónico.<br><br>";
        $contenido .= "</div><br><br><img style='margin: 0 auto;display:block;' src='".JURI::root()."images/correo/datos.png'><br><div style='background-image: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:18px;display:inline-block;'></div>";


        $mail->setBody($contenido);

        $mail->addAttachment($fileName);

        // enviar email
        $mail->Send();



	}
}
?>