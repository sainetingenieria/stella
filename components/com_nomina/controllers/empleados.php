<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );



// Begining of the controller
class NominaControllerEmpleados extends JControllerLegacy{


	public function getEmpleado( $id ){

		$model = $this->getModel('empleados');

		$empleado = $model->getObject($id);

		$hijos = array();

		foreach ($empleado as $key => &$value) {

			if (strpos($key, 'correo') !== false || strpos($key, 'mail') !== false)
				continue;
			
			$value = ucfirst($value);

			if ($key == 'numero_de_hijos') {

				if ($value != '0') {

					for ($i=0; $i <= $value; $i++) {

						if ($i == 0) {
							$i = '';
						}

						$nombre = 'nombre'.$i;
						$identificacion = 'identificacion'.$i;
						$fecha_de_nacimiento = 'fecha_de_nacimiento'.$i;

						if (!empty( $empleado->$nombre )) {

							$hijo = array(
								'nombre' => $empleado->$nombre,
								'identificacion' => $empleado->$identificacion,
								'fecha_de_nacimiento' => date('d/m/Y',$empleado->$fecha_de_nacimiento)
							);

							array_push($hijos, $hijo);
						}
					}
				}
			}
		}

		$modelCarpetas = $this->getModel('carpetas');
		$modelArchivos = $this->getModel('archivos');

		$carpetas = $modelCarpetas->getCarpetas( $id );
		$archivos = $modelArchivos->getArchivos( $id );

		$total = array();

		foreach ($carpetas as $key => $carpeta) {

			$item = array(
				'id' => $carpeta->id,
				'nombre' => $carpeta->nombre,
				'empleado' => $carpeta->id_empleado,
				'tipo' => 'carpeta'
			);

			array_push($total, $item);
		}

		foreach ($archivos as $key => $archivo) {
			$item = array(
				'id' => $archivo->id,
				'nombre' => $archivo->nombre,
				'archivo' => $archivo->archivo,
				'empleado' => $archivo->id_empleado,
				'descripcion' => $archivo->descripcion,
				'tipo' => 'archivo'
			);

			array_push($total, $item);
		}

		$modelVariables = $this->getModel('variables');

		$variables = $modelVariables->getObjects();

		if ($empleado->aux_transporte == 's') {
			$empleado->aux_transporte = '$'.Misc::numberDots($variables[0]->aux_transporte);
		}else{
			$empleado->aux_transporte = 'N/A';
		}

		$empleado->vacaciones_prestacionales = Helper::getVacacionesPrestacionales($empleado->salario, $empleado->corte_de_vacaciones, $empleado->dias_tomados );
		$empleado->cesantias = Helper::getCesantias($empleado);
		$empleado->intereses_cesantias = Helper::getInteresesCesantias($empleado);
		$empleado->prima_legal = Helper::getPrima($empleado);
		$empleado->indemnizacion = Helper::getIndemnizacion($empleado);
		$empleado->retefuente_indemnizacion = Helper::getRetefuenteIndemnizacion($empleado, $variables);

		$empleado->total = $empleado->salario + $empleado->aux_transporte + $empleado->vacaciones_prestacionales + $empleado->cesantias + $empleado->intereses_cesantias + $empleado->prima_legal + $empleado->indemnizacion + $empleado->retefuente_indemnizacion;

		$empleado->vacaciones_prestacionales = Misc::numberDots($empleado->vacaciones_prestacionales);
		$empleado->intereses_cesantias = Misc::numberDots($empleado->intereses_cesantias);
		$empleado->cesantias = Misc::numberDots($empleado->cesantias);
		$empleado->prima_legal = Misc::numberDots($empleado->prima_legal);
		$empleado->indemnizacion = Misc::numberDots($empleado->indemnizacion);
		$empleado->retefuente_indemnizacion = Misc::numberDots($empleado->retefuente_indemnizacion);

		$empleado->total = Misc::numberDots($empleado->total);
		
		$empleado->files = $total;
		$empleado->hijos = $hijos;		

		$empleado->fecha_de_exp = Misc::formatDateSpanish(date('Y-m-d',$empleado->fecha_de_exp));
		$empleado->lugar_de_expedicion = ucfirst($empleado->lugar_de_expedicion);

		$empleado->fecha_nacimiento = Misc::formatDateSpanish(date('Y-m-d',$empleado->fecha_nacimiento));
		$empleado->ciudad_de_nacimiento = ucfirst($empleado->ciudad_de_nacimiento);

		$empleado->fecha_de_ingreso = Misc::formatDateSpanish(date('Y-m-d',$empleado->fecha_de_ingreso));
		$empleado->fecha_terminacion = Misc::formatDateSpanish(date('Y-m-d',$empleado->fecha_terminacion));


		$empleado->lunes_entrada = date('H:i:s',$empleado->lunes_entrada);
		$empleado->martes_entrada = date('H:i:s',$empleado->martes_entrada);
		$empleado->miercoles_entrada = date('H:i:s',$empleado->miercoles_entrada);
		$empleado->jueves_entrada = date('H:i:s',$empleado->jueves_entrada);
		$empleado->viernes_entrada = date('H:i:s',$empleado->viernes_entrada);
		$empleado->sabado_entrada = date('H:i:s',$empleado->sabado_entrada);
		$empleado->domingo_entrada = date('H:i:s',$empleado->domingo_entrada);

		$empleado->lunes_salida = date('H:i:s',$empleado->lunes_salida);
		$empleado->martes_salida = date('H:i:s',$empleado->martes_salida);
		$empleado->miercoles_salida = date('H:i:s',$empleado->miercoles_salida);
		$empleado->jueves_salida = date('H:i:s',$empleado->jueves_salida);
		$empleado->viernes_salida = date('H:i:s',$empleado->viernes_salida);
		$empleado->sabado_salida = date('H:i:s',$empleado->sabado_salida);
		$empleado->domingo_salida = date('H:i:s',$empleado->domingo_salida);

		$empleado->salario = Misc::numberDots($empleado->salario);
		$empleado->aux_alimentacion = Misc::numberDots($empleado->aux_alimentacion);
		$empleado->auxilio_de_educacion = Misc::numberDots($empleado->auxilio_de_educacion);
		$empleado->auxilio_de_vivienda = Misc::numberDots($empleado->auxilio_de_vivienda);
		$empleado->otros_auxilios = Misc::numberDots($empleado->otros_auxilios);
		$empleado->promedio_pagos_salariales = Misc::numberDots($empleado->promedio_pagos_salariales);

		$empleado->corte_de_vacaciones = date('d/m/Y',$empleado->corte_de_vacaciones);
		$empleado->corte_prima = date('d/m/Y',$empleado->corte_prima);
		$empleado->corte_cesantias = date('d/m/Y',$empleado->corte_cesantias);

		$empleado->intereses = Misc::numberDots($empleado->intereses);
		$empleado->vacaciones = Misc::numberDots($empleado->vacaciones);
		$empleado->cesantias_v = Misc::numberDots($empleado->cesantias_v);
		$empleado->prima = Misc::numberDots($empleado->prima);

		

		return $empleado;
	}

	public function getEmpleados(){

		$response = new stdClass();

		$id = JRequest::getVar( 'id' );

		$empleado = $this->getEmpleado($id);

		$response->empleado = $empleado;
		$response->status = 200;
		echo json_encode($response);
		die;
	}

	public function getSubCarpetas(){

		$response = new stdClass();

		$id = JRequest::getVar( 'id' );
		$empleado = JRequest::getVar( 'empleado' );		

		$modelCarpetas = $this->getModel('carpetas');
		$modelArchivos = $this->getModel('archivos');

		if ($id == 0) {
			$response->padre = true;
			$carpetas = $modelCarpetas->getCarpetas( $empleado );
			$archivos = $modelArchivos->getArchivos( $empleado );
		}else{
			$carpetas = $modelCarpetas->getCarpetasByCarpeta( $id );
			$archivos = $modelArchivos->getArchivosByCarpeta( $id );
		}

		$esPadre = $modelCarpetas->verifyIsPadre( $id );

		if (empty($esPadre->id_padre)) {
			$response->codigo = 0;
		}else{
			$response->codigo = $esPadre->id_padre;
		}

		$total = array();

		foreach ($carpetas as $key => $carpeta) {

			$item = array(
				'id' => $carpeta->id,
				'nombre' => $carpeta->nombre,
				'empleado' => $carpeta->id_empleado,
				'tipo' => 'carpeta'
			);

			array_push($total, $item);
		}

		foreach ($archivos as $key => $archivo) {
			$item = array(
				'id' => $archivo->id,
				'nombre' => $archivo->nombre,
				'archivo' => $archivo->archivo,
				'empleado' => $archivo->id_empleado,
				'descripcion' => $archivo->descripcion,
				'tipo' => 'archivo'
			);

			array_push($total, $item);
		}

		$response->files = $total;
		$response->empleado = $empleado;


		$response->status = 200;
		echo json_encode($response);
		die;

	}

	public function refreshFolders(){
		$response = new stdClass();

		$empleado = JRequest::getVar( 'empleado' );

		$modelCarpetas = $this->getModel('carpetas');
		$modelArchivos = $this->getModel('archivos');

		$carpetas = $modelCarpetas->getCarpetas( $empleado );
		$archivos = $modelArchivos->getArchivos( $empleado );

		$total = array();

		foreach ($carpetas as $key => $carpeta) {

			$item = array(
				'id' => $carpeta->id,
				'nombre' => $carpeta->nombre,
				'empleado' => $carpeta->id_empleado,
				'tipo' => 'carpeta'
			);

			array_push($total, $item);
		}

		foreach ($archivos as $key => $archivo) {
			$item = array(
				'id' => $archivo->id,
				'nombre' => $archivo->nombre,
				'archivo' => $archivo->archivo,
				'empleado' => $archivo->id_empleado,
				'descripcion' => $archivo->descripcion,
				'tipo' => 'archivo'
			);

			array_push($total, $item);
		}

		$response->files = $total;
		$response->empleado = $empleado;

		$response->status = 200;
		echo json_encode($response);
		die;	

	}

	public function getFile(){

		$response = new stdClass();

		$codigo = JRequest::getVar( 'codigo' );

		$modelArchivos = $this->getModel('archivos');

		$file = $modelArchivos->getObject( $codigo );

		$filename = JPATH_ROOT.'/pdfs/'.$file->archivo;

		$response->filename = $filename;
		$response->status = 200;
		echo json_encode($response);
		die;	
	}

	public function downloadPDFLiquidacion(){
		$response = new stdClass();

		$id = JRequest::getVar( 'id' );

		$empleado = $this->getEmpleado($id);

		$file_name = liquiGen::getPdf($empleado);

		$response->filename = $file_name;
		$response->status = 200;
		echo json_encode($response);
		die;	
	}

	public function downloadFile(){

		$filename = JRequest::getVar( 'filename' );

		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($filename)).' GMT');
		header('Cache-Control: private',false);
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment; filename="'.basename($filename).'"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.filesize($filename));
		header('Connection: close');
		readfile($filename);
		exit();
	}

	public function generatePDFSalarial(){

		
		$response = new stdClass();

		$data = JRequest::getVar( 'data' );

		$modelVariables = $this->getModel('variables');

		$variables = $modelVariables->getObjects();

		$salario = $variables[0]->salario_minimo;

		$user = JFactory::getUser();

		$data['nit'] = $user->nit;
		$data['razon_social'] = $modelVariables->getEmpresaByNit($user->nit)->razon_social;


		$data['tipo_salario'] = '';

		$data['vacaciones'] = $data['sueldo'] * (15/360);
		$nivelesRiesgo = array(
				'1' => $variables[0]->clase_riesgo1
			,	'2' => $variables[0]->clase_riesgo2
			,	'3' => $variables[0]->clase_riesgo3
			,	'4' => $variables[0]->clase_riesgo4
			,	'5' => $variables[0]->clase_riesgo5
		);

		if ($data['sueldo'] >= ($salario * 13)) {
			$data['tipo_salario'] = ($data['tipo'] == 1) ? 'ordinario' : 'integral';
		}else{
			$data['tipo_salario'] = 'ordinario';
		}

		if ($data['tipo_salario'] == 'ordinario') {
			if ($data['sueldo'] < ($salario * 2)) {
				$data['aux_transporte'] =  $variables[0]->aux_transporte;
			}else{
				$data['aux_transporte'] =  0;
			}

			$data['cesantias'] = ($data['sueldo'] + $data['aux_transporte'] ) * (1/12);
			$data['intereses_cesantias'] = ($data['sueldo'] + $data['aux_transporte'] ) * 0.01;
			$data['prima'] = ($data['sueldo'] + $data['aux_transporte'] ) * (1/12);
			$data['prestaciones'] = $data['vacaciones'] + $data['cesantias'] + $data['intereses_cesantias'] + $data['prima'];

			if ($data['sueldo'] >= ($salario * 10)) {
				$data['sena'] = $data['sueldo'] * 0.02;
				$data['icbf'] = $data['sueldo'] * 0.03;
				$data['salud'] = $data['sueldo'] * 0.085;
			}else{
				$data['salud'] = 0;
				$data['sena'] = 0;
				$data['icbf'] = 0;
			}

			$data['pension'] = $data['sueldo'] * 0.12;
			$data['caja'] = $data['sueldo'] * 0.04;

			$rango_sueldo = 0;

			if ($data['sueldo'] >= ($salario * 4) || $data['sueldo'] < ($salario * 16)) {
				$rango_sueldo = $variables[0]->rangos_fondo1;
			}

			if ($data['sueldo'] >= ($salario * 16) || $data['sueldo'] < ($salario * 17)) {
				$rango_sueldo = $variables[0]->rangos_fondo2;
			}

			if ($data['sueldo'] >= ($salario * 17) || $data['sueldo'] < ($salario * 18)) {
				$rango_sueldo = $variables[0]->rangos_fondo3;
			}

			if ($data['sueldo'] >= ($salario * 18) || $data['sueldo'] < ($salario * 19)) {
				$rango_sueldo = $variables[0]->rangos_fondo4;
			}

			if ($data['sueldo'] >= ($salario * 19) || $data['sueldo'] < ($salario * 20)) {
				$rango_sueldo = $variables[0]->rangos_fondo5;
			}

			if ($data['sueldo'] >= ($salario * 20)) {
				$rango_sueldo = $variables[0]->rangos_fondo6;
			}

			$data['fondo_solidaridad'] = $data['sueldo'] * $rango_sueldo;

			$data['arl'] = $data['sueldo'] * $nivelesRiesgo[$data['nivel_riesgo']];
			$data['aportes'] = $data['salud'] + $data['pension'] + $data['caja'] + $data['sena'] + $data['icbf'] + $data['arl'] + $data['fondo_solidaridad'];
			$data['costo_total'] = $data['sueldo'] + $data['otros_ingresos_no'] + $data['aux_transporte'] + $data['prestaciones'] + $data['aportes'];

		}else{
			$data['salud'] = $data['sueldo'] * 0.7 * 0.085;
			$data['pension'] = ($data['sueldo'] * 0.7) * 0.12;
			$data['caja'] = ($data['sueldo'] * 0.7) * 0.04;
			$data['sena'] = $data['sueldo'] * 0.7 * 0.02;
			$data['icbf'] = $data['sueldo'] * 0.7 * 0.3;
			$data['arl'] = $data['sueldo'] * 0.7 * $nivelesRiesgo[$data['nivel_riesgo']];
			$data['costo_total'] = $data['sueldo'] + $data['otros_ingresos_no'] + $data['salud'] + $data['pension'] + $data['caja'] + $data['sena'] + $data['icbf'] + $data['arl'];
		}

		$file_name = salarialGen::getPdf($data);

		$response->filename = $file_name;
		$response->status = 200;
		echo json_encode($response);
		die;

	}
}
?>