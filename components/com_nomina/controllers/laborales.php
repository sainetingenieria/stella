<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class NominaControllerLaborales extends JControllerLegacy{

	/**
	 * send retenciones to specific company
	 * @return [type] [description]
	 */
	public function sendLaboralesPDF(){

		$response = new stdClass();

		$data = JRequest::getVar( 'data' );

		$model = $this->getModel('empleados');

		$result = $model->verifyCedula( $data['cedula'] );

	
		if (empty($result)) {
			$response->status = 500;
			echo json_encode($response);
			die;
		}

		$empleado = $model->getObject( $result->identificador );

		foreach ($empleado as $key => &$value) {

			if (strpos($key, 'correo') !== false || strpos($key, 'mail') !== false)
				continue;

			$value = ucfirst($value);
		}


		$file_name = laboralGen::getPdf($empleado);
		
		$this->sendMail($file_name, $empleado);

		$response->empleado = $empleado;
		$response->status = 200;
		echo json_encode($response);
		die;

	}	

	public function sendMail($fileName,$empleado){

		$config = JFactory::getConfig();
		$mail = JFactory::getMailer();

		$sender = array(
        	$config->get('mailfrom'),
        	$config->get('fromname'),
        );

        $mail->setSender($sender);
        $mail->addRecipient($empleado->correo_personal);
        $mail->setSubject('Certificado Laboral '.$empleado->nombres.' '.$empleado->apellidos );
        $mail->Encoding = 'base64';
        $mail->isHtml($ishtml = true);

        $contenido = "<div style='font-weight: 200;font-family: Roboto, sans-serif;font-size:15px; color: #7B7B7B; width: 100%;background-image: url(".JURI::root()."images/correo/fondo-correo.png)'><div style='background: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:13px;display:inline-block;'></div><br><br><img src='".JURI::root()."images/correo/logo-correo.png'><div style='padding-left:14px;padding-right:14px;text-align:justify;'><br><br>

        ".$empleado->nombres.' '.$empleado->apellidos.", <br><br>";

        $contenido .= "Adjunto encontrarás el Certificado Laboral para ".$empleado->nombres." ".$empleado->apellidos." de ".$empleado->razon_social." solicitado en ".JURI::root()." para uso personal. <br><br><br>";
        $contenido .= "</div><br><br><img style='margin: 0 auto;display:block;' src='".JURI::root()."images/correo/datos.png'><br><div style='background-image: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:18px;display:inline-block;'></div>";


        $mail->setBody($contenido);

        $mail->addAttachment($fileName);

        // enviar email
        $mail->Send();



	}
}
?>