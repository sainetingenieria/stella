<?php

/**
 * Model for "Object"
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.model' );

// Initializes the Class
class NominaModelRetenciones extends JModelList {

	/**
	 * [$id description]
	 * @var [type]
	 */
	var $id;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nit_empresa_retenedora;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $razon_social_empresa_retenedora;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $direccion;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $ciudad_empresa_retenedora;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nit_empresa_beneficiaria;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $razon_social_empresa_beneficiaria;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $correo_electronico_empresa_beneficiaria;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_1;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_1;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_2;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_2;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_3;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_3;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_4;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_4;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_5;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_5;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_6;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_6;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_7;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_7;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_8;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_8;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_9;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_9;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_10;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_10;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_11;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_11;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_12;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_12;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_13;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_13;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_14;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_14;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_15;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_15;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_16;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_16;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_17;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_17;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_18;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_18;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_19;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_19;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_20;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_20;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_21;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_21;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_22;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_22;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_23;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_23;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_24;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_24;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_25;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_25;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_26;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_26;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_27;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_27;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_28;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_28;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_29;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_29;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_30;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_30;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_31;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_31;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_32;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_32;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_33;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_33;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_34;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_34;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_35;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_35;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_36;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_36;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_37;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_37;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_38;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_38;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_39;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_39;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_40;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_40;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_41;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_41;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $base_42;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $retencion_42;


	/**
	 * Attribute state
	 * @var bool
	 */
	var $state;
	
	/**
	 * Get if the object exists
	 * @var bool
	 */
	var $exists = false;
	
	/**
	 * Cache of results, data, queries
	 * @var unknown
	 */
	var $data;

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = '#__nomina_retenciones';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = 'filter.component.';
	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			'id'
		,	'nit_empresa_retenedora'
		,	'razon_social_empresa_retenedora'
		,	'direccion'
		,	'ciudad_empresa_retenedora'
		,	'nit_empresa_beneficiaria'
		,	'razon_social_empresa_beneficiaria'
		,	'correo_electronico_empresa_beneficiaria'
		,	'base_1'
		,	'retencion_1'
		,	'base_2'
		,	'retencion_2'
		,	'base_3'
		,	'retencion_3'
		,	'base_4'
		,	'retencion_4'
		,	'base_5'
		,	'retencion_5'
		,	'base_6'
		,	'retencion_6'
		,	'base_7'
		,	'retencion_7'
		,	'base_8'
		,	'retencion_8'
		,	'base_9'
		,	'retencion_9'
		,	'base_10'
		,	'retencion_10'
		,	'base_11'
		,	'retencion_11'
		,	'base_12'
		,	'retencion_12'
		,	'base_13'
		,	'retencion_13'
		,	'base_14'
		,	'retencion_14'
		,	'base_15'
		,	'retencion_15'
		,	'base_16'
		,	'retencion_16'
		,	'base_17'
		,	'retencion_17'
		,	'base_18'
		,	'retencion_18'
		,	'base_19'
		,	'retencion_19'
		,	'base_20'
		,	'retencion_20'
		,	'base_21'
		,	'retencion_21'
		,	'base_22'
		,	'retencion_22'
		,	'base_23'
		,	'retencion_23'
		,	'base_24'
		,	'retencion_24'
		,	'base_25'
		,	'retencion_25'
		,	'base_26'
		,	'retencion_26'
		,	'base_27'
		,	'retencion_27'
		,	'base_28'
		,	'retencion_28'
		,	'base_29'
		,	'retencion_29'
		,	'base_30'
		,	'retencion_30'
		,	'base_31'
		,	'retencion_31'
		,	'base_32'
		,	'retencion_32'
		,	'base_33'
		,	'retencion_33'
		,	'base_34'
		,	'retencion_34'
		,	'base_35'
		,	'retencion_35'
		,	'base_36'
		,	'retencion_36'
		,	'base_37'
		,	'retencion_37'
		,	'base_38'
		,	'retencion_38'
		,	'base_39'
		,	'retencion_39'
		,	'base_40'
		,	'retencion_40'
		,	'base_41'
		,	'retencion_41'
		,	'base_42'
		,	'retencion_42'
	);

	var $nombreRetenciones = array(

		1 => 'Compras generales (declarantes)',
		2 => 'Compras generales (no declarantes)',
		3 => 'Compras con tarjeta débito o crédito',
		4 => 'Compras de bienes o productos agrícolas o pecuarios sin procesamiento industrial',
		5 => 'Compras de bienes o productos agrícolas o pecuarios con procesamiento industrial (declarantes)',
		6 => 'Compras de bienes o productos agrícolas o pecuarios con procesamiento industrial declarantes (no declarantes)',
		7 => 'Compras de café pergamino o cereza',
		8 => 'Compras de combustibles derivados del petróleo',
		9 => 'Enajenación de activos fijos de personas naturales(notarías y tránsito son agentes retenedores)',
		10 => 'Compras de vehículos',
		11 => 'Compras de bienes raíces cuya destinación y uso sea vivienda de habitación (por las primeras 20.000 UVT, es decir hasta $549.700.000)',
		12 => 'Compras  de bienes raíces cuya destinación y uso sea vivienda de habitación (exceso de las primeras 20.000 UVT, es decir superior a $565.580.000)',
		13 => 'Compras  de bienes raíces cuya destinación y uso sea distinto a vivienda de habitación',
		14 => 'Servicios generales (declarantes)',
		15 => 'Servicios generales (no declarantes)',
		16 => 'Por emolumentos eclesiásticos (declarantes)',
		17 => 'Por emolumentos eclesiásticos (no declarantes)',
		18 => 'Servicios de transporte de carga',
		19 => 'Servicios de  transporte nacional de pasajeros por vía terrestre (declarantes)',
		20 => 'Servicios de  transporte nacional de pasajeros por vía terrestre (no declarantes)',
		21 => 'Servicios de  transporte nacional de pasajeros por vía aérea o marítima',
		22 => 'Servicios prestados por empresas de servicios temporales (sobre AIU)',
		23 => 'Servicios prestados por empresas de vigilancia y aseo (sobre AIU)',
		24 => 'Servicios integrales de salud prestados por IPS',
		25 => 'Servicios de hoteles y restaurantes (declarantes)',
		26 => 'Servicios de hoteles y restaurantes (no declarantes)',
		27 => 'Arrendamiento de bienes muebles',
		28 => 'Arrendamiento de bienes inmuebles (declarantes)',
		39 => 'Arrendamiento de bienes inmuebles (no declarantes)',
		30 => 'Otros ingresos tributarios (declarantes)',
		31 => 'Otros ingresos tributarios (no declarantes)',
		32 => 'Pagos o abonos en cuenta gravables realizados a personas naturales clasificadas en la categoría tributaria de empleado',
		33 => 'Pagos o abonos en cuenta gravables realizados a personas naturales clasificadas en la categoría tributaria de empleado (tarifa mínima)',
		34 => 'Honorarios y comisiones (personas jurídicas)',
		35 => 'Honorarios y comisiones personas naturales que suscriban contrato o cuya sumatoria de los pagos o abonos en cuenta superen las 3.330 UVT',
		36 => 'Honorarios y comisiones (no declarantes)',
		37 => 'Servicios de licenciamiento o derecho de uso de software',
		38 => 'Intereses o rendimientos financieros',
		39 => 'Rendimientos financieros provenientes de títulos de renta fija',
		40 => 'Loterías, rifas, apuestas y similares',
		41 => 'Retención en colocación independiente de juegos de suerte y azar',
		42 => 'Contratos de construcción  y urbanización',

	);

	/**
	 * Constructor
	 * 
	 * @param { array || int } the args to instance the model or the single id
	 * 
	 */
	public function __construct()
	{
	    parent::__construct();


	    if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'nit', 'a.nit',
				'razon_social', 'a.razon_social',
				'digito_verificacion', 	'a.digito_verificacion',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Adjust the context to support modal layouts.
		if ($layout = JRequest::getVar('layout', 'default'))
		{
			$this->context .= '.'.$layout;
		}

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.razon_social', 'razon_social');
		$this->setState('filter.razon_social', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.nit', 'nit');
		$this->setState('filter.nit', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.digito_verificacion', 'digito_verificacion');
		$this->setState('filter.digito_verificacion', $search);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_nomina');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'ASC');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.razon_social');
		$id	.= ':'.$this->getState('filter.nit');
		$id	.= ':'.$this->getState('filter.digito_verificacion');

		return parent::getStoreId($id);
	}
		
	public function instance( $config = NULL ){
		
		if( is_numeric( $config ) )
			$config = array( $this->attrs_map[ 0 ] => $config );
		
		if( ! is_array( $config ) )
			return;
		
		// Get existing object if the id was passed through
		return $this->fill( $config );
	}
	
	/**
	 * Fill the model attributes with the passed arguments.
	 *
	 * @param { arr } Object arguments
	 */
	protected function fill( $args = NULL ){


		if ( ! is_array( $args ) )
			return false;
		
	
		// Get object in DB			
		if ( is_numeric( $args[ $this->attrs_map[ 0 ] ] ) ){

			$object = $this->getObject( $args[ $this->attrs_map[ 0 ] ] );
			
			if( is_object( $object ) ){
				foreach ( $this->attrs_map as $attr ) {
					
					if ( isset( $object->$attr ) )
						$this->$attr = $object->$attr;
				}
			}

		}

	
		// Merge attributes	when id is not passed through
		foreach ( $this->attrs_map as $attr ) {
			if ( isset( $args[ $attr ] ) )
				$this->$attr = $args[ $attr ];
		}

		// Set exists to true.
		$this->exists = true;
	
	}
	
	
	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getObject( $id = 1 ){
		
		if( ! is_numeric( $id ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		
		$c = get_called_class();  

		$query->select( '*' );
		$query->from( $c::TABLE );
		$query->where( $this->attrs_map[ 0 ].' = ' . $id );
		
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}

	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getRetenciones( $nit ){
		
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		
		$c = get_called_class();  

		$query->select( '*' );
		$query->from( $c::TABLE );
		$query->where( 'nit_empresa_beneficiaria = ' . $db->quote($nit) );

		
		$db->setQuery( $query );
		
		return $db->loadObjectList();
		
	}

	/**
	 * SQL server change
	 *
	 * @param   integer  $user_id  User identifier
	 *
	 * @return  string   Groups titles imploded :$
	 */
	public function verifyNit($nit)
	{
		$db = $this->getDbo();
		$query = "SELECT * FROM " . $db->quoteName('#__nomina_retenciones') . " WHERE nit_empresa_beneficiaria='" . $nit ."'";

		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;
	}
	
	/**
	 * Get Objects collection
	 *
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 **/
	public function getObjects( $wheres = NULL , $prlimits = array() , $params ){
		
		if( !isset( $prlimits[ 'limitstart' ] ) )
			$prlimits[ 'limitstart' ] = $this->getState('list.start');
			
		if( !isset( $prlimits[ 'limit' ] ) )	
			$prlimits[ 'limit' ] = $this->getState('list.limit');
		
		$result = array();
		
		if( ! is_array( $wheres ) )
			$wheres = array();
			
		if( ! is_array( $params ) )	
			$params = array();
		
		$query = $this->buildQuery( $wheres , $params );
		//fb( $query->__toString() );
		$this->data = $this->_getList( $query , $prlimits[ 'limitstart' ] , $prlimits[ 'limit' ] );
		$this->counts = $this->_getListCount($query); 
		
		foreach ( $this->data as $obj ){

			$args = array();
			
			foreach ( $obj as $key => $attr ) {
				
				$args[ $key ] = $obj->$key;
			}
			
			$estaClase = get_class( $this );
	
			$object = new $estaClase();
			$object->fill( $args );
			array_push( $result, $object );
			
		}
	
		return $result;
	
	}

	/**
	 * Truncates table from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function truncate(){
	
		// Restart the table, clean the rows and restart id = 0
		$query = "TRUNCATE TABLE ". self::TABLE;
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}


	/**
	 * Save a new object or update an exist.
	 *
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the query string calling the insert query
	 *
	 */
	public function save( $return = 'string' ){

		// Initialize
		$db = JFactory::getDbo();
		$response = ( object ) array();
		$model = ( object ) array();

		if( ! is_string( $return ) )
			$return = 'string';

		// Fetch the model attributes with the $model's var
		foreach ( $this->attrs_map as $attribute ) {
			$model->$attribute = $this->$attribute;
		}
		
		$c = get_called_class(); 
		
		
		$id =  $this->attrs_map[0];

		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( $model->$id == NULL || $model->$id == "" ){

			if (! $db->insertObject( $c::TABLE, $model ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el registro. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el registro. " . $db->stderr();
					return $response;
				}
			}
			$this->insertId = $db->insertid();

			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( $c::TABLE, $model, $this->attrs_map[ 0 ], false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	/**
	 * Delete object from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function delete(){
		
		$firstkey = $this->attrs_map[ 0 ];
	
		if( ! is_numeric( $this->$firstkey ) )
			return false;
	
	
		// Delete existing object if the id was passed through
		$c = get_called_class();  

		$query = "DELETE FROM ". $c::TABLE ." WHERE ". $this->attrs_map[ 0 ] . " = " . $this->$firstkey;
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}

	/**
	 * Publish or unpublish the object
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the state after change
	 *
	 */
	public function publish( $return = 'string' ){

		$db = JFactory::getDbo();
		
		$firstkey = $this->attrs_map[ 0 ];
		$secondkey = $this->attrs_map[ 1 ];
		
		$this->$secondkey = ( $this->$secondkey == 1 ) ? 0 : 1;

		// Update
		$std = new stdClass();
		$std->$firstkey = $this->$firstkey;
		$std->$secondkey = $this->$secondkey;
		
		$c = get_called_class();  

		if (! $db->updateObject( $c::TABLE, $std, $this->attrs_map[ 0 ], false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo actualizar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	/**
	 * Build a query for collection. Filters query are included.
	 *
	 * @param { array } wheres clausule. Clausule must be { key: 'value', value: 'value', condition:'=', glue: 'AND || OR' }
	 * @return { string } the query string calling the collection
	 *
	 */
	protected function buildQuery( $wheres = NULL , $params = array() ){

		// Validation
		if( ! is_array( $wheres ) )
			$wheres = array();

		// Get a storage key.
		$store = $this->getStoreId();

		// Initialize
		$db = JFactory::getDbo();
		$query  = $db->getQuery(true);
		
		$c = get_called_class();  
		
		// Query base		
		$query->select( "a.*" );
		$query->from( $db->quoteName($c::TABLE).' AS a' );
		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
			
			if( ! is_object( $clausule ) )
				continue;

			$query->where( $db->quoteName($clausule->key) . $clausule->condition . $db->quote($clausule->value) . ' '.trim($clausule->glue).' ' );

		}
		
		//query order
		if( isset($params[ 'order' ]) ){
			$query->order( $params[ 'order' ] );
		}
		else{
			// Add the list ordering clause.
			$query->order($db->escape($this->getState('list.ordering', 'a.id')).' '.$db->escape($this->getState('list.direction', 'ASC')));
		}
		
		//query group	
		if( isset($params['group']) )
			$query->group( $params[ 'group' ] );

		//Filters appending


		//filter for tipo_gestion
		$nit = $this->getState('filter.nit');
		if( !empty($nit) ){
			$query->where( $db->quoteName('a.nit') .' LIKE '. $db->quote('%'.$db->escape($nit).'%') );
		}

		// filter for razon_social
		$razon_social = $this->getState('filter.razon_social');
		if( !empty($razon_social) ){
			$query->where( $db->quoteName('a.razon_social_oec') .' LIKE '. $db->quote('%'.$db->escape($razon_social).'%') );
		}

		// filter for nit
		$digito_verificacion = $this->getState('filter.digito_verificacion');
		if( !empty($digito_verificacion) ){
			$query->where( $db->quoteName('a.digito_verificacion') .' LIKE '. $db->quote('%'.$db->escape($digito_verificacion).'%') );
		}

		// filter for descripcion de la actividad
		$descripcion = $this->getState('filter.descripcion');
		$descripcion = preg_replace('/(\n|\r|\n\r|\t|\0|\x0B)/i', '', $descripcion);
		$descripcion = trim($descripcion);	
		
		if( !empty($descripcion) ){
			$query->where( $db->quoteName('a.descripcion_actividad') .' LIKE '. $db->quote('%'.$db->escape($descripcion).'%') );
		}

		//var_dump( $query->__toString() );
		
		return $query;
	}
	
	/**
	* generar Paginador
	*/
	public function getPagination(){

		// Load the content if it doesn't already exist
        jimport('joomla.html.pagination');
        $_pagination = new JPagination( $this->counts , $this->getState('list.start'), $this->getState('list.limit') );

        return $_pagination;
	}
	
	 /*
	 * obtener usuario que creo o el que actualizo el registro
	 * @param { array } un array de objetos con las consultas
	 * @return { object , array } objeto o arrray de objectos
	 */
	 public function getUserCreate( $wheres = NULL ){
		 
		 if( ! is_array( $wheres ) )
		 	$wheres = array();
		 
		 $db = JFactory::getDbo();
		 $query = $db->getQuery(true);
		 
		 $query->select('*');
		 $query->from( '#__users' );
		 
		 $query->where( 'id = '. $this->created_by );
		 
		 foreach( $wheres as $key=>$where ){
			 
			 if( !is_object( $where ) )
			 	continue;
			 
			 $query->where( $where->key . $where->condition . $where->value . $where->glue );
		 }
		 
		 $db->setQuery( $query );
		 
		 
		return $db->loadObject();
	 }

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	protected function API(){
		
		// Instance an object with defaults
		$Object = new ComponentModelObject();
		
		// Instance an object with args not ID
		$args = array(
				'attribue'	=> 'any'
		);
		
		$Object = new ComponentModelObject();
		$Object->instance( $args );
		$Object->save(); // saves a new item
		
		//---------------
		// Instance an object with args with the ID
		// Result will be the model from DB merge the fields passed
		$args = array(
					'id' 		=> 1
				,	'tema' 		=> 1
				,	'titulo'		=> 'example'
				,	'ruta' 		=> 'pdf_test.pdf'
				,	'ano'		=> '2013'
				,	'state'		=>	1 // state pusblished
		);
		
		$Object = new ComponentModelObject();
		$Object->instance( $args );
		$Object->save(); // It will update the object with the id
		$Object->delete(); // It will delete the object with the id

		// Instance an object with args with the ID		
		$Object = new ComponentModelObject( 1 );

		// To get the objects with the table
		$Object = new ComponentModelObject();
		$object->getObjects();


		// To get the objects with conditions
		$wheres = array(
			0 => ( object ) array(
					'key' => 'id'
				,	'value' => '3'
				,	'condition' => '='
				,	'glue' => 'AND'
			)
		);

		$Object = new ComponentModelObject();
		$Object->getObjects( $wheres );
	}
	
	
	

	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getEmpresaByNit( $id = 1 ){
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*' );
		$query->from( self::TABLE );
		$query->where( 'nit = ' . $db->quote($id) );
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}
	
	
}
?>