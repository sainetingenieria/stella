<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . '/controller.php' );
require_once( JPATH_COMPONENT . '/helpers/misc.php' );

require_once( JPATH_COMPONENT . '/helpers/helper.php' );
require_once( JPATH_COMPONENT . '/helpers/vendor/autoload.php' );
require_once( JPATH_COMPONENT . '/helpers/cert-generator.php' );
require_once( JPATH_COMPONENT . '/helpers/liquidacion-generator.php' );
require_once( JPATH_COMPONENT . '/helpers/salarial-generator.php' );
require_once( JPATH_COMPONENT . '/helpers/laboral-generator.php' );




$controller = JControllerLegacy::getInstance('Nomina');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>