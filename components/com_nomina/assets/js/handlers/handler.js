/**
*
* Event Handler for { Object }
*
*
**/
( function( $, window, document, /*controller, Utilities*/ ){
	
	var ObjectHandler = function( a ){
		
		// atributes, selector or global vars
		this.attribute = 'my-atribute';
		
	};
	
	ObjectHandler.prototype ={
			
			/**
			 * Inializes the functions when DOM ready
			 */	
			initialize: function(){
				
				this.myFunctionClick();
				
			}

			/**
			 * Function header
			 *
			 * @param {}
			 * @return {}
			 */
		,	myFunctionClick: function(){
				
				var _this = this;
			
				$( this.attribute ).click( function( e ){
					
					
					return _this.attribute;

				});
			}
		
			
	};
	
	// Use this when the script is loaded in <head> tag
	$( document ).ready( function(){
		// Expose to global scope
		window.ObjectHandler = new ObjectHandler();
		window.ObjectHandler.initialize();
	});

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	// window.ObjectHandler = new ObjectHandler();
	// window.ObjectHandler.initialize();
	
})( jQuery, this, this.document, /*this.ObjectController, this.Misc, */ undefined );