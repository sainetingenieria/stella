<?php
/**
* This class generates the certifies dinamically
*
*/
use Dompdf\Dompdf;

ini_set('memory_limit', '-1');
class laboralGen {

	public static function getPdf( $data ){

		// if( ! is_object( $data ) )
		// 	return false;

        $starttime = microtime(true);

		$dompdf = new DOMPDF();


		// $font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader( $data );
		$html .= self::getContent( $data );
		$html .= self::getFooter( $data );

		// var_dump($html);
		// die;


		$dompdf->load_html( $html );
		$dompdf->set_paper('A4','portrait');
		$dompdf->render();

		

		$_output = $dompdf->output();

		$randomString = JPATH_COMPONENT . '/assets/pdf/'.time().'_'.$data->nit_empresa_retenedora.'_laboral.pdf';

		file_put_contents($randomString, $_output );

		return $randomString;
	}


	/**
	* Returns the pdf header
	* 
	* @param { string } the type of document
	* @return { string } the html compiled
	*/
	protected static function getHeader($data){

		$html = '';

				$html .= '<html>
				<head>
					<title>CERTIFICADO LABORAL</title>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					'.self::getStyles().'
				</head>

				<body>
				<img src="'.JPATH_ROOT.'/images/pdf/encabezado.png" style="width: 100%;">
				<br><br>
				<div class="header" style="text-align: left;padding-left: 60px;margin-top: 22px;">
					<img src="'.JPATH_ROOT.'/logos/'.$data->imagen_membrete.'">
	 			</div><br><br><br>';

		return $html;

	}

	protected static function getContent( $data ){

		$tipo_contrato = ($data->tipo_contrato == '') ? 'Termino Indefinido' : 'Termino Fijo';

		$html .= '
		<br><br><br><br><br>
		<div class="main">
			<div class="center">
				<table style="width: 100%;">
					<tr>								
						<td style="text-align: center; font-weight: bold;font-size: 17px;">
							CERTIFICADO LABORAL
						</td>
					</tr>
				</table>
			</div>
			<br><br><br>


			<div class="center" style="text-align:justify; padding: 55px;">
				<p><span style="font-weight: bold;">'.$data->razon_social.'</span> certifica que:</p>
				<br>
				<p><span style="font-weight: bold;">'.ucfirst($data->nombres).' '.ucfirst($data->apellidos).'</span> identificado con cedula de ciudadania numero <span style="font-weight: bold;">'.$data->numero_identificacion.'</span> de '.$data->lugar_expedicion.' labora en esta empresa desempeñando el cargo de '.$data->cargo.'.</p>

				<br>

				<p>Tiene contrato a '.$tipo_contrato.' desde el '.date('d/m/y', $data->fecha_de_ingreso).'.</p>

				<p>Devenga un salario de <span style="font-weight: bold;">'.Misc::numberDots($data->salario).' M/cte mensuales.</span> Y otros ingresos de otros auxilios</p>

				<br>

				<p>Se expide el dia '.date('d/m/y').' con destino a quien interese.</p>
			</div>
			';

		$html .= '
			<div class="center" style="text-align:justify; padding: 55px;">
				<img src="'.JPATH_ROOT.'/logos/'.$data->imagen_firma.'">
			</div>
		</div>';

		return $html;
	}

	protected static function getFooter($data){
		$html = '<br><br><br><br><br><br>
		

		<br><br><br><br><br><br>
		<img src="'.JPATH_ROOT.'/images/pdf/pie-de-pag.png" style="width: 100%;margin-top:45px;">';

		return $html;
	}

	protected static function getStyles(){

		$html .= '
		<style type="text/css">

			*{
				font-family: "Roboto", sans-serif;
				margin:0;
				padding:0;
				font-weight: lighter;
			}

			body {
				background-color: #FFF;
				text-align: center;
				font-size: 14px;
			}

			@page document-wrapper {
				size: ar portrait; 
				margin: 1cm;
				background-color: #FFF;
				padding: 40px 0;
			}

		</style>';

		return $html;
	}
}
?>