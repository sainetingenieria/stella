<?php
/**
* This class generates the certifies dinamically
*
*/
use Dompdf\Dompdf;

ini_set('memory_limit', '-1');
class certGen {

	public static function getPdf( $data ){

		// if( ! is_object( $data ) )
		// 	return false;

        $starttime = microtime(true);

		$dompdf = new DOMPDF();


		// $font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader();
		$html .= self::getContent( $data );
		$html .= self::getFooter( );

		// var_dump($html);
		// die;

		$dompdf->load_html( $html );
		$dompdf->set_paper('A4','portrait');
		$dompdf->render();

		

		$_output = $dompdf->output();

		$randomString = JPATH_COMPONENT . '/assets/pdf/'.time().'_'.$data->nit_empresa_retenedora.'_retencion.pdf';

		file_put_contents($randomString, $_output );

		return $randomString;
	}


	/**
	* Returns the pdf header
	* 
	* @param { string } the type of document
	* @return { string } the html compiled
	*/
	protected static function getHeader(){

		$html = '';

				$html .= '<html>
				<head>
					<title>CERTIFICADO DE RETENCIÓN EN LA FUENTE</title>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					'.self::getStyles().'
				</head>

				<body>
				<img src="'.JPATH_ROOT.'/images/pdf/encabezado.png" style="width: 100%;">
				<br><br>
				<br><br><br>
				<div class="header" style="text-align: left;padding-left: 60px;margin-top: 22px;">
					<img src="'.JPATH_ROOT.'/images/pdf/logo-gris.jpg">
	 			</div><br><br><br><br><br>';

		return $html;

	}

	protected static function getContent( $data ){

		$html = '
		<div class="main">
			<div class="center">
				<table style="width: 100%;">
					<tr>								
						<td style="text-align: center; font-weight: bold;font-size: 17px;">
							CERTIFICADO DE RETENCIÓN EN LA FUENTE
						</td>
					</tr>
					<tr>								
						<td style="text-align: center;font-size: 13px;font-style: italic;">
							Año Gravable '.(date('Y') - 1).'
						</td>
					</tr>
				</table>
			</div>

			<br><br>
			<br><br>

			<div class="center">
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;">
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Razón social
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.ucfirst($data->razon_social_empresa_retenedora).'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Nit
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->nit_empresa_retenedora.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Dirección
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->direccion.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Ciudad
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.ucfirst($data->ciudad_empresa_retenedora).'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Retenido a
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->razon_social_empresa_beneficiaria.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Nit
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-bottom: 8px;padding-top: 7px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->nit_empresa_beneficiaria.'
						</td>
					</tr>
				</table>
			</div>

			<br><br>';



		$html .= '
		<div class="center">

			<table style="width: 85%;margin: 0 auto;">
				<tr style="background-color: #E8E8E8; color: #4F4F4F">
					<td style="padding-top: 8px;padding-bottom: 8px;text-align: justify;padding-left: 8px;">
						Concepto
					</td>
					<td style="padding-top: 8px;padding-bottom: 8px;text-align: center;">
						Base
					</td>
					<td style="padding-top: 8px;padding-bottom: 8px;text-align: center;">
						Valor Retenido
					</td>
				</tr>';

		foreach ($data->retenciones as $key => $retencion) {
			$html .= '
				<tr style="background-color: #F1F2F2; color:#949494;">
					<td style="padding-top: 8px;padding-bottom: 8px;text-align: justify;padding-left: 8px;">
						'.$retencion['concepto'].'
					</td>
					<td style="padding-top: 8px;padding-bottom: 8px;text-align: center;">
						$ '.Misc::numberDots($retencion['base']).'
					</td>
					<td style="padding-top: 8px;padding-bottom: 8px;text-align: center;">
						$ '.Misc::numberDots($retencion['retencion']).'
					</td>
				</tr>';
		}

		$html .= '</table></div><br><br><br><br></div>';

		return $html;
	}

	protected static function getFooter(){
		$html = '
		<div class="footer" style="width: 85%;margin: 0 auto;font-style: italic;color: #9C9C9C;">

			<div class="center">
				<p>“El artículo 10 del decreto 836 de 1991: las personas jurídicas podrán entregar los certificados de retención en la fuente, en forma continua impresa por computador, sin necesidad de firma autógrafa”.</p>
			</div>
		</div>

		<br><br><br><br><br><br><br><br>
		<img src="'.JPATH_ROOT.'/images/pdf/pie-de-pag.png" style="width: 100%;margin-top:15px;">';

		return $html;
	}

	protected static function getStyles(){

		$html .= '
		<style type="text/css">

			

			*{
				font-family: "Roboto";
				margin:0;
				padding:0;
			}

			body {
				background-color: #FFF;
				text-align: center;
				font-size: 14px;
			}

			@page document-wrapper {
				size: legal landscape; 
				margin: 1cm;
				background-color: #FFF;
				padding: 40px 0;
			}

		</style>';

		return $html;
	}
}
?>