<?php
/**
 * Helper class for anuncios component
 *
 * 
 */
class Helper {

    public static function getDepartamentos()  {

		$db = JFactory::getDbo();

    	$query = $db->getQuery(true);
    	$query->select('departamento as value, departamento as text');
    	$query->from('#__nomina_empleados');
        $query->group('departamento');

    	
    	$db->setQuery( $query );

        $result = $db->loadObjectList();

        array_unshift($result, "");

    	return $result;
    }

    public static function getVacacionesPrestacionales($salario, $corte, $dias){

        $trans   = ($empleado->corte_de_vacaciones-strtotime(date('Y-m-d')))/86400;
        $trans   = abs($trans); 
        $trans   = floor($trans);

        $vacaciones = (($salario/30)*(((15/360) * ($trans/ 360))) - $dias);

        return $vacaciones;
    
    }


     public static function getCesantias($empleado){

        $trans   = ($empleado->corte_cesantias-strtotime(date('Y-m-d')))/86400;
        $trans   = abs($trans); 
        $trans   = floor($trans); 

        $sumatoria_anios = ($empleado->salario + $empleado->aux_transporte + $empleado->promedio_pagos_salariales) / 360;

        $cesantias = $sumatoria_anios * $trans;
       

        return $cesantias;
    
    }

    public static function getInteresesCesantias($empleado){

        $trans   = ($empleado->corte_cesantias-strtotime(date('Y-m-d')))/86400;
        $trans   = abs($trans); 
        $trans   = floor($trans);


        $cesantias = $empleado->cesantias / 360;

        $intereses = ($cesantias * $trans) * 0.12;

        return $intereses;
    
    }

    public static function getPrima($empleado){

        $trans   = ($empleado->corte_prima-strtotime(date('Y-m-d')))/86400;
        $trans   = abs($trans); 
        $trans   = floor($trans); 

        $sumatoria_anios = ($empleado->salario + $empleado->aux_transporte + $empleado->promedio_pagos_salariales) / 360;

        $prima = $sumatoria_anios * $trans;
       

        return $prima;
    
    }

    public static function getIndemnizacion($empleado){

        if ($empleado->tipo_contrato == 'i') {
            $trans   = ($empleado->fecha_de_ingreso-strtotime(date('Y-m-d')))/86400;
            $trans   = abs($trans); 
            $trans   = floor($trans);

            if ($trans < 365) {
               $indemnizacion = ((30/12)*$trans);
            }else{
               $indemnizacion = ($empleado->salario + ((20/12)*$trans));
            }

        }else{
            $trans   = ($empleado->fecha_terminacion-strtotime(date('Y-m-d')))/86400;
            $trans   = abs($trans); 
            $trans   = floor($trans);

            $indemnizacion = $trans * ($empleado->salario / 240);
        }

        return $indemnizacion;
    }

    public static function getRetefuenteIndemnizacion($empleado, $variables){

        $salario_minimo = $variables[0]->salario_minimo * 10;

        $retefuente = 0;

        if ($empleado->salario > $salario_minimo) {
            $retefuente = ($empleado->indemnizacion - ($empleado->indemnizacion * 0.25)) * 0.2;
        }

        return $retefuente;

    }
  
}
?>