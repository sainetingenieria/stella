<?php
/**
* This class generates the certifies dinamically
*
*/
use Dompdf\Dompdf;

ini_set('memory_limit', '-1');
class salarialGen {

	public static function getPdf( $data ){

		// if( ! is_object( $data ) )
		// 	return false;

		$dompdf = new DOMPDF();

		// $font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader( $data );
		$html .= self::getContent( $data );
		$html .= self::getFooter( $data );

		// var_dump($html);
		// die;

		$dompdf->load_html( $html );
		$dompdf->set_paper('A4','portrait');
		$dompdf->render();

		$_output = $dompdf->output();

		$randomString = JPATH_COMPONENT . '/assets/pdf/'.time().'_'.$data->nit_empresa_retenedora.'_salarial.pdf';

		file_put_contents($randomString, $_output );

		return $randomString;
	}


	/**
	* Returns the pdf header
	* 
	* @param { string } the type of document
	* @return { string } the html compiled
	*/
	protected static function getHeader(){

		$html = '';

				$html .= '<html>
				<head>
					<title>Certificado Salarial</title>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					'.self::getStyles().'
				</head>

				<body>
				<img src="'.JPATH_ROOT.'/images/pdf/encabezado.png" style="width: 100%;">
				<br><br>
				<div class="header" style="text-align: left;padding-left: 60px;margin-top: 22px;">
					<img src="'.JPATH_ROOT.'/images/pdf/logo-gris.jpg">
	 			</div><br><br><br>';

		return $html;

	}

	protected static function getContent( $data ){

		if ($data['tipo_salario'] == 'integral') {
			$html .= '<br><br><br><br><br><br><br><br><br>';
		}

		$html .= '
		<div class="main">
			<div class="center">
				<table style="width: 100%;">
					<tr>								
						<td style="text-align: center; font-weight: bold;font-size: 17px;">
							SALARIO '.strtoupper($data["tipo_salario"]).'
						</td>
					</tr>
				</table>
			</div>
			<br><br><br>

			<br>


			<div class="center">
				<h3 style="color: #0071B3;text-align: left;width: 85%;margin: 0 auto;font-weight: 200;margin-bottom: 10px;">Datos de la Empresa</h3>
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;text-align: left;border: 2px solid #F2F2F2;">
					<tr>								
						<td style="padding-top: 10px;color: #797979;padding-left:20px;font-size: 15px;">
							<span style="color:#4F4F4F;">Razón Social:</span>
							'.ucfirst($data['razon_social']).'
						</td>
					</tr>
					<tr>								
						<td style="padding-bottom: 8px;padding-top: 5px;color: #797979;padding-left:20px;font-size: 15px;">
							<span style="color:#4F4F4F;">Nit:</span>
							'.$data['nit'].'
						</td>
					</tr>
				</table>
			</div>

			<br>

			<div class="center">
				<h3 style="color: #0071B3;text-align: left;width: 85%;margin: 0 auto;font-weight: 200;margin-bottom: 10px;">Resultados</h3>
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;text-align: left;">
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Salario
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['sueldo']).'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Otros Ingresos No Salariales
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['otros_ingresos_no']).'
						</td>
					</tr>';

					if (isset($data['aux_transporte'])) {
						$html .= '<tr>								
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
								Auxilio de Transporte
							</td>
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
								$'.Misc::numberDots($data['aux_transporte']).'
							</td>
						</tr>';
					}
					

					$html .= '<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Vacaciones
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['vacaciones']).'
						</td>
					</tr>';

					if (isset($data['cesantias'])) {
						$html .= '<tr>								
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
								Cesantias
							</td>
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
								$'.Misc::numberDots($data['cesantias']).'
							</td>
						</tr>';
					}
					
					if (isset($data['intereses_cesantias'])) {
						$html .= '<tr>								
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
								Intereses de Cesantias
							</td>
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
								$'.Misc::numberDots($data['intereses_cesantias']).'
							</td>
						</tr>';
					}

					if (isset($data['prima'])) {
						$html .= '<tr>								
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
								Prima Anual
							</td>
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
								$'.Misc::numberDots($data['prima']).'
							</td>
						</tr>';
					}

					if (isset($data['prestaciones'])) {
						$html .= '<tr>								
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
								Prestaciones
							</td>
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
								$'.Misc::numberDots($data['prestaciones']).'
							</td>
						</tr>';
					}

					$html .= '<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Salud
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['salud']).'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Pension
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['pension']).'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Caja de Compensacion
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['caja']).'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							SENA
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['sena']).'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							ICBF
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['icbf']).'
						</td>
					</tr>

					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							ARL
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['arl']).'
						</td>
					</tr>';

					if (isset($data['fondo_solidaridad'])) {
						$html .= '<tr>								
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
								Fondo de Solidaridad Pensional
							</td>
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
								$'.Misc::numberDots($data['fondo_solidaridad']).'
							</td>
						</tr>';
					}

					if (isset($data['aportes'])) {
						$html .= '<tr>								
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
								Aportes de Seguridad Social
							</td>
							<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
								$'.Misc::numberDots($data['aportes']).'
							</td>
						</tr>';
					}

					$html .= '<tr style="background-color: #ECECEC;">								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Costo Total
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							$'.Misc::numberDots($data['costo_total']).'
						</td>
					</tr>
				</table>
			</div>
			';



		$html .= '</div>';

		return $html;
	}

	protected static function getFooter($data){

	
		$html = '<br><br><br><br><br>';

		if ($data['tipo_salario'] == 'integral') {
			$html .= '<br>';
			$html .= '<br><br><br><br>
			<img src="'.JPATH_ROOT.'/images/pdf/pie-de-pag.png" style="width: 100%;margin-top:55px;">';
		}else{
			
			$html .= '<br><br><br><br>
			<img src="'.JPATH_ROOT.'/images/pdf/pie-de-pag.png" style="width: 100%;margin-top:44px;">';
		}

		return $html;
	}

	protected static function getStyles(){

		$html .= '
		<style type="text/css">

			*{
				font-family: "Roboto";
				margin:0;
				padding:0;
			}

			body {
				background-color: #FFF;
				text-align: center;
				font-size: 14px;
			}

			@page document-wrapper {
				size: legal landscape; 
				margin: 1cm;
				background-color: #FFF;
				padding: 40px 0;
			}

		</style>';

		return $html;
	}
}
?>