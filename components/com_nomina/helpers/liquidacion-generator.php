<?php
/**
* This class generates the certifies dinamically
*
*/
use Dompdf\Dompdf;

ini_set('memory_limit', '-1');
class liquiGen {

	public static function getPdf( $data ){

		// if( ! is_object( $data ) )
		// 	return false;

        $starttime = microtime(true);

		$dompdf = new DOMPDF();


		// $font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader();
		$html .= self::getContent( $data );
		$html .= self::getFooter( );

		// var_dump($html);
		// die;


		$dompdf->load_html( $html );
		$dompdf->set_paper('A4','portrait');
		$dompdf->render();

		

		$_output = $dompdf->output();

		$randomString = JPATH_COMPONENT . '/assets/pdf/'.time().'_'.$data->nit_empresa_retenedora.'_liquidacion.pdf';

		file_put_contents($randomString, $_output );

		return $randomString;
	}


	/**
	* Returns the pdf header
	* 
	* @param { string } the type of document
	* @return { string } the html compiled
	*/
	protected static function getHeader(){

		$html = '';

				$html .= '<html>
				<head>
					<title>RESULTADOS CALCULO LIQUIDACION</title>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					'.self::getStyles().'
				</head>

				<body>
				<img src="'.JPATH_ROOT.'/images/pdf/encabezado.png" style="width: 100%;">
				<br><br>
				<div class="header" style="text-align: left;padding-left: 60px;margin-top: 22px;">
					<img src="'.JPATH_ROOT.'/images/pdf/logo-gris.jpg">
	 			</div><br><br><br>';

		return $html;

	}

	protected static function getContent( $data ){

		$html = '
		<div class="main">
			<div class="center">
				<table style="width: 100%;">
					<tr>								
						<td style="text-align: center; font-weight: bold;font-size: 17px;">
							RESULTADOS CALCULO LIQUIDACION
						</td>
					</tr>
				</table>
			</div>
			<br><br><br>
			<div class="center">
				<h3 style="color: #0071B3;text-align: left;width: 85%;margin: 0 auto;font-weight: 200;margin-bottom: 10px;">Datos del Empleado</h3>
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;text-align: left;border: 2px solid #F2F2F2;">
					<tr>								
						<td style="padding-top: 10px;color: #797979;padding-left:20px;font-size: 15px;">
							<span style="color:#4F4F4F;">Nombres y Apellidos:</span>
							'.ucfirst($data->nombres).' '.ucfirst($data->apellidos).'
						</td>
					</tr>
					<tr>								
						<td style="padding-top: 5px;color: #797979;padding-left:20px;font-size: 15px;">
							<span style="color:#4F4F4F;">Número de documento de identidad:</span>
							'.$data->numero_identificacion.'
						</td>
					</tr>
					<tr>								
						<td style="padding-top: 5px;color: #797979;padding-left:20px;font-size: 15px;">
							<span style="color:#4F4F4F;">Cargo:</span>
							'.$data->cargo.'
						</td>
					</tr>
					<tr>								
						<td style="padding-bottom: 8px;padding-top: 5px;color: #797979;padding-left:20px;font-size: 15px;">
							<span style="color:#4F4F4F;">Fecha de ingreso:</span>
							'.date('d-m-y',$data->fecha_ingreso).'
						</td>
					</tr>
				</table>
			</div>

			<br>


			<div class="center">
				<h3 style="color: #0071B3;text-align: left;width: 85%;margin: 0 auto;font-weight: 200;margin-bottom: 10px;">Datos de la Empresa</h3>
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;text-align: left;border: 2px solid #F2F2F2;">
					<tr>								
						<td style="padding-top: 10px;color: #797979;padding-left:20px;font-size: 15px;">
							<span style="color:#4F4F4F;">Razón Social:</span>
							'.ucfirst($data->razon_social).'
						</td>
					</tr>
					<tr>								
						<td style="padding-bottom: 8px;padding-top: 5px;color: #797979;padding-left:20px;font-size: 15px;">
							<span style="color:#4F4F4F;">Nit:</span>
							'.$data->nit.'
						</td>
					</tr>
				</table>
			</div>

			<br>

			<div class="center">
				<h3 style="color: #0071B3;text-align: left;width: 85%;margin: 0 auto;font-weight: 200;margin-bottom: 10px;">Resultados</h3>
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;text-align: left;">
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Salario
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->salario.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Auxilio de Transporte
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->aux_transporte.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Vacaciones Prestacionales
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->vacaciones_prestacionales.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Cesantias
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->cesantias.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Intereses Cesantias
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->intereses_cesantias.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Prima de Servicios
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->prima_legal.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Indemnizacion
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->indemnizacion.'
						</td>
					</tr>
					<tr>								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Retefuente por Indemnizacion
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->retefuente_indemnizacion.'
						</td>
					</tr>
					<tr style="background-color: #ECECEC;">								
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color: #4F4F4F;padding-left:5px;font-size: 15px;">
							Costo Total
						</td>
						<td style="border-bottom: 1px solid #CCC; padding-top: 2px;color:#A1A1A1;padding-right:5px;text-align:right;font-size: 15px;">
							'.$data->total.'
						</td>
					</tr>
				</table>
			</div>
			';



		$html .= '</div>';

		return $html;
	}

	protected static function getFooter(){
		$html = '<br><br><br><br>
		<div class="footer" style="width: 85%;margin: 0 auto;font-style: italic;color: #9C9C9C;">

			<div class="center">
				<p>"El valor final puede variar y no tiene validez legal"</p>
				<p>"No se tienen en cuenta la Retención en la fuente por salarios"</p>
			</div>
		</div>

		<br><br><br><br><br><br>
		<img src="'.JPATH_ROOT.'/images/pdf/pie-de-pag.png" style="width: 100%;margin-top:24px;">';

		return $html;
	}

	protected static function getStyles(){

		$html .= '
		<style type="text/css">

			*{
				font-family: "Roboto";
				margin:0;
				padding:0;
			}

			body {
				background-color: #FFF;
				text-align: center;
				font-size: 14px;
			}

			@page document-wrapper {
				size: legal landscape; 
				margin: 1cm;
				background-color: #FFF;
				padding: 40px 0;
			}

		</style>';

		return $html;
	}
}
?>