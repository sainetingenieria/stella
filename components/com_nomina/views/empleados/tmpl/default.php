<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.

JHtml::_('behavior.framework');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.formvalidator');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
        var url = "'.JURI::base().'"
    ' );

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$user = JFactory::getUser();
$app = JFactory::getApplication();

if ($user->guest) {
    $app->redirect('index.php');
}

?>

<form action="" method="post" name="adminForm" id="adminForm" class="list-empleados">
    <h1>Empleados</h1>

    <div id="filter-bar" class="btn-toolbar">

        <div class="filter-search btn-group pull-left"> 
            <label class="filter-search-lbl" for="nit">Número de Identificación</label>
            <input type="text" placeholder="" name="numero_identificacion" id="numero_identificacion" class="plg-datatime" value="<?php echo $this->state->get( 'filter.numero_identificacion' );?>">
        </div>

        <div class="filter-search btn-group pull-left">
            <label class="filter-search-lbl" for="razon_social">Nombres y Apellidos:</label>
            <input type="text" placeholder="" name="nombres" id="nombres" value="<?php echo $this->state->get( 'filter.nombres' );?>">
        </div>

        <div class="filter-search btn-group pull-left">
            <?php $departamento = $this->state->get( 'filter.departamento' ); ?>
            <label class="filter-search-lbl" for="razon_social">Departamento:</label>
            <?php echo JHTML::_('select.genericlist', Helper::getDepartamentos(), 'departamento', 'class="inputbox"', 'value', 'text', $departamento); ?>
        </div>
                
    </div>
     <div class="filter-search btn-group pull-left actions">
        <button type="submit" class="btn hasTooltip" title="" data-original-title="Buscar">
            Buscar
            <span class="icon-search"></span>
        </button>
        <!-- <button type="button" class="btn hasTooltip" title="" onclick="document.getElementById('nit').value='';document.getElementById('nombre').value='';document.getElementById('numero_identificacion').value='';this.form.submit();" data-original-title="Limpiar">
            <span class="icon-remove"></span>
        </button> -->
    </div>

    <div class="clr"> </div>

    <table class="table table-striped empleados">
        <thead>
            <tr>
                <th class="center nowrap">
                   <?php echo JHtml::_('grid.sort', 'Número de Identificación', 'a.numero_identificacion', $listDirn, $listOrder); ?>
                </th>
                <th class="center nowrap">
                    <?php echo JHtml::_('grid.sort', 'Nombres', 'a.nombres', $listDirn, $listOrder); ?>
                </th>
                <th class="center nowrap">
                    <?php echo JHtml::_('grid.sort', 'Apellidos', 'a.apellidos', $listDirn, $listOrder); ?>
                </th>
                <th class="center nowrap">
                    <?php echo JHtml::_('grid.sort', 'Departamento', 'a.departamento', $listDirn, $listOrder); ?>
                </th>
                <th class="center nowrap">
                    <?php echo JHtml::_('grid.sort', 'Estado', 'a.activo', $listDirn, $listOrder); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="9">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
        <?php foreach ($this->items as $i => $item) : ?>

            <?php 
                $class = '';

                if ($item->activo == 0){
                    $class = 'desactivaded';
                }
            ?>
            
            <tr class="row<?php echo $i % 2; ?> show-info <?php echo $class; ?>" data-id="<?php echo $item->id; ?>">
                <td class="center"><?php echo $item->numero_identificacion?></td>
                <td class="center"><?php echo ucfirst($item->nombres)?></td>
                <td class="center"><?php echo ucfirst($item->apellidos)?></td>
                <td class="center"><?php echo ucfirst($item->departamento)?></td>
                <td class="center"><?php $estado = ($item->activo == 1) ? '<span class="activo">Activo</span>' : '<span class="inactivo">Inactivo</span>'; echo $estado;?><i class="not-expanded"></i></td>
            </tr>
        <?php 
        endforeach; 
        ?>
        </tbody>
    </table>

    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <input type="hidden" name="boxchecked" value="0" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>