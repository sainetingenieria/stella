<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewEmpleados extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	// Function that initializes the view
	public function display( $tpl = null ){

		$this->items		= $this->get('Objects');
		$this->pagination 	= $this->get('Pagination');
		$this->state		= $this->get('State');
		
		parent::display( $tpl );
	}

}
?>