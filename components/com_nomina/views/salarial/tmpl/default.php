<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.

JHtml::_('behavior.framework');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.formvalidator');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
        var url = "'.JURI::base().'"
    ' );


$user = JFactory::getUser();
$app = JFactory::getApplication();

if ($user->guest) {
    $app->redirect('index.php');
}

?>

<div class="registration">


    <form action="" method="post" name="adminForm" id="calculadora-salarial" class="calculadora-salarial">
        <h1>Calculadora Salarial</h1>   

        <fieldset>
            <div class="control-group">
                <div class="controls">
                    <input type="text" name="sueldo" placeholder="Sueldo">
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <input type="text" name="otros_ingresos" placeholder="Otros Ingresos Salariales">
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <select name="nivel_riesgo" id="nivel-riesgo">
                        <option value="">Nivel de Riesgo</option>
                        <option value="1">Muy bajo</option>
                        <option value="2">Bajo</option>
                        <option value="3">Medio</option>
                        <option value="4">Alto</option>
                        <option value="5">Muy Alto</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <select name="tipo" id="tipo">
                        <option value="">Tipo</option>
                        <option value="1">Ordinario</option>
                        <option value="2">Integral</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <input type="text" name="otros_ingresos_no" placeholder="Otros Ingresos No Salariales">
                </div>
            </div>
        </fieldset>

        <div class="control-group actions">
            <div class="cpntrols">
                <button type="submit" class="btn btn-primary validate"><span>Descargar PDF</span></button>
            </div>
        </div>

    </form>
</div>
