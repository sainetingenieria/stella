<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewSalarial extends JViewLegacy {

	// Function that initializes the view
	public function display( $tpl = null ){
		
		parent::display( $tpl );
	}

}
?>