<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.
//JHtml::_('behavior.tooltip');
//JHtml::_('behavior.multiselect');
//JHtml::_('behavior.modal');
JHtml::_('behavior.framework');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
        var url = "'.JURI::base().'"
    ' );

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$user = JFactory::getUser();

$user = JFactory::getUser();
$app = JFactory::getApplication();

if ($user->guest) {
    $app->redirect('index.php');
}

//fb( $this->state );
?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=liquidador&layout=list');?>" method="post" name="adminForm" id="adminForm">

    <div class="clr"> </div>

    <h1>Reportes</h1>

    <table class="table table-striped reportes">
        <thead>
            <tr>
                
                <th class="center nowrap">
                    Fecha de creación del reporte
                </th>
                <th class="center nowrap">
                    Nombre del reporte
                </th>
                <th class="center nowrap">
                    Enlace al archivo PDF
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="9">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
        <?php foreach ($this->items as $i => $item) :
        ?>
            <tr class="row<?php echo $i % 2; ?>">
                <td class="center"><?php echo date('Y-m-d',$item->fecha)?></td>
                <td class="center"><?php echo ucwords($item->nombre)?></td>
                <td class="center"><a class="pdf" id="pdf-<?php echo $item->id ?>" href="index.php?option=com_nomina&task=empleados.downloadFile&filename=<?php echo $item->pdf_file ?>"><?php echo ucwords($item->nombre)?></a></td>
            </tr>
        <?php 
        endforeach; 
        ?>
        </tbody>
    </table>

    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <input type="hidden" name="boxchecked" value="0" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>