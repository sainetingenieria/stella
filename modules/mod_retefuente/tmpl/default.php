<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$url = JURI::root();

?>


<div class="wrapper-retefuente">

	<form id="retenciones-form">
		<ul class="retefuente">
			<li><label>Nit</label><span class="required">*</span></li>
			<li><input type="text" name="nit"></li>
		</ul>

		<input type="submit" value="Consultar" class="btn-primary">
	</form>

	<div class="message">
		<div class="success"></div>

		<div class="error"></div>

	</div>

	<a href="#" id="back-button">Volver</a>
</div>