<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$url = JURI::root();

?>


<div class="wrapper-retefuente">

	<form id="laboral-form">
		<ul class="retefuente">
			<li><label>Cedula de Ciudadania</label></li>
			<li><input type="text" name="cedula"></li>
		</ul>

		<input type="submit" value="Consultar" class="btn-primary">
	</form>

	<div class="message">
		<div class="success"></div>

		<div class="error"></div>

	</div>

	<a href="#" id="back-button">Volver</a>
</div>