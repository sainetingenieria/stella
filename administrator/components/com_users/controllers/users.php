<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Users list controller class.
 *
 * @since  1.6
 */
class UsersControllerUsers extends JControllerAdmin
{
	/**
	 * @var    string  The prefix to use with controller messages.
	 * @since  1.6
	 */
	protected $text_prefix = 'COM_USERS_USERS';

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.6
	 * @see     JController
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		$this->registerTask('block', 'changeBlock');
		$this->registerTask('unblock', 'changeBlock');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'User', $prefix = 'UsersModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * Method to change the block status on a record.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function changeBlock()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('block' => 1, 'unblock' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('COM_USERS_USERS_NO_ITEM_SELECTED'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

		

			// Change the state of the records.
			if (!$model->block($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{

					$this->setMessage(JText::plural('COM_USERS_N_USERS_BLOCKED', count($ids)));
				}
				elseif ($value == 0)
				{
					$this->setMessage(JText::plural('COM_USERS_N_USERS_UNBLOCKED', count($ids)));
				}
			}
		}

		$this->setRedirect('index.php?option=com_users&view=users');
	}


	


	/**
	 * Method to activate a record.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function activate()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids = $this->input->get('cid', array(), 'array');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('COM_USERS_USERS_NO_ITEM_SELECTED'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			
			foreach ($ids as $key => $id) {
				$user = JFactory::getUser($id);

				$empresa = $model->getEmpresa($user->nit);

				// get method mailer
				$mail = JFactory::getMailer();

				// construct body mail
				$contenido = "<div style='font-weight: 200;font-family: Roboto, sans-serif;font-size:16px; color: #7B7B7B; width: 100%;background-image: url(".JURI::root()."images/correo/fondo-correo.png)'><div style='background: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:13px;display:inline-block;'></div><br><br><img src='".JURI::root()."images/correo/logo-correo.png'><div style='padding-left:14px;padding-right:14px;text-align:justify;'><br><br>";

			 	$contenido .= "Hola ,".$user->username."<br><br>Gracias por registrarte en Stella  Te informamos que ha sido aprobado tu acceso a Stella para ".$empresa->razon_social.". Ahora puedes iniciar sesión con los datos de registro que utilizaste en ".JURI::root().".<br><br> Nombre de Usuario: ".$user->username."<br> Contraseña: Su contraseña<br><br> ".JURI::root().".<br><br>";	

				$contenido .= "</div><br><br><img style='margin: 0 auto;display:block;' src='".JURI::root()."images/correo/datos.png'><br><div style='background-image: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:18px;display:inline-block;'></div>";


				$config = JFactory::getConfig();
				$sender = array( 
				   	$config->get( 'mailfrom' ),
				   	$config->get( 'fromname' )
				);

				// set parameters to correct send mail
				$mail->setSender( $sender );
				$mail->addRecipient( $user->email );
				$mail->setSubject( "Tu nueva cuenta  en Stella" );
				$mail->Encoding = 'base64';
				$mail->isHtml( true );
				$mail->setBody( $contenido );
				//send mail
				$mail->Send();
			}

			// Change the state of the records.
			if (!$model->activate($ids))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				$this->setMessage(JText::plural('COM_USERS_N_USERS_ACTIVATED', count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_users&view=users');
	}
}
