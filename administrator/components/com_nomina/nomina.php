<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . '/controller.php' );
require_once( JPATH_COMPONENT . '/helpers/misc.php' );

require_once( JPATH_COMPONENT . '/helpers/helper.php' );
require_once( JPATH_COMPONENT . '/helpers/vendor/autoload.php' );
require_once( JPATH_COMPONENT . '/helpers/uploader.php' );
require_once( JPATH_COMPONENT . '/helpers/nomina-generator.php' );
require_once( JPATH_COMPONENT . '/helpers/reporte-generator.php' );
require_once( JPATH_COMPONENT . '/helpers/costo-generator.php' );





$controller = JControllerLegacy::getInstance('Nomina');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>