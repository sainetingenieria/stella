<?php

/**
 * Model for "Object"
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.model' );

// Initializes the Class
class NominaModelEmpleados extends JModelList {

	/**
	 * [$id description]
	 * @var [type]
	 */
	var $id;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nombres;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $apellidos;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $id_empresa;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $numero_identificacion;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $tipo_documento;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $lugar_de_expedicion;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $fecha_de_exp;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $activo;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $fecha_nacimiento;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $ciudad_de_nacimiento;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $direccion_residencia;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $ciudad_de_residencia;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $pais_residencia;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $telefono_residencia;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $celular;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $correo_corporativo;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $correo_personal;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $estado_civil;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nombre_empresa;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $fecha_de_ingreso;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $fecha_terminacion;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $cargo;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $departamento;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $proyecto;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $jefe_inmediato;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $direccion_de_trabajo;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $telefono_trabajo;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $ext;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $ciudad_trabajo;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $pais_trabajo;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $eps;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $arl;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nivel_de_riesgo;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $pension;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $caja;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $cesantias;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $conyuge_o_contacto;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $documento;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $telefono;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $ciudad;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $mail;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $numero_de_hijos;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nombre;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $identificacion;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $fecha_de_nacimiento;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nombre2;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $identificacion2;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $fecha_de_nacimiento2;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nombre3;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $identificacion3;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $fecha_de_nacimiento3;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nombre4;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $identificacion4;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $fecha_de_nacimiento4;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $nombre5;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $identificacion5;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $fecha_de_nacimiento5;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $salario;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $promedio_pagos_salariales;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $aux_transporte;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $salario_integral;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $aux_alimentacion;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $auxilio_de_educacion;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $auxilio_de_vivienda;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $otros_auxilios;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $tipo_contrato;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $corte_de_vacaciones;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $dias_tomados;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $corte_cesantias;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $corte_prima;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $lunes_entrada;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $lunes_salida;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $martes_entrada;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $martes_salida;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $miercoles_entrada;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $miercoles_salida;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $jueves_entrada;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $jueves_salida;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $viernes_entrada;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $viernes_salida;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $sabado_entrada;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $sabado_salida;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $domingo_entrada;
	/**
	 * [$id description]
	 * @var [type]
	 */
	var $domingo_salida;

	/**
	 * Attribute state
	 * @var bool
	 */
	var $state;
	
	/**
	 * Get if the object exists
	 * @var bool
	 */
	var $exists = false;
	
	/**
	 * Cache of results, data, queries
	 * @var unknown
	 */
	var $data;

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = '#__nomina_empleados';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = 'filter.component.';


	
	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			'id'
		,	'nombres'
		,	'apellidos'
		,	'id_empresa'
		,	'numero_identificacion'
		,	'tipo_documento'
		,	'lugar_de_expedicion'
		,	'fecha_de_exp'
		,	'activo'
		,	'fecha_nacimiento'
		,	'ciudad_de_nacimiento'
		,	'direccion_residencia'
		,	'ciudad_de_residencia'
		,	'pais_residencia'
		,	'telefono_residencia'
		,	'celular'
		,	'correo_corporativo'
		,	'correo_personal'
		,	'estado_civil'
		,	'nombre_empresa'
		,	'fecha_de_ingreso'
		,	'fecha_terminacion'
		,	'cargo'
		,	'departamento'
		,	'proyecto'
		,	'jefe_inmediato'
		,	'direccion_de_trabajo'
		,	'telefono_trabajo'
		,	'ext'
		,	'ciudad_trabajo'
		,	'pais_trabajo'
		,	'eps'
		,	'arl'
		,	'nivel_de_riesgo'
		,	'pension'
		,	'caja'
		,	'cesantias'
		,	'conyuge_o_contacto'
		,	'documento'
		,	'telefono'
		,	'ciudad'
		,	'mail'
		,	'numero_de_hijos'
		,	'nombre'
		,	'identificacion'
		,	'fecha_de_nacimiento'
		,	'nombre2'
		,	'identificacion2'
		,	'fecha_de_nacimiento2'
		,	'nombre3'
		,	'identificacion3'
		,	'fecha_de_nacimiento3'
		,	'nombre4'
		,	'identificacion4'
		,	'fecha_de_nacimiento4'
		,	'nombre5'
		,	'identificacion5'
		,	'fecha_de_nacimiento5'
		,	'salario'
		,	'promedio_pagos_salariales'
		,	'aux_transporte'
		,	'salario_integral'
		,	'aux_alimentacion'
		,	'auxilio_de_educacion'
		,	'auxilio_de_vivienda'
		,	'otros_auxilios'
		,	'tipo_contrato'
		,	'corte_de_vacaciones'
		,	'dias_tomados'
		,	'corte_cesantias'
		,	'corte_prima'
		,	'lunes_entrada'
		,	'lunes_salida'
		,	'martes_entrada'
		,	'martes_salida'
		,	'miercoles_entrada'
		,	'miercoles_salida'
		,	'jueves_entrada'
		,	'jueves_salida'
		,	'viernes_entrada'
		,	'viernes_salida'
		,	'sabado_entrada'
		,	'sabado_salida'
		,	'domingo_entrada'
		,	'domingo_salida'

	);

	/**
	 * Constructor
	 * 
	 * @param { array || int } the args to instance the model or the single id
	 * 
	 */
	public function __construct()
	{
	    parent::__construct();


	    if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'nit', 'a.nit',
				'razon_social', 'a.razon_social',
				'digito_verificacion', 	'a.digito_verificacion',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Adjust the context to support modal layouts.
		if ($layout = JRequest::getVar('layout', 'default'))
		{
			$this->context .= '.'.$layout;
		}

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.razon_social', 'razon_social');
		$this->setState('filter.razon_social', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.nit', 'nit');
		$this->setState('filter.nit', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.digito_verificacion', 'digito_verificacion');
		$this->setState('filter.digito_verificacion', $search);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_nomina');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'ASC');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.razon_social');
		$id	.= ':'.$this->getState('filter.nit');
		$id	.= ':'.$this->getState('filter.digito_verificacion');

		return parent::getStoreId($id);
	}
		
	public function instance( $config = NULL ){
		
		if( is_numeric( $config ) )
			$config = array( $this->attrs_map[ 0 ] => $config );
		
		if( ! is_array( $config ) )
			return;
		
		// Get existing object if the id was passed through
		return $this->fill( $config );
	}
	
	/**
	 * Fill the model attributes with the passed arguments.
	 *
	 * @param { arr } Object arguments
	 */
	protected function fill( $args = NULL ){


		if ( ! is_array( $args ) )
			return false;
		
	
		// Get object in DB			
		if ( is_numeric( $args[ $this->attrs_map[ 0 ] ] ) ){

			$object = $this->getObject( $args[ $this->attrs_map[ 0 ] ] );
			
			if( is_object( $object ) ){
				foreach ( $this->attrs_map as $attr ) {
					
					if ( isset( $object->$attr ) )
						$this->$attr = $object->$attr;
				}
			}

		}

	
		// Merge attributes	when id is not passed through
		foreach ( $this->attrs_map as $attr ) {
			if ( isset( $args[ $attr ] ) )
				$this->$attr = $args[ $attr ];
		}

		// Set exists to true.
		$this->exists = true;
	
	}
	
	
	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getObject( $id = 1 ){
		
		if( ! is_numeric( $id ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		
		$c = get_called_class();  

		$query->select( '*' );
		$query->from( $c::TABLE );
		$query->where( $this->attrs_map[ 0 ].' = ' . $id );
		
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}
	
	/**
	 * Get Objects collection
	 *
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 **/
	public function getObjects( $wheres = NULL , $prlimits = array() , $params ){
		
		if( !isset( $prlimits[ 'limitstart' ] ) )
			$prlimits[ 'limitstart' ] = $this->getState('list.start');
			
		if( !isset( $prlimits[ 'limit' ] ) )	
			$prlimits[ 'limit' ] = $this->getState('list.limit');
		
		$result = array();
		
		if( ! is_array( $wheres ) )
			$wheres = array();
			
		if( ! is_array( $params ) )	
			$params = array();
		
		$query = $this->buildQuery( $wheres , $params );
		//fb( $query->__toString() );
		$this->data = $this->_getList( $query , $prlimits[ 'limitstart' ] , $prlimits[ 'limit' ] );
		$this->counts = $this->_getListCount($query); 
		
		foreach ( $this->data as $obj ){

			$args = array();
			
			foreach ( $obj as $key => $attr ) {
				
				$args[ $key ] = $obj->$key;
			}
			
			$estaClase = get_class( $this );
	
			$object = new $estaClase();
			$object->fill( $args );
			array_push( $result, $object );
			
		}
	
		return $result;
	
	}

	/**
	 * Save a new object or update an exist.
	 *
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the query string calling the insert query
	 *
	 */
	public function save( $return = 'string' ){

		// Initialize
		$db = JFactory::getDbo();
		$response = ( object ) array();
		$model = ( object ) array();

		if( ! is_string( $return ) )
			$return = 'string';

		// Fetch the model attributes with the $model's var
		foreach ( $this->attrs_map as $attribute ) {
			$model->$attribute = $this->$attribute;
		}
		
		$c = get_called_class(); 
		
		
		$id =  $this->attrs_map[0];

		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( $model->$id == NULL || $model->$id == "" ){

			if (! $db->insertObject( $c::TABLE, $model ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el registro. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el registro. " . $db->stderr();
					return $response;
				}
			}
			$this->insertId = $db->insertid();

			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( $c::TABLE, $model, $this->attrs_map[ 0 ], false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	/**
	 * Delete object from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function delete(){
		
		$firstkey = $this->attrs_map[ 0 ];
	
		if( ! is_numeric( $this->$firstkey ) )
			return false;
	
	
		// Delete existing object if the id was passed through
		$c = get_called_class();  

		$query = "DELETE FROM ". $c::TABLE ." WHERE ". $this->attrs_map[ 0 ] . " = " . $this->$firstkey;
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}

	/**
	 * Publish or unpublish the object
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the state after change
	 *
	 */
	public function publish( $return = 'string' ){

		$db = JFactory::getDbo();
		
		$firstkey = $this->attrs_map[ 0 ];
		$secondkey = $this->attrs_map[ 1 ];
		
		$this->$secondkey = ( $this->$secondkey == 1 ) ? 0 : 1;

		// Update
		$std = new stdClass();
		$std->$firstkey = $this->$firstkey;
		$std->$secondkey = $this->$secondkey;
		
		$c = get_called_class();  

		if (! $db->updateObject( $c::TABLE, $std, $this->attrs_map[ 0 ], false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo actualizar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	/**
	 * Build a query for collection. Filters query are included.
	 *
	 * @param { array } wheres clausule. Clausule must be { key: 'value', value: 'value', condition:'=', glue: 'AND || OR' }
	 * @return { string } the query string calling the collection
	 *
	 */
	protected function buildQuery( $wheres = NULL , $params = array() ){

		// Validation
		if( ! is_array( $wheres ) )
			$wheres = array();

		// Get a storage key.
		$store = $this->getStoreId();

		// Initialize
		$db = JFactory::getDbo();
		$query  = $db->getQuery(true);
		
		$c = get_called_class();  
		
		// Query base		
		$query->select( "a.*" );
		$query->from( $db->quoteName($c::TABLE).' AS a' );
		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
			
			if( ! is_object( $clausule ) )
				continue;

			$query->where( $db->quoteName($clausule->key) . $clausule->condition . $db->quote($clausule->value) . ' '.trim($clausule->glue).' ' );

		}
		
		//query order
		if( isset($params[ 'order' ]) ){
			$query->order( $params[ 'order' ] );
		}
		else{
			// Add the list ordering clause.
			$query->order($db->escape($this->getState('list.ordering', 'a.id')).' '.$db->escape($this->getState('list.direction', 'ASC')));
		}
		
		//query group	
		if( isset($params['group']) )
			$query->group( $params[ 'group' ] );

		//Filters appending


		//filter for tipo_gestion
		$nit = $this->getState('filter.nit');
		if( !empty($nit) ){
			$query->where( $db->quoteName('a.nit') .' LIKE '. $db->quote('%'.$db->escape($nit).'%') );
		}

		// filter for razon_social
		$razon_social = $this->getState('filter.razon_social');
		if( !empty($razon_social) ){
			$query->where( $db->quoteName('a.razon_social_oec') .' LIKE '. $db->quote('%'.$db->escape($razon_social).'%') );
		}

		// filter for nit
		$digito_verificacion = $this->getState('filter.digito_verificacion');
		if( !empty($digito_verificacion) ){
			$query->where( $db->quoteName('a.digito_verificacion') .' LIKE '. $db->quote('%'.$db->escape($digito_verificacion).'%') );
		}

		// filter for descripcion de la actividad
		$descripcion = $this->getState('filter.descripcion');
		$descripcion = preg_replace('/(\n|\r|\n\r|\t|\0|\x0B)/i', '', $descripcion);
		$descripcion = trim($descripcion);	
		
		if( !empty($descripcion) ){
			$query->where( $db->quoteName('a.descripcion_actividad') .' LIKE '. $db->quote('%'.$db->escape($descripcion).'%') );
		}

		//var_dump( $query->__toString() );
		
		return $query;
	}
	
	/**
	* generar Paginador
	*/
	public function getPagination(){

		// Load the content if it doesn't already exist
        jimport('joomla.html.pagination');
        $_pagination = new JPagination( $this->counts , $this->getState('list.start'), $this->getState('list.limit') );

        return $_pagination;
	}
	
	 /*
	 * obtener usuario que creo o el que actualizo el registro
	 * @param { array } un array de objetos con las consultas
	 * @return { object , array } objeto o arrray de objectos
	 */
	 public function getUserCreate( $wheres = NULL ){
		 
		 if( ! is_array( $wheres ) )
		 	$wheres = array();
		 
		 $db = JFactory::getDbo();
		 $query = $db->getQuery(true);
		 
		 $query->select('*');
		 $query->from( '#__users' );
		 
		 $query->where( 'id = '. $this->created_by );
		 
		 foreach( $wheres as $key=>$where ){
			 
			 if( !is_object( $where ) )
			 	continue;
			 
			 $query->where( $where->key . $where->condition . $where->value . $where->glue );
		 }
		 
		 $db->setQuery( $query );
		 
		 
		return $db->loadObject();
	 }

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	protected function API(){
		
		// Instance an object with defaults
		$Object = new ComponentModelObject();
		
		// Instance an object with args not ID
		$args = array(
				'attribue'	=> 'any'
		);
		
		$Object = new ComponentModelObject();
		$Object->instance( $args );
		$Object->save(); // saves a new item
		
		//---------------
		// Instance an object with args with the ID
		// Result will be the model from DB merge the fields passed
		$args = array(
					'id' 		=> 1
				,	'tema' 		=> 1
				,	'titulo'		=> 'example'
				,	'ruta' 		=> 'pdf_test.pdf'
				,	'ano'		=> '2013'
				,	'state'		=>	1 // state pusblished
		);
		
		$Object = new ComponentModelObject();
		$Object->instance( $args );
		$Object->save(); // It will update the object with the id
		$Object->delete(); // It will delete the object with the id

		// Instance an object with args with the ID		
		$Object = new ComponentModelObject( 1 );

		// To get the objects with the table
		$Object = new ComponentModelObject();
		$object->getObjects();


		// To get the objects with conditions
		$wheres = array(
			0 => ( object ) array(
					'key' => 'id'
				,	'value' => '3'
				,	'condition' => '='
				,	'glue' => 'AND'
			)
		);

		$Object = new ComponentModelObject();
		$Object->getObjects( $wheres );
	}
	
	
	

	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getEmpleado( $id = 1 ){
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*' );
		$query->from( self::TABLE );
		$query->where( 'numero_identificacion = ' . $db->quote($id) );

		
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}
	
	
}
?>