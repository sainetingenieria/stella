<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

// Begining of the controller
class NominaControllerRetenciones extends JControllerLegacy{

	/**
	*
	* Uploads and the excels files
	*
	*/
	public function uploadExcel(){


		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'xlsx' );
		// max file size in bytes
		$sizeLimit = 8 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../excels/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}
	
	/**
	 * 
	 * @return [type]
	 */
	public function parseExcel(){

		$data = JRequest::getVar( 'data' );

		$response = ( object )array();
		$response->log = '';

		$response->status = 200;
		$response->message = 'Controlador Alcanzado';
		$response->sent = $data;
		$response->excel = $data['excel'];

		$inputFileName = '../excels/' . $data['excel'];

		// get the file uploaded
		if( ! file_exists($inputFileName) ){

			$response->status = 500;
			$response->log .= 'Archivo no encontrado.<br>';
			$response->message = 'No se puede acceder al archivo que has subido.';

			echo json_encode( $response );
			die();
		}

	    $response = $this->saveExcel( $inputFileName, $response);

	    $response->log .= '<br>';


		// delete the file
		unlink( $inputFileName );

		echo json_encode( $response );
		die();

	}


	/**
	 * @param  [type]
	 * @param  [type]
	 * @return [type]
	 */
	public function saveExcel( $inputFileName, $response ){

		// if( ! is_object( $data ) )
		// 	return false;

		$reader = ReaderFactory::create(Type::XLSX);
		$reader->open($inputFileName);

		foreach ($reader->getSheetIterator() as $sheet) {
		    foreach ($sheet->getRowIterator() as $key => $rows) {

		    	if ($key == 1) {
		    		continue;
		    	}

		    	$row = array(
						'nit_empresa_retenedora' 					=> $rows[0]
					,	'razon_social_empresa_retenedora' 			=> $rows[1]
					,	'direccion' 								=> $rows[2]
					,	'ciudad_empresa_retenedora' 				=> $rows[3]
					,	'nit_empresa_beneficiaria' 					=> $rows[4]
					,	'razon_social_empresa_beneficiaria' 		=> $rows[5]
					,	'correo_electronico_empresa_beneficiaria' 	=> $rows[6]
					,	'base_1'									=> $rows[7]
					,	'retencion_1'								=> $rows[8]
					,	'base_2'									=> $rows[9]
					,	'retencion_2' 								=> $rows[10]
					,	'base_3' 									=> $rows[11]
					,	'retencion_3' 								=> $rows[12]
					,	'base_4' 									=> $rows[13]
					,	'retencion_4' 								=> $rows[14]
					,	'base_5' 									=> $rows[15]
					,	'retencion_5' 								=> $rows[16]
					,	'base_6' 									=> $rows[17]
					,	'retencion_6' 								=> $rows[18]
					,	'base_7' 									=> $rows[19]
					,	'retencion_7' 								=> $rows[20]
					,	'base_8' 									=> $rows[21]
					,	'retencion_8' 								=> $rows[22]
					,	'base_9' 									=> $rows[23]
					,	'retencion_9' 								=> $rows[24]
					,	'base_10' 									=> $rows[25]
					,	'retencion_10' 								=> $rows[26]
					,	'base_11' 									=> $rows[27]
					,	'retencion_11' 								=> $rows[28]
					,	'base_12' 									=> $rows[29]
					,	'retencion_12' 								=> $rows[30]
					,	'base_13' 									=> $rows[31]
					,	'retencion_13' 								=> $rows[32]
					,	'base_14' 									=> $rows[33]
					,	'retencion_14' 								=> $rows[34]
					,	'base_15' 									=> $rows[35]
					,	'retencion_15' 								=> $rows[36]
					,	'base_16' 									=> $rows[37]
					,	'retencion_16' 								=> $rows[38]
					,	'base_17' 									=> $rows[39]
					,	'retencion_17' 								=> $rows[40]
					,	'base_18' 									=> $rows[41]
					,	'retencion_18' 								=> $rows[42]
					,	'base_19' 									=> $rows[43]
					,	'retencion_19' 								=> $rows[44]
					,	'base_20' 									=> $rows[45]
					,	'retencion_20' 								=> $rows[46]
					,	'base_21' 									=> $rows[47]
					,	'retencion_21' 								=> $rows[48]
					,	'base_22' 									=> $rows[49]
					,	'retencion_22' 								=> $rows[50]
					,	'base_23' 									=> $rows[51]
					,	'retencion_23' 								=> $rows[52]
					,	'base_24' 									=> $rows[53]
					,	'retencion_24' 								=> $rows[54]
					,	'base_25' 									=> $rows[55]
					,	'retencion_25' 								=> $rows[56]
					,	'base_26' 									=> $rows[57]
					,	'retencion_26' 								=> $rows[58]
					,	'base_27' 									=> $rows[59]
					,	'retencion_27' 								=> $rows[60]
					,	'base_28' 									=> $rows[61]
					,	'retencion_28' 								=> $rows[62]
					,	'base_29' 									=> $rows[63]
					,	'retencion_29' 								=> $rows[64]
					,	'base_30' 									=> $rows[65]
					,	'retencion_30' 								=> $rows[66]
					,	'base_31' 									=> $rows[67]
					,	'retencion_31' 								=> $rows[68]
					,	'base_32' 									=> $rows[69]
					,	'retencion_32' 								=> $rows[70]
					,	'base_33' 									=> $rows[71]
					,	'retencion_33' 								=> $rows[72]
					,	'base_34' 									=> $rows[73]
					,	'retencion_34' 								=> $rows[74]
					,	'base_35' 									=> $rows[75]
					,	'retencion_35' 								=> $rows[76]
					,	'base_36' 									=> $rows[77]
					,	'retencion_36' 								=> $rows[78]
					,	'base_37' 									=> $rows[79]
					,	'retencion_37' 								=> $rows[80]
					,	'base_38' 									=> $rows[81]
					,	'retencion_38' 								=> $rows[82]
					,	'base_39' 									=> $rows[83]
					,	'retencion_39' 								=> $rows[84]
					,	'base_40' 									=> $rows[85]
					,	'retencion_40' 								=> $rows[86]
					,	'base_41' 									=> $rows[87]
					,	'retencion_41' 								=> $rows[88]
					,	'base_42' 									=> $rows[89]
					,	'retencion_42' 								=> $rows[90]
				);

		    	$modelRetenciones = $this->getModel('retenciones');

				$modelRetenciones->instance($row);


				if( ! $modelRetenciones->save( 'bool' ) ){
					$response->log .= '<tr><td style="background-color: red;">El registro en la fila :'.$key.' no se pudo guardar</td></tr><br>';
					continue;
				}else {
					$response->log .= '<tr><td style="background-color: greenyellow;">El registro en la fila : '. $key.' se ha guardado correctamente.</td></tr><br>';
				}

		    }
		}
 
		$response->status = 200;
		$response->message = "Archivo convertido y guardado correctamente.";

		return $response;
	}

	/**
	* Truncate the table
	*
	*/
	public function truncate(){

		$response = ( object )array();

		$model = $this->getModel( 'retenciones' );

		$model->truncate();

		$response->status = 200;
		$response->message = "";

		echo json_encode( $response );
		die();

	}

	/**
	* Deletes all temporary excel files
	*
	*/
	public function deleteTemp(){

		$response = (object)array();

		// delete excels folder
		system('/bin/rm -rf ' . escapeshellarg( '../excels' ) );

		// re create excels folder
		mkdir( '../excels' );

		$response->status = 200;
		$response->message = "Archivos temporales y antiguos borrados correctamente.";

		echo json_encode( $response );
		die();	
	}
}
?>