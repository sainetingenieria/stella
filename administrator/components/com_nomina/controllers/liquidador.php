<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

// Begining of the controller
class NominaControllerLiquidador extends JControllerLegacy{

	/**
	*
	* Uploads and the excels files
	*
	*/
	public function uploadExcel(){


		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'xlsx' );
		// max file size in bytes
		$sizeLimit = 8 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../excels/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}
	
	/**
	 * 
	 * @return [type]
	 */
	public function parseExcel(){

		$data = JRequest::getVar( 'data' );

		$response = ( object )array();
		$response->log = '';

		$response->status = 200;
		$response->message = 'Controlador Alcanzado';
		$response->sent = $data;
		$response->excel = $data['excel'];

		$inputFileName = '../excels/' . $data['excel'];

		// get the file uploaded
		if( ! file_exists($inputFileName) ){

			$response->status = 500;
			$response->log .= 'Archivo no encontrado.<br>';
			$response->message = 'No se puede acceder al archivo que has subido.';

			echo json_encode( $response );
			die();
		}

	    $response = $this->saveExcel( $inputFileName, $response);

	    $response->log .= '<br>';


		// delete the file
		unlink( $inputFileName );

		echo json_encode( $response );
		die();

	}


	/**
	 * @param  [type]
	 * @param  [type]
	 * @return [type]
	 */
	public function saveExcel( $inputFileName, $response ){

		// if( ! is_object( $data ) )
		// 	return false;

		$reader = ReaderFactory::create(Type::XLSX);
		$reader->open($inputFileName);

		$writer = WriterFactory::create(Type::XLSX); // for XLSX files

		$fileName = time().'_liquidacion.xlsx';
		$filePath = JPATH_ROOT.'/administrator/liquidaciones/'.$fileName;

		$writer->openToFile($filePath); // write data to a file or to a PHP stream

		$multipleRows = array(
			array(
					'Periodo Liquidacion'
				,	'Cédula de Ciudadanía'
				,	'Nombre del empleado'
				,	'Sueldo básico'
				,	'Días liquidados'
				,	'Básico'
				,	'Auxilio de transporte'
				,	'Horas extras'
				,	'Otros auxilios'
				,	'Comisiones'
				,	'Total devengado'
				,	'Salud'
				,	'Pension'
				,	'Fondo de solidaridad'
				,	'Retención en la fuente'
				,	'Otras deducciones'
				,	'Total deducciones'
				,	'Total pagos'
				,	'Neto pagado'
				,	'Salud'
				,	'Riesgos profesionales'
				,	'Fondo de pensiones'
				,	'ICBF'
				,	'SENA'
				,	'Subsidio familiar'
				,	'Auxilio de Alimentación'
				,	'Cesantias'
				,	'Intereses sobre cesantías'
				,	'Prima de servicios'
				,	'Vacaciones'
			)
		);

		$writer->addRows($multipleRows); // add multiple rows at a time

		$results = array();

		foreach ($reader->getSheetIterator() as $sheet) {
		    foreach ($sheet->getRowIterator() as $key => $rows) {

		    	if ($key == 1) {
		    		continue;
		    	}

		    	$row = array(
						'A' => $rows[0]
					,	'B' => $rows[1]
					,	'C' => $rows[2]
					,	'D' => $rows[3]
					,	'E' => $rows[4]
					,	'F' => $rows[5]
					,	'G' => $rows[6]
					,	'H' => $rows[7]
					,	'I' => $rows[8]
					,	'J' => $rows[9]
					,	'K' => $rows[10]
					,	'L' => $rows[11]
					,	'M' => $rows[12]
					,	'N' => $rows[13]
					,	'O' => $rows[14]
					,	'P' => $rows[15]
					,	'Q' => $rows[16]
					,	'R' => $rows[17]
					,	'S' => $rows[18]
					,	'T' => $rows[19]
					,	'U' => $rows[20]
					,	'V' => $rows[21]
					,	'W' => $rows[22]
					,	'X' => $rows[23]
					,	'Y' => $rows[24]
					,	'Z' => $rows[25]
					,	'AA' => $rows[26]
					,	'AB' => $rows[27]
					,	'AC' => $rows[28]
					,	'AD' => $rows[29]
					,	'AE' => $rows[30]
					,	'AF' => $rows[31]
					,	'AG' => $rows[32]
					,	'AH' => $rows[33]
					,	'AI' => $rows[34]
					,	'AJ' => $rows[35]
					,	'AK' => $rows[36]
					,	'AL' => $rows[37]
					,	'AM' => $rows[38]
					,	'AN' => $rows[39]
					,	'AO' => $rows[40]
					,	'AP' => $rows[41]
					,	'AQ' => $rows[42]
					,	'AR' => $rows[43]
					,	'AS' => $rows[44]
					,	'AT' => $rows[45]
					,	'AU' => $rows[46]
					,	'AV' => $rows[47]
					,	'AW' => $rows[48]
					,	'AX' => $rows[49]
					,	'AY' => $rows[50]
					,	'AZ' => $rows[51]
					,	'BA' => $rows[52]
					,	'BB' => $rows[53]
					,	'BC' => $rows[54]
					,	'BD' => $rows[55]
					,	'BE' => $rows[56]
					,	'BF' => $rows[57]
					,	'BG' => $rows[58]
					,	'BH' => $rows[59]
					,	'BI' => $rows[60]
					,	'BJ' => $rows[61]
					,	'BK' => $rows[62]

				);

		    	$modelEmpleados = $this->getModel('empleados');

		    	$modelEmpresa = $this->getModel('empresas');

		    	$empresa = $modelEmpresa->getEmpresaByNit( $row['A'] );

		    	if (empty($empresa)) {
					$response->log .= "<tr><td style='background-color: red;'>El nit ".$row['A']." de la fila ".$key." no se encuentra en la base de datos.</td></tr><br>";
					continue;
		    	}

		    	$periodo = 0;

				$fecha_inicio_incapacidad_laboral = 0;
				$fecha_fin_incapacidad_laboral = 0;
				$fecha_inicio_incapacidad_no_laboral = 0;
				$fecha_fin_incapacidad_no_laboral = 0;
				$fecha_inicio_licencia_remunerada = 0;
				$fecha_fin_licencia_remunerada = 0;
				$fecha_inicio_licencia_no_remunerada = 0;
				$fecha_fin_licencia_no_remunerada = 0;

				if (!empty($row['B'])) {

		    		$row['B'] = trim($row['B']);
		    		list($d,$m,$y) = explode('/', $row['B']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato del Periodo no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$periodo = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['AH'])) {

		    		$row['AH'] = trim($row['AH']);
		    		list($d,$m,$y) = explode('/', $row['AH']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato Fecha inicio incapacidad laboral no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_inicio_incapacidad_laboral = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['AI'])) {

		    		$row['AI'] = trim($row['AI']);
		    		list($d,$m,$y) = explode('/', $row['AI']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato Fecha fin incapacidad laboral no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_fin_incapacidad_laboral = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['AJ'])) {

		    		$row['AJ'] = trim($row['AJ']);
		    		list($d,$m,$y) = explode('/', $row['AJ']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la Fecha inicio incapacidad no laboral no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_inicio_incapacidad_no_laboral = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['AK'])) {

		    		$row['AK'] = trim($row['AK']);
		    		list($d,$m,$y) = explode('/', $row['AK']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la Fecha fin incapacidad no laboral no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_fin_incapacidad_no_laboral = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['AL'])) {

		    		$row['AL'] = trim($row['AL']);
		    		list($d,$m,$y) = explode('/', $row['AL']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la Fecha inicio licencia remunerada no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_inicio_licencia_remunerada = strtotime(date($y.$m.$d));

		    	}


		    	if (!empty($row['AM'])) {

		    		$row['AM'] = trim($row['AM']);
		    		list($d,$m,$y) = explode('/', $row['AM']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la Fecha fin licencia remunerada no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_fin_licencia_remunerada = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['AN'])) {

		    		$row['AN'] = trim($row['AN']);
		    		list($d,$m,$y) = explode('/', $row['AN']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la Fecha inicio licencia no remunerada no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_inicio_licencia_no_remunerada = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['AO'])) {

		    		$row['AO'] = trim($row['AO']);
		    		list($d,$m,$y) = explode('/', $row['AO']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la Fecha fin licencia no remunerada no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_fin_licencia_no_remunerada = strtotime(date($y.$m.$d));

		    	}


		    	$entrada_dia1 = 0;
				$salida_dia1 = 0;
				$entrada_dia2 = 0;
				$salida_dia2 = 0;
				$entrada_dia3 = 0;
				$salida_dia3 = 0;
				$entrada_dia4 = 0;
				$salida_dia4 = 0;
				$entrada_dia5 = 0;
				$salida_dia5 = 0;
				$entrada_dia6 = 0;
				$salida_dia6 = 0;
				$entrada_dia7 = 0;
				$salida_dia7 = 0;
				$entrada_dia8 = 0;
				$salida_dia8 = 0;
				$entrada_dia9 = 0;
				$salida_dia9 = 0;
				$entrada_dia10 = 0;
				$salida_dia10 = 0;
				$entrada_dia11 = 0;
				$salida_dia11 = 0;
				$entrada_dia12 = 0;
				$salida_dia12 = 0;
				$entrada_dia13 = 0;
				$salida_dia13 = 0;
				$entrada_dia14 = 0;
				$salida_dia14 = 0;
				$entrada_dia15 = 0;
				$salida_dia15 = 0;

		    	if (!empty($row['D'])) {

		    		$row['D'] = trim($row['D']);
		    		list($h,$i) = explode(':', $row['D']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 1 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia1 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['E'])) {

		    		$row['E'] = trim($row['E']);
		    		list($h,$i) = explode(':', $row['E']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 1 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia1 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 1;

		    	}

		    	if (!empty($row['F'])) {

		    		$row['F'] = trim($row['F']);
		    		list($h,$i) = explode(':', $row['F']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 2 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia2 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['G'])) {

		    		$row['G'] = trim($row['G']);
		    		list($h,$i) = explode(':', $row['G']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 2 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia2 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 3;

		    	}

		    	if (!empty($row['H'])) {

		    		$row['H'] = trim($row['H']);
		    		list($h,$i) = explode(':', $row['H']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 3 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia3 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['I'])) {

		    		$row['I'] = trim($row['I']);
		    		list($h,$i) = explode(':', $row['I']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 3 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia3 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 3;

		    	}

		    	if (!empty($row['J'])) {

		    		$row['J'] = trim($row['J']);
		    		list($h,$i) = explode(':', $row['J']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 4 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia4 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['K'])) {

		    		$row['K'] = trim($row['K']);
		    		list($h,$i) = explode(':', $row['K']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 4 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia4 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 4;

		    	}

		    	if (!empty($row['L'])) {

		    		$row['L'] = trim($row['L']);
		    		list($h,$i) = explode(':', $row['L']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 5 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia5 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['M'])) {

		    		$row['M'] = trim($row['M']);
		    		list($h,$i) = explode(':', $row['M']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 5 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia5 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 5;

		    	}

		    	if (!empty($row['N'])) {

		    		$row['N'] = trim($row['N']);
		    		list($h,$i) = explode(':', $row['N']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 6 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia6 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['O'])) {

		    		$row['O'] = trim($row['O']);
		    		list($h,$i) = explode(':', $row['O']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 6 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia6 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 6;

		    	}

		    	if (!empty($row['P'])) {

		    		$row['P'] = trim($row['P']);
		    		list($h,$i) = explode(':', $row['P']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 7 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia7 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['Q'])) {

		    		$row['Q'] = trim($row['Q']);
		    		list($h,$i) = explode(':', $row['Q']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 7 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia7 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 7;

		    	}

		    	if (!empty($row['R'])) {

		    		$row['R'] = trim($row['R']);
		    		list($h,$i) = explode(':', $row['R']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 8 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia8 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['S'])) {

		    		$row['S'] = trim($row['S']);
		    		list($h,$i) = explode(':', $row['S']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 8 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia8 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 8;

		    	}

		    	if (!empty($row['T'])) {

		    		$row['T'] = trim($row['T']);
		    		list($h,$i) = explode(':', $row['T']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 9 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia9 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['U'])) {

		    		$row['U'] = trim($row['U']);
		    		list($h,$i) = explode(':', $row['U']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 9 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia9 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 9;

		    	}

		    	if (!empty($row['V'])) {

		    		$row['V'] = trim($row['V']);
		    		list($h,$i) = explode(':', $row['V']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 10 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia10 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['W'])) {

		    		$row['W'] = trim($row['W']);
		    		list($h,$i) = explode(':', $row['W']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 10 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia10 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 10;

		    	}

		    	if (!empty($row['X'])) {

		    		$row['X'] = trim($row['X']);
		    		list($h,$i) = explode(':', $row['X']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 11 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia11 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['Y'])) {

		    		$row['Y'] = trim($row['Y']);
		    		list($h,$i) = explode(':', $row['Y']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 11 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia11 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 11;

		    	}

		    	if (!empty($row['Z'])) {

		    		$row['Z'] = trim($row['Z']);
		    		list($h,$i) = explode(':', $row['Z']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 12 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia12 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['AA'])) {

		    		$row['AA'] = trim($row['AA']);
		    		list($h,$i) = explode(':', $row['AA']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 12 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia12 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 12;

		    	}

		    	if (!empty($row['AB'])) {

		    		$row['AB'] = trim($row['AB']);
		    		list($h,$i) = explode(':', $row['AB']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 13 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia13 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['AC'])) {

		    		$row['AC'] = trim($row['AC']);
		    		list($h,$i) = explode(':', $row['AC']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 13 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia13 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 13;

		    	}

		    	if (!empty($row['AD'])) {

		    		$row['AD'] = trim($row['AD']);
		    		list($h,$i) = explode(':', $row['AD']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 14 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia14 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['AE'])) {

		    		$row['AE'] = trim($row['AE']);
		    		list($h,$i) = explode(':', $row['AE']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 14 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia14 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 14;

		    	}

		    	if (!empty($row['AF'])) {

		    		$row['AF'] = trim($row['AF']);
		    		list($h,$i) = explode(':', $row['AF']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día 15 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$entrada_dia15 = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['AG'])) {

		    		$row['AG'] = trim($row['AG']);
		    		list($h,$i) = explode(':', $row['AG']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día 15 no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$salida_dia15 = strtotime(date($h.':'.$i));
		    		$dias_liquidados = 15;

		    	}

		    	$empleado = $modelEmpleados->getEmpleado($row['C']);

		    	if (empty($empleado)) {
					$response->log .= "<tr><td style='background-color: red;'>El empleado con identificacion ".$row['C']." de la fila ".$key." no se encuentra en la base de datos.</td></tr><br>";
					continue;
		    	}

		    	$modelVariables = $this->getModel('variables');

		    	$data = array();
   
  				$variables = $modelVariables->getObjects();
  				$salario = $variables[0]->salario_minimo;

  				$nivelesRiesgo = array(
						'1' => $variables[0]->clase_riesgo1
					,	'2' => $variables[0]->clase_riesgo2
					,	'3' => $variables[0]->clase_riesgo3
					,	'4' => $variables[0]->clase_riesgo4
					,	'5' => $variables[0]->clase_riesgo5
				);


  				$periodo = date('Y/m/d',$periodo);

				$periodo1 = strtotime ( '-16 day' , strtotime ( $periodo ) ) ;

				$periodo1 = date ( 'Y/m/d' , $periodo1 );

				$data['periodo'] = $periodo1.' - '.$periodo;
  				$data['cedula_ciudadania'] = $empleado->numero_identificacion;
  				$data['nombres'] = ucfirst($empleado->nombres) . ' '.ucfirst($empleado->apellidos);
  				$data['sueldo_basico'] = $empleado->salario;
  				$data['dias_liquidados'] = $dias_liquidados;

  				//Sueldo: (salario/30)*(30-días de ausencia)
  				$data['basico'] = ($empleado->salario / 30) * (30 / $row['AP']);

  				// Aux. transporte: Sí Transporte(empleado) = s; entonces aux. transporte(global)/30*(30-días de ausencia)
		    	$data['aux_transporte'] = 0;
		    	if (strtolower($row['AV']) == 's') {
		    		$data['aux_transporte'] = ($variables[0]->aux_transporte / 30) * (30 - $row['AP']);
		    	}

		    	$data['horas_extras'] = 0;

		    	//Otros auxilios: alimentación + educación + vivienda + otros.
		    	$data['otros_auxilios'] = $row['AT'] + $row['AU'] + $row['AW'] + $row['AX'];

		    	$data['comisiones'] = $row['AS'];

		    	$data['total_devengado'] = 0;
		    	//Salud: 0.04 * ( sueldo+aux. transporte + otros auxilios)
		    	$data['salud_descuento'] = 0.04 * ($data['basico'] + $data['aux_transporte'] + $data['otros_auxilios']);


		    	//Pension: 0.04 * ( sueldo+aux. transporte + otros auxilios)
		    	$data['pension'] = 0.04 * ($data['basico'] + $data['aux_transporte'] + $data['otros_auxilios']);

		    	//Fondo de solidaridad: ( sueldo+aux. transporte + otros auxilios) * (según rango)
		    	$rango_sueldo = 0;

				if ($data['sueldo_basico'] >= ($salario * 4) || $data['sueldo_basico'] < ($salario * 16)) {
					$rango_basico = $variables[0]->rangos_fondo1;
				}

				if ($data['sueldo_basico'] >= ($salario * 16) || $data['sueldo_basico'] < ($salario * 17)) {
					$rango_basico = $variables[0]->rangos_fondo2;
				}

				if ($data['sueldo_basico'] >= ($salario * 17) || $data['sueldo_basico'] < ($salario * 18)) {
					$rango_basico = $variables[0]->rangos_fondo3;
				}

				if ($data['sueldo_basico'] >= ($salario * 18) || $data['sueldo_basico'] < ($salario * 19)) {
					$rango_basico = $variables[0]->rangos_fondo4;
				}

				if ($data['sueldo_basico'] >= ($salario * 19) || $data['sueldo_basico'] < ($salario * 20)) {
					$rango_basico = $variables[0]->rangos_fondo5;
				}

				if ($data['sueldo_basico'] >= ($salario * 20)) {
					$rango_sueldo = $variables[0]->rangos_fondo6;
				}

			
				$data['fondo_solidaridad'] = $data['basico'] * ($rango_sueldo / 100);


				$data['retencion_fuente'] = $row['BJ'];
				$data['otras_deducciones'] = $row['BK'];

				$pagos_salariales = ($data['basico'] + $data['horas_extras'] + $data['recargos'] + $data['comisiones'] + $row['AQ']);
				$pagos_no_salariales = ($row['AT'] + $row['AU'] + $data['aux_transporte'] + $row['AW'] + $row['AX'] + $row['AR']);


				$data['total_deducciones'] = $data['salud_descuento'] + $data['pension'] + $data['fondo_solidaridad'] + $data['retencion_fuente'] + $data['otras_deducciones'] + $row['AY'] + $row['BB'];

				$data['total_pagos'] = $pagos_salariales + $pagos_no_salariales;

				$data['neto_pagado'] = $data['total_pagos'] - $data['total_deducciones'];

				if ($empleado->salario > ($salario * 10)) {
					$data['salud_empresa'] = $data['total_pagos'] * 0.085;
					$data['icbf'] = $data['total_pagos'] * 0.03;
					$data['sena'] = $data['total_pagos'] * 0.02;
					
				}else{
					$data['salud_empresa'] = 0;
				}
				
				$data['riesgos_profesionales'] = (($data['basico'] / 30) * $data['dias_liquidados']) * $nivelesRiesgo[$empleado->nivel_de_riesgo];

				$data['fondo_pensiones'] = $data['total_pagos'] * 0.12;
				$data['subsidio_familiar'] = $data['salud_descuento'] + $data['pension'] + $data['fondo_solidaridad'] + $data['retencion_fuente'] + $data['otras_deducciones'];
				$data['aux_alimentacion'] = $row['AT'];

				$data['cesantias'] = ($data['total_pagos'] + $data['aux_transporte']) * 0.083;
				$data['intereses_cesantias'] = $data['cesantias'] * 0.01;
				$data['vacaciones'] = ($empleado->salario) * 0.0416;
				$data['prima'] = ($data['total_pagos'] + $data['aux_transporte']) * 0.083;

				$writer->addRow($data); // add a row at a time

				$data['id_empleado'] = $empleado->id;
				$data['id_empresa'] = $empresa->id;
				$data['valor'] = $data['neto_pagado'];

				array_push($results, $data);

		    }
		}
		

		$writer->close();
		$results['excel'] = $fileName;

		$session = JFactory::getSession();
		$session->set('results', $results);	


 		$response->results = $results;
		$response->status = 200;
		$response->message = "Archivo convertido y guardado correctamente.";

		return $response;
	}

	/**
	 * Show tpl edit to specific company
	 * @return display edit
	 */
	public function edit(){

		$id = JRequest::getVar('cid');

		$model = $this->getModel('empresas');

		$empresa = $model->getObject($id[0]);

		$view = $this->getView('empresas', 'html');

		$view->setLayout('edit');

		$view->assignRef('empresa', $empresa);

		$view->display();
	}

	/**
	 * Save edit company
	 * @return redirect to page list
	 */
	public function save(){

		$data = JFactory::getApplication()->input->post->getArray();

		$model = $this->getModel('empresas');

		$model->instance($data);

		$link = 'index.php?option=com_nomina&view=empresas&layout=list';
		$app = JFactory::getApplication();

		if (! $model->save('bool')) {
			$msg = 'Hubo un error al guardar en la base de datos por favor intente más tarde';
			return $app->redirect($link, $msg, $msgType='error');
		}

		$msg = 'El registro se actualizo correctamente';
		return $app->redirect($link, $msg, $msgType='message');

	

	}

	/**
	 *  Task that deletes the list of company
	 */
	public function delete(){
		
		//Het the items picked
		$pks = JRequest::getVar( 'cid' );

		//Instance a model for each item and delete
		foreach ( $pks as $id ) {
			
			// Call the model
			$model = $this->getModel( 'empresas' );
			
			$model->instance( $id );

			$model->delete();
			
		}

		$app = JFactory::getApplication();
		$link = 'index.php?option=com_nomina&view=empresas&layout=list';
		$msg = 'Registros eliminados';
		return $app->redirect($link, $msg, $msgType='message');

		return;
	}

	/**
	* Truncate the table
	*
	*/
	public function truncate(){

		$response = ( object )array();

		// $model = $this->getModel( 'usuarios' );

		//$model->truncate();

		$response->status = 200;
		$response->message = "";

		echo json_encode( $response );
		die();

	}

	/**
	* Deletes all temporary excel files
	*
	*/
	public function deleteTemp(){

		$response = (object)array();

		// delete excels folder
		system('/bin/rm -rf ' . escapeshellarg( '../excels' ) );

		// re create excels folder
		mkdir( '../excels' );

		$response->status = 200;
		$response->message = "Archivos temporales y antiguos borrados correctamente.";

		echo json_encode( $response );
		die();	
	}


	public function sendNotificationLiquidador(){

		$config = JFactory::getConfig();
		$mail = JFactory::getMailer();

		$sender = array(
        	$config->get('mailfrom'),
        	$config->get('fromname'),
        );

        $mail->setSender($sender);
        $mail->addRecipient($config->get('mailfrom'));
        $mail->setSubject('Novedades de Liquidador de Nomina');
        $mail->Encoding = 'base64';
        $mail->isHtml($ishtml = true);

        $contenido = "<div style='font-weight: 200;font-family: Roboto, sans-serif;font-size:15px; color: #7B7B7B; width: 100%;background-image: url(".JURI::root()."images/correo/fondo-correo.png)'><div style='background: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:13px;display:inline-block;'></div><br><br><img src='".JURI::root()."images/correo/logo-correo.png'><div style='padding-left:14px;padding-right:14px;text-align:justify;'><br><br>

       	Buen día,<br><br>";

        $contenido .= "No olvides subir el archivo de novedades para realizar la liquidación del próximo periodo, a continuación podrás descargar el archivo necesario para subir las novedades al Stella.<br><br>";
        $contenido .= "Para subir las novedades ingresa a:<br><br>";
        $contenido .= "1. Ingresar al <a href='".JURI::root()."administrator'>administrador de contenidos Joomla</a><br>";
        $contenido .= "2. Iniciar sesión con un usuario y contraseña.<br>";
        $contenido .= "3. Una vez dentro del administrador ir a la parte superior en el menú Componentes.<br>";
        $contenido .= "4. Dentro del menú encontrará una opción llamada 'Nomina' y dentro de esta una opción llamada 'Generar Nomina'.<br><br>";

        $contenido .= "Te agradecemos enviarla lo antes posible.<br><br>";

        $contenido .= "Feliz día.<br><br>";

        $contenido .= "</div><br><br><img style='margin: 0 auto;display:block;' src='".JURI::root()."images/correo/datos.png'><br><div style='background-image: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:18px;display:inline-block;'></div>";

        $mail->setBody($contenido);

        // enviar email
        $mail->Send();

	}

	public function saveNomina(){

		$id = JRequest::getVar('id');

		$session = JFactory::getSession();
		$results = $session->get('results');

		$modelLiquidacion = $this->getModel('liquidador');
		$modelEmpleadoLiquidaciones = $this->getModel('liquidaciones');

		$valor = 0;

		foreach ($results as $key => $result) {
			$valor += $result['valor'];
		}

		$periodo = explode('-', $results[0]['periodo']);

		list($y,$m,$d) = explode('/', $periodo[1]);
		$periodo = strtotime(date($y.$m.$d));

		$argsLiquidacion = array(
				'id' => $id
			,	'id_empresa' => $results[0]['id_empresa']
			,	'fecha' => strtotime(date('y-m-d'))
			,	'valor' => $valor
			,	'periodo' => $periodo
		);

		$modelLiquidacion->instance($argsLiquidacion);

		$link = 'index.php?option=com_nomina&view=liquidador&layout=liquidaciones';
		$app = JFactory::getApplication();

		if (!$modelLiquidacion->save('bool')) {
			$msg = 'No se pudo cargar la nomina intente más tarde';
			return $app->redirect($link, $msg, $msgType='error');
		}

		$insertId = (!empty($id)) ? $id : $modelLiquidacion->insertId;

		foreach ($results as $key => $result) {

			if (!is_array($result)) {
				continue;
			}

			$nominaExistente = $modelEmpleadoLiquidaciones->getNominaByLiquidacion($result['id_empleado'], $insertId);
			
			$empleados = array(
					'id' => $nominaExistente->id
				,	'id_empleado' => $result['id_empleado']
				,	'id_liquidacion' => $insertId
				,	'sueldo_basico' => $result['sueldo_basico']
				,	'dias_liquidados' => $result['dias_liquidados']
				,	'basico' => $result['basico']
				,	'aux_transporte' => $result['aux_transporte']
				,	'horas_extras' => $result['horas_extras']
				,	'otros_auxilios' => $result['otros_auxilios']
				,	'comisiones' => $result['comisiones']
				,	'total_devengado' => $result['total_devengado']
				,	'salud_descuento' => $result['salud_descuento']
				,	'pension' => $result['pension']
				,	'fondo_solidaridad' => $result['fondo_solidaridad']
				,	'retencion_fuente'	 => $result['retencion_fuente'	]
				,	'otras_deducciones' => $result['otras_deducciones']
				,	'total_deducciones' => $result['total_deducciones']
				,	'neto_pagado' => $result['neto_pagado']
				,	'salud_empresa' => $result['salud_empresa']
				,	'riesgos_profesionales' => $result['riesgos_profesionales']
				,	'fondo_pensiones' => $result['fondo_pensiones']
				,	'icbf' => $result['icbf']
				,	'sena' => $result['sena']
				,	'subsidio_familiar' => $result['subsidio_familiar']
				,	'aux_alimentacion' => $result['aux_alimentacion']
				,	'cesantias' => $result['cesantias']
				,	'intereses_cesantias' => $result['intereses_cesantias']
				,	'prima' => $result['prima']
				,	'vacaciones' => $result['vacaciones']
			);


			$modelEmpleadoLiquidaciones->instance($empleados);

		
			if (!$modelEmpleadoLiquidaciones->save('bool')) {
				$msg = 'No se pudo cargar la nomina intente más tarde';
				return $app->redirect($link, $msg, $msgType='error');
			}
		}

		$msg = 'La nomina se guardo correctamente';
		return $app->redirect($link, $msg, $msgType='message');


	}

	public function downloadNomina(){

		$id = JRequest::getVar('id');

		$modelLiquidacion = $this->getModel('liquidador');
		$modelEmpleadoLiquidaciones = $this->getModel('liquidaciones');
		$modelEmpleados = $this->getModel('empleados');


		$liquidacion = $modelLiquidacion->getObject($id);

		$empleados = $modelEmpleadoLiquidaciones->getLiquidaciones($id);

		$writer = WriterFactory::create(Type::XLSX); // for XLSX files

		$fileName = time().'_liquidacion.xlsx';
		$filePath = JPATH_ROOT.'/administrator/liquidaciones/'.$fileName;

		$writer->openToFile($filePath); // write data to a file or to a PHP stream

		$multipleRows = array(
			array(
					'Periodo Liquidacion'
				,	'Cédula de Ciudadanía'
				,	'Nombre del empleado'
				,	'Sueldo básico'
				,	'Días liquidados'
				,	'Básico'
				,	'Auxilio de transporte'
				,	'Horas extras'
				,	'Comisiones'
				,	'Total devengado'
				,	'Salud'
				,	'Pension'
				,	'Fondo de solidaridad'
				,	'Retención en la fuente'
				,	'Otras deducciones'
				,	'Total deducciones'
				,	'Neto pagado'
				,	'Salud'
				,	'Riesgos profesionales'
				,	'Fondo de pensiones'
				,	'ICBF'
				,	'SENA'
				,	'Subsidio familiar'
				,	'Cesantia'
				,	'Intereses sobre cesantías'
				,	'Prima de servicios'
				,	'Vacaciones'
			)
		);

		$writer->addRows($multipleRows); // add multiple rows at a time

		foreach ($empleados as $key => $empleado) {
			$data = array();

			$empl = $modelEmpleados->getEmpleado($empleado->id_empleado);

			$periodo = date('Y/m/d',$liquidacion->periodo);

			$periodo1 = strtotime ( '-16 day' , strtotime ( $periodo ) ) ;

			$periodo1 = date ( 'Y/m/d' , $periodo1 );

			$data['periodo'] = $periodo1.' - '.$periodo;

			// $data['periodo'] = $liquidacion->periodo;
			$data['cedula_ciudadania'] = $empleado->cedula_ciudadania;
			$data['nombres'] = ucfirst($empl->nombres) . ' '.ucfirst($empl->apellidos);
  			$data['sueldo_basico'] = $empleado->sueldo_basico;
			$data['dias_liquidados'] = $empleado->dias_liquidados;
			$data['basico'] = $empleado->basico;
	    	$data['aux_transporte'] = $empleado->aux_transporte;
	    	$data['horas_extras'] = $empleado->horas_extras;
	    	$data['comisiones'] = $empleado->comisiones;
	    	$data['total_devengado'] = $empleado->total_devengado;
	    	$data['salud_descuento'] = $empleado->salud_descuento;
	    	$data['pension'] = $empleado->pension;
			$data['fondo_solidaridad'] = $empleado->fondo_solidaridad;
			$data['retencion_fuente'] = $empleado->retencion_fuente;
			$data['otras_deducciones'] = $empleado->otras_deducciones;
			$data['total_deducciones'] = $empleado->total_deducciones;
			$data['neto_pagado'] = $empleado->neto_pagado;
			$data['salud_empresa'] = $empleado->salud_empresa;
			$data['riesgos_profesionales'] = $empleado->riesgos_profesionales;
			$data['fondo_pensiones'] = $empleado->fondo_pensiones;
			$data['icbf'] = $empleado->icbf;
			$data['sena'] = $empleado->sena;
			$data['subsidio_familiar'] = $empleado->subsidio_familiar;
			$data['cesantias'] = $empleado->cesantias;
			$data['intereses_cesantias'] = $empleado->intereses_cesantias;
			$data['prima'] = $empleado->prima;
			$data['vacaciones'] = $empleado->vacaciones;

			$writer->addRow($data); // add a row at a time
		}

		$writer->close();

		$app = JFactory::getApplication();

		$link = JURI::root().'administrator/liquidaciones/'.$fileName;

		return $app->redirect($link);

	}

	public function enviarDesprendibles(){


		$id = JRequest::getVar('id');

		$modelLiquidacion = $this->getModel('liquidador');
		$modelEmpleadoLiquidaciones = $this->getModel('liquidaciones');
		$modelEmpleados = $this->getModel('empleados');
		$modelEmpresa = $this->getModel('empresas');
		$liquidacion = $modelLiquidacion->getObject($id);

		$empresa = $modelEmpresa->getObject($liquidacion->id_empresa);

		$empleados = $modelEmpleadoLiquidaciones->getLiquidaciones($id);

		foreach ($empleados as $key => $empleado) {
			$data = array();

			$empl = $modelEmpleados->getObject($empleado->id_empleado);

			$data['razon_social'] = ucfirst($empresa->razon_social);
			$data['nit'] = ucfirst($empresa->nit);

			$data['cargo'] = ucfirst($empl->cargo);

			$data['periodo'] = $liquidacion->periodo;
			$data['cedula_ciudadania'] = $empleado->cedula_ciudadania;
			$data['nombres'] = ucfirst($empl->nombres) . ' '.ucfirst($empl->apellidos);
  			$data['sueldo_basico'] = Misc::numberDots($empleado->sueldo_basico);
			$data['dias_liquidados'] = Misc::numberDots($empleado->dias_liquidados);
			$data['basico'] = Misc::numberDots($empleado->basico);
	    	$data['aux_transporte'] = Misc::numberDots($empleado->aux_transporte);
	    	$data['horas_extras'] = Misc::numberDots($empleado->horas_extras);
	    	$data['otros_auxilios'] = Misc::numberDots($empleado->otros_auxilios);
	    	$data['comisiones'] = Misc::numberDots($empleado->comisiones);
	    	$data['total_devengado'] = Misc::numberDots($empleado->total_devengado);
	    	$data['salud_descuento'] = Misc::numberDots($empleado->salud_descuento);
	    	$data['pension'] = Misc::numberDots($empleado->pension);
			$data['fondo_solidaridad'] = Misc::numberDots($empleado->fondo_solidaridad);
			$data['retencion_fuente'] = Misc::numberDots($empleado->retencion_fuente);
			$data['otras_deducciones'] = Misc::numberDots($empleado->otras_deducciones);
			$data['total_deducciones'] = Misc::numberDots($empleado->total_deducciones);
			$data['total_pagos'] = Misc::numberDots($empleado->total_pagos);
			$data['neto_pagado'] = Misc::numberDots($empleado->neto_pagado);
			$data['salud_empresa'] = Misc::numberDots($empleado->salud_empresa);
			$data['riesgos_profesionales'] = Misc::numberDots($empleado->riesgos_profesionales);
			$data['fondo_pensiones'] = Misc::numberDots($empleado->fondo_pensiones);
			$data['icbf'] = Misc::numberDots($empleado->icbf);
			$data['sena'] = Misc::numberDots($empleado->sena);
			$data['subsidio_familiar'] = Misc::numberDots($empleado->subsidio_familiar);
			$data['aux_alimentacion'] = Misc::numberDots($empleado->aux_alimentacion);
			$data['cesantias'] = Misc::numberDots($empleado->cesantias);
			$data['intereses_cesantias'] = Misc::numberDots($empleado->intereses_cesantias);
			$data['prima'] = Misc::numberDots($empleado->prima);
			$data['vacaciones'] = Misc::numberDots($empleado->vacaciones);

			$file_name = nominaGen::getPdf($data);

			$config = JFactory::getConfig();
			$mail = JFactory::getMailer();

			$sender = array(
	        	$config->get('mailfrom'),
	        	$config->get('fromname'),
	        );

	        $mail->setSender($sender);
	        $mail->addRecipient($empl->correo_personal);
	        $mail->setSubject('Tu desprendible de nómina  '.$data['nombres']);
	        $mail->Encoding = 'base64';
	        $mail->isHtml($ishtml = true);

	        $contenido = "<div style='font-weight: 200;font-family: Roboto, sans-serif;font-size:15px; color: #7B7B7B; width: 100%;background-image: url(".JURI::root()."images/correo/fondo-correo.png)'><div style='background: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:13px;display:inline-block;'></div><br><br><img src='".JURI::root()."images/correo/logo-correo.png'><div style='padding-left:14px;padding-right:14px;text-align:justify;'><br><br>

	        Estimado ".$data['nombres'].", <br><br>";

	        $contenido .= "Te informamos que tu desprendible de nómina de ".$data['razon_social']."está adjunto en este correo.Te agradecemos hacernos llegar una copia de éste firmada por ti. <br><br><br>";
	        $contenido .= "Cualquier duda escríbenos a nomina@humbertogonzalez.com.co y la resolveremos lo antes posible.<br><br>";
	        $contenido .= "</div><br><br><img style='margin: 0 auto;display:block;' src='".JURI::root()."images/correo/datos.png'><br><div style='background-image: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:18px;display:inline-block;'></div>";


	        $mail->setBody($contenido);

	        $mail->addAttachment($file_name);

	        // enviar email
	        $mail->Send();
		}

		$app = JFactory::getApplication();
		$msg = 'Desprendibles de nomina enviados';
		$link = 'index.php?option=com_nomina&view=liquidador&layout=liquidaciones';
		return $app->redirect($link, $msg, $msgType='message');

	}

	public function imageConvert(){

        $response = new stdClass();

        $data = JRequest::getVar('data');

        $data = explode(',', $data);

        $data = base64_decode($data[1]);
        
        //generate password
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($a = 0; $a < 6; $a++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        // // delete images folder
        // system('/bin/rm -rf ' . escapeshellarg(JPATH_ROOT.'/images/graficos/'));

        // // re create images folder
        // mkdir(JPATH_ROOT.'/images/graficos/');


        file_put_contents(JPATH_ROOT.'/images/graficos/'.$randomString.'.png', $data);

        $response->filename = $randomString.'.png';
        echo json_encode($response);
        die;
    }

}
?>