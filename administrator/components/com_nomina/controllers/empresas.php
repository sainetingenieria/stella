<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

// Begining of the controller
class NominaControllerEmpresas extends JControllerLegacy{

	/**
	*
	* Uploads and the excels files
	*
	*/
	public function uploadExcel(){


		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'xlsx' );
		// max file size in bytes
		$sizeLimit = 8 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../excels/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}
	
	/**
	 * 
	 * @return [type]
	 */
	public function parseExcel(){

		$data = JRequest::getVar( 'data' );

		$response = ( object )array();
		$response->log = '';

		$response->status = 200;
		$response->message = 'Controlador Alcanzado';
		$response->sent = $data;
		$response->excel = $data['excel'];

		$inputFileName = '../excels/' . $data['excel'];

		// get the file uploaded
		if( ! file_exists($inputFileName) ){

			$response->status = 500;
			$response->log .= 'Archivo no encontrado.<br>';
			$response->message = 'No se puede acceder al archivo que has subido.';

			echo json_encode( $response );
			die();
		}

	    $response = $this->saveExcel( $inputFileName, $response);

	    $response->log .= '<br>';


		// delete the file
		unlink( $inputFileName );

		echo json_encode( $response );
		die();

	}


	/**
	 * @param  [type]
	 * @param  [type]
	 * @return [type]
	 */
	public function saveExcel( $inputFileName, $response ){

		// if( ! is_object( $data ) )
		// 	return false;

		$reader = ReaderFactory::create(Type::XLSX);
		$reader->open($inputFileName);

		foreach ($reader->getSheetIterator() as $sheet) {
		    foreach ($sheet->getRowIterator() as $key => $rows) {

		    	if ($key == 1) {
		    		continue;
		    	}

		    	$row = array(
						'A' => $rows[0]
					,	'B' => $rows[1]
					,	'C' => $rows[2]
					,	'D' => $rows[3]
					,	'E' => $rows[4]
					,	'F' => $rows[5]
					,	'G' => $rows[6]
					,	'H' => $rows[7]
					,	'I' => $rows[8]
					,	'J' => $rows[9]
					,	'K' => $rows[10]
				);

		    	$modelEmpresa = $this->getModel('empresas');

		    	$empresa = $modelEmpresa->getEmpresaByNit($row['B']);

				if (empty($empresa)) {

					$argsEmpresa  = array(
							'razon_social' => $row['A']
						,	'nit' => $row['B']
						,	'digito_verificacion' => $row['C']
						,	'direccion' => $row['D']
						,	'ciudad' => $row['E']
						,	'telefono' => $row['F']
						,	'imagen_firma' => $row['G']
						,	'imagen_membrete' => $row['H']
						,	'encargado' => $row['I']
						,	'cargo' => $row['J']
					);

				}else{

					$argsEmpresa  = array(
							'id' => $empresa->id
						,	'razon_social' => $row['A']
						,	'nit' => $row['B']
						,	'digito_verificacion' => $row['C']
						,	'direccion' => $row['D']
						,	'ciudad' => $row['E']
						,	'telefono' => $row['F']
						,	'imagen_firma' => $row['G']
						,	'imagen_membrete' => $row['H']
						,	'encargado' => $row['I']
						,	'cargo' => $row['J']
					);

				}
				
				$modelEmpresa->instance($argsEmpresa);

				if( ! $modelEmpresa->save( 'bool' ) ){
					$response->log .= '<tr><td style="background-color: red;">La empresa :'.$row['A'].' no se pudo guardar</td></tr><br>';
					continue;
				}
				if (!empty($empresa)) {
					$response->log .= "<tr><td style='background-color: orange;'>" . 'Datos de la empresa  con el Nit' . $row['B'] . ' ya existe y sus datos fueron actualizados correctamente.</td></tr><br>';
					continue;
				} else {
					$response->log .= '<tr><td style="background-color: greenyellow;">La empresa con NIT: '. $row['B'].' Razón Social: '.$row['A'].' se ha guardado correctamente.</td></tr><br>';
				}

		    }
		}
 
		$response->status = 200;
		$response->message = "Archivo convertido y guardado correctamente.";

		return $response;
	}

	/**
	 * Show tpl edit to specific company
	 * @return display edit
	 */
	public function edit(){

		$id = JRequest::getVar('cid');

		$model = $this->getModel('empresas');

		$empresa = $model->getObject($id[0]);

		$view = $this->getView('empresas', 'html');

		$view->setLayout('edit');

		$view->assignRef('empresa', $empresa);

		$view->display();
	}

	/**
	 * Save edit company
	 * @return redirect to page list
	 */
	public function save(){

		$data = JFactory::getApplication()->input->post->getArray();

		$model = $this->getModel('empresas');

		$model->instance($data);

		$link = 'index.php?option=com_nomina&view=empresas&layout=list';
		$app = JFactory::getApplication();

		if (! $model->save('bool')) {
			$msg = 'Hubo un error al guardar en la base de datos por favor intente más tarde';
			return $app->redirect($link, $msg, $msgType='error');
		}

		$msg = 'El registro se actualizo correctamente';
		return $app->redirect($link, $msg, $msgType='message');

	

	}

	/**
	 *  Task that deletes the list of company
	 */
	public function delete(){
		
		//Het the items picked
		$pks = JRequest::getVar( 'cid' );

		//Instance a model for each item and delete
		foreach ( $pks as $id ) {
			
			// Call the model
			$model = $this->getModel( 'empresas' );
			
			$model->instance( $id );

			$model->delete();
			
		}

		$app = JFactory::getApplication();
		$link = 'index.php?option=com_nomina&view=empresas&layout=list';
		$msg = 'Registros eliminados';
		return $app->redirect($link, $msg, $msgType='message');

		return;
	}

	/**
	* Truncate the table
	*
	*/
	public function truncate(){

		$response = ( object )array();

		// $model = $this->getModel( 'usuarios' );

		//$model->truncate();

		$response->status = 200;
		$response->message = "";

		echo json_encode( $response );
		die();

	}

	/**
	* Deletes all temporary excel files
	*
	*/
	public function deleteTemp(){

		$response = (object)array();

		// delete excels folder
		system('/bin/rm -rf ' . escapeshellarg( '../excels' ) );

		// re create excels folder
		mkdir( '../excels' );

		$response->status = 200;
		$response->message = "Archivos temporales y antiguos borrados correctamente.";

		echo json_encode( $response );
		die();	
	}
}
?>