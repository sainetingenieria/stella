<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );



// Begining of the controller
class NominaControllerReportes extends JControllerLegacy{


	public function asignar(){

		$data = JFactory::getApplication()->input->post->getArray();

		$modelLiquidacion = $this->getModel('liquidador');
		$modelEmpleadoLiquidaciones = $this->getModel('liquidaciones');

		$liquidaciones = $modelLiquidacion->getLiquidacionByEmpresa($data);

		$dataArray = array();

		foreach ($liquidaciones as $key => $liquidacion) {
			$empleados = $modelEmpleadoLiquidaciones->getItems( $liquidacion->id );

			$dataArray = array_merge($empleados, $dataArray);

		}

		$view = $this->getView('reportes','html');

		$view->assignRef('asign', $data);
		$view->assignRef('items', $dataArray);

		$view->setLayout('asignar');
		$view->display();

	}

	public function filter(){

		$data = JFactory::getApplication()->input->post->getArray();


		$modelLiquidacion = $this->getModel('liquidador');
		$modelEmpleadoLiquidaciones = $this->getModel('liquidaciones');

		$liquidaciones = $modelLiquidacion->getLiquidacionByEmpresa($data);

		$dataArray = array();

		foreach ($liquidaciones as $key => $liquidacion) {
			$empleados = $modelEmpleadoLiquidaciones->getItems( $liquidacion->id, $data );

			$dataArray = array_merge($empleados, $dataArray);

		}

		$view = $this->getView('reportes','html');

		$view->assignRef('asign', $data);
		$view->assignRef('items', $dataArray);

		$view->setLayout('asignar');
		$view->display();
	}

	public function save(){

		$data = JFactory::getApplication()->input->post->getArray();

		$modelLiquidacion = $this->getModel('liquidador');
		$modelEmpleadoLiquidaciones = $this->getModel('liquidaciones');

		$liquidaciones = $modelLiquidacion->getLiquidacionByEmpresa($data);

		$dataArray = array();

		foreach ($liquidaciones as $key => $liquidacion) {
			$empleados = $modelEmpleadoLiquidaciones->getItems( $liquidacion->id, $data );

			$dataArray = array_merge($empleados, $dataArray);

		}

		if ($data['tipo_reporte'] == '1') {
			$file_name = reporteGen::getPdf($dataArray);
		}

		if ($data['tipo_reporte'] == '3') {
			$file_name = costoGen::getPdf($dataArray, $data);
		}

		$reportesModel = $this->getModel('reportes');

		$arrayReporte = array(
				'id_empresa' => $data['empresa']
			,	'pdf_file' => $file_name
			,	'fecha' => strtotime(date('y-m-d'))
			,	'nombre' => $data['nombre']
		);

		$app = JFactory::getApplication();
		$link = 'index.php?option=com_nomina&view=reportes';

		$reportesModel->instance($arrayReporte);

		if (!$reportesModel->save('bool')) {
			$msg = 'No se pudo asignar el reporte intente de nuevo';
			return $app->redirect($link, $msg, $msgType='error');
		}
		$msg = 'Reporte asignado correctamente.';
		return $app->redirect($link, $msg, $msgType='message');



	}

}
?>