<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

// Begining of the controller
class NominaControllerEmpleados extends JControllerLegacy{

	/**
	*
	* Uploads and the excels files
	*
	*/
	public function uploadExcel(){


		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'xlsx' );
		// max file size in bytes
		$sizeLimit = 8 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../excels/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}
	
	/**
	 * 
	 * @return [type]
	 */
	public function parseExcel(){

		$data = JRequest::getVar( 'data' );

		$response = ( object )array();
		$response->log = '';

		$response->status = 200;
		$response->message = 'Controlador Alcanzado';
		$response->sent = $data;
		$response->excel = $data['excel'];

		$inputFileName = '../excels/' . $data['excel'];

		// get the file uploaded
		if( ! file_exists($inputFileName) ){

			$response->status = 500;
			$response->log .= 'Archivo no encontrado.<br>';
			$response->message = 'No se puede acceder al archivo que has subido.';

			echo json_encode( $response );
			die();
		}

	    $response = $this->saveExcel( $inputFileName, $response);

	    $response->log .= '<br>';


		// delete the file
		unlink( $inputFileName );

		echo json_encode( $response );
		die();

	}


	/**
	 * @param  [type]
	 * @param  [type]
	 * @return [type]
	 */
	public function saveExcel( $inputFileName, $response ){

		// if( ! is_object( $data ) )
		// 	return false;

		$reader = ReaderFactory::create(Type::XLSX);
		$reader->open($inputFileName);

		foreach ($reader->getSheetIterator() as $sheet) {
		    foreach ($sheet->getRowIterator() as $key => $rows) {

		    	if ($key == 1) {
		    		continue;
		    	}

		    	$row = array(
						'A' => $rows[0]
					,	'B' => $rows[1]
					,	'C' => $rows[2]
					,	'D' => $rows[3]
					,	'E' => $rows[4]
					,	'F' => $rows[5]
					,	'G' => $rows[6]
					,	'H' => $rows[7]
					,	'I' => $rows[8]
					,	'J' => $rows[9]
					,	'K' => $rows[10]
					,	'L' => $rows[11]
					,	'M' => $rows[12]
					,	'N' => $rows[13]
					,	'O' => $rows[14]
					,	'P' => $rows[15]
					,	'Q' => $rows[16]
					,	'R' => $rows[17]
					,	'S' => $rows[18]
					,	'T' => $rows[19]
					,	'U' => $rows[20]
					,	'V' => $rows[21]
					,	'W' => $rows[22]
					,	'X' => $rows[23]
					,	'Y' => $rows[24]
					,	'Z' => $rows[25]
					,	'AA' => $rows[26]
					,	'AB' => $rows[27]
					,	'AC' => $rows[28]
					,	'AD' => $rows[29]
					,	'AE' => $rows[30]
					,	'AF' => $rows[31]
					,	'AG' => $rows[32]
					,	'AH' => $rows[33]
					,	'AI' => $rows[34]
					,	'AJ' => $rows[35]
					,	'AK' => $rows[36]
					,	'AL' => $rows[37]
					,	'AM' => $rows[38]
					,	'AN' => $rows[39]
					,	'AO' => $rows[40]
					,	'AP' => $rows[41]
					,	'AQ' => $rows[42]
					,	'AR' => $rows[43]
					,	'AS' => $rows[44]
					,	'AT' => $rows[45]
					,	'AU' => $rows[46]
					,	'AV' => $rows[47]
					,	'AW' => $rows[48]
					,	'AX' => $rows[49]
					,	'AY' => $rows[50]
					,	'AZ' => $rows[51]
					,	'BA' => $rows[52]
					,	'BB' => $rows[53]
					,	'BC' => $rows[54]
					,	'BD' => $rows[55]
					,	'BE' => $rows[56]
					,	'BF' => $rows[57]
					,	'BG' => $rows[58]
					,	'BH' => $rows[59]
					,	'BI' => $rows[60]
					,	'BJ' => $rows[61]
					,	'BK' => $rows[62]
					,	'BL' => $rows[63]
					,	'BM' => $rows[64]
					,	'BN' => $rows[65]
					,	'BO' => $rows[66]
					,	'BP' => $rows[67]
					,	'BQ' => $rows[68]
					,	'BR' => $rows[69]
					,	'BS' => $rows[70]
					,	'BT' => $rows[71]
					,	'BU' => $rows[72]
					,	'BV' => $rows[73]
					,	'BW' => $rows[74]
					,	'BX' => $rows[75]
					,	'BY' => $rows[76]
					,	'BZ' => $rows[77]
					,	'CA' => $rows[78]
					,	'CB' => $rows[79]
					,	'CC' => $rows[80]
					,	'CD' => $rows[81]
					,	'CE' => $rows[82]
					,	'CF' => $rows[83]
				);

		    	$modelEmpleados = $this->getModel('empleados');

		    	$modelEmpresa = $this->getModel('empresas');

		    	$empresa = $modelEmpresa->getEmpresaByNit( $row['C'] );

		    

		    	if (empty($empresa)) {
					$response->log .= "<tr><td style='background-color: red;'>El nit ".$row['C']." del empleado  :".$row['A']." ".$row['B']."(".$row['D'].") no se encuentra en la base de datos.</td></tr><br>";
					continue;
		    	}

		    	$fecha_de_exp = 0;
		    	$activo = 0;
		    	$fecha_nacimiento = 0;
		    	$fecha_de_ingreso = 0;
		    	$fecha_terminacion = 0;
		    	$fecha_de_nacimiento = 0;
		    	$fecha_de_nacimiento2 = 0;
		    	$fecha_de_nacimiento3 = 0;
		    	$fecha_de_nacimiento4 = 0;
		    	$fecha_de_nacimiento5 = 0;




		    	if (!empty($row['G'])) {

		    		$row['G'] = trim($row['G']);
		    		list($d,$m,$y) = explode('/', $row['G']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la fecha de expedicion del documento no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_de_exp = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['H'])) {
		    		$row['H'] = strtolower($row['H']);
		    		if ($row['H'] == 'a') {
		    			$activo = 1;
		    		}elseif ($row['H'] == 'i') {
		    			$activo = 0;
		    		}else{
		    			$response->log .= "<tr><td style='background-color: red;'>El estado del empleado (activo) debe ser una 'a' para activo o una 'i' para inactivo Fila ".$key."</td></tr><br>";
						continue;
		    		}
		    	}else{
		    		$response->log .= "<tr><td style='background-color: red;'>Debe digitar el estado del empleado Fila ".$key."</td></tr><br>";
					continue;
		    	}

		    	if (!empty($row['I'])) {

		    		$row['I'] = trim($row['I']);
		    		list($d,$m,$y) = explode('/', $row['I']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la fecha de nacimiento no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_nacimiento = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['U'])) {

		    		$row['U'] = trim($row['U']);
		    		list($d,$m,$y) = explode('/', $row['U']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la fecha de terminacion no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_terminacion = strtotime(date($y.$m.$d));

		    	}


		    	if (!empty($row['T'])) {

		    		$row['T'] = trim($row['T']);
		    		list($d,$m,$y) = explode('/', $row['T']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la fecha de ingreso no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_de_ingreso = strtotime(date($y.$m.$d));
		    	}

		    	if (!empty($row['AT'])) {

		    		$row['AT'] = trim($row['AT']);
		    		list($d,$m,$y) = explode('/', $row['AT']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la fecha de nacimiento del hijo 1 no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_de_nacimiento = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['AW'])) {

		    		$row['AW'] = trim($row['AW']);
		    		list($d,$m,$y) = explode('/', $row['AW']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la fecha de nacimiento del hijo 2 no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_de_nacimiento2 = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['AZ'])) {

		    		$row['AZ'] = trim($row['AZ']);
		    		list($d,$m,$y) = explode('/', $row['AZ']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la fecha de nacimiento del hijo 3 no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_de_nacimiento3 = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['BC'])) {

		    		$row['BC'] = trim($row['BC']);
		    		list($d,$m,$y) = explode('/', $row['BC']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la fecha de nacimiento del hijo 4 no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_de_nacimiento4 = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['BF'])) {

		    		$row['BF'] = trim($row['BF']);
		    		list($d,$m,$y) = explode('/', $row['BF']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la fecha de nacimiento del hijo 5 no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$fecha_de_nacimiento5 = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['BP'])) {

		    		$row['BP'] = trim($row['BP']);
		    		list($d,$m,$y) = explode('/', $row['BP']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato del corte de vacaciones no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$corte_de_vacaciones = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['BR'])) {

		    		$row['BR'] = trim($row['BR']);
		    		list($d,$m,$y) = explode('/', $row['BR']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato del corte de cesantias no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$corte_cesantias = strtotime(date($y.$m.$d));

		    	}

		    	if (!empty($row['BS'])) {

		    		$row['BS'] = trim($row['BS']);
		    		list($d,$m,$y) = explode('/', $row['BS']);

		    		if (empty($d) || empty($m) || empty($y)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato del corte de prima no es el correcto ('dd/mm/yyyy') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$corte_prima = strtotime(date($y.$m.$d));

		    	}




		    	$lunes_entrada = 0;
		    	$lunes_salida = 0;
		    	$martes_entrada = 0;
		    	$martes_salida = 0;
		    	$miercoles_entrada = 0;
		    	$miercoles_salida = 0;
		    	$jueves_entrada = 0;
		    	$jueves_salida = 0;
		    	$viernes_entrada = 0;
		    	$viernes_salida = 0;
		    	$sabado_entrada = 0;
		    	$sabado_salida = 0;
		    	$domingo_entrada = 0;
		    	$domingo_salida = 0;


		    	if (!empty($row['BT'])) {

		    		$row['BT'] = trim($row['BT']);
		    		list($h,$i) = explode(':', $row['BT']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día lunes no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$lunes_entrada = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['BU'])) {

		    		$row['BU'] = trim($row['BU']);
		    		list($h,$i) = explode(':', $row['BU']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día lunes no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$lunes_salida = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['BV'])) {

		    		$row['BV'] = trim($row['BV']);
		    		list($h,$i) = explode(':', $row['BV']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día martes no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$martes_entrada = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['BW'])) {

		    		$row['BW'] = trim($row['BW']);
		    		list($h,$i) = explode(':', $row['BW']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día martes no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$martes_salida = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['BX'])) {

		    		$row['BX'] = trim($row['BX']);
		    		list($h,$i) = explode(':', $row['BX']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día miercoles no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$miercoles_entrada = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['BY'])) {

		    		$row['BY'] = trim($row['BY']);
		    		list($h,$i) = explode(':', $row['BY']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día miercoles no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$miercoles_salida = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['BZ'])) {

		    		$row['BZ'] = trim($row['BZ']);
		    		list($h,$i) = explode(':', $row['BZ']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día jueves no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$jueves_entrada = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['CA'])) {

		    		$row['CA'] = trim($row['CA']);
		    		list($h,$i) = explode(':', $row['CA']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día jueves no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$jueves_salida = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['CB'])) {

		    		$row['CB'] = trim($row['CB']);
		    		list($h,$i) = explode(':', $row['CB']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día viernes no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$viernes_entrada = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['CC'])) {

		    		$row['CC'] = trim($row['CC']);
		    		list($h,$i) = explode(':', $row['CC']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día viernes no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$viernes_salida = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['CD'])) {

		    		$row['CD'] = trim($row['CD']);
		    		list($h,$i) = explode(':', $row['CD']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día sabado no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$sabado_entrada = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['CE'])) {

		    		$row['CE'] = trim($row['CE']);
		    		list($h,$i) = explode(':', $row['CE']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día sabado no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$sabado_salida = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['CF'])) {

		    		$row['CF'] = trim($row['CF']);
		    		list($h,$i) = explode(':', $row['CF']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la entrada del día domingo no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$domingo_entrada = strtotime(date($h.':'.$i));

		    	}

		    	if (!empty($row['CG'])) {

		    		$row['CG'] = trim($row['CG']);
		    		list($h,$i) = explode(':', $row['CG']);

		    		if (empty($h) || empty($i)) {
		    			$response->log .= "<tr><td style='background-color: red;'>El formato de la salida del día domingo no es el correcto ('HH:MM') Fila ".$key."</td></tr><br>";
						continue;
		    		}

		    		$domingo_salida = strtotime(date($h.':'.$i));

		    	}


		    	$empleado = $modelEmpleados->getEmpleado($row['D']);

				if (empty($empleado)) {

					$argsempleado  = array(
							'nombres' => $row['A']
						,	'apellidos' => $row['B']
						,	'id_empresa' => $empresa->id
						,	'numero_identificacion' => $row['D']
						,	'tipo_documento' => $row['E']
						,	'lugar_de_expedicion' => $row['F']
						,	'fecha_de_exp' => $fecha_de_exp
						,	'activo' => $activo
						,	'fecha_nacimiento' => $fecha_nacimiento
						,	'ciudad_de_nacimiento' => $row['J']
						,	'direccion_residencia' => $row['K']
						,	'ciudad_de_residencia' => $row['L']
						,	'pais_residencia' => $row['M']
						,	'telefono_residencia' => $row['N']
						,	'celular' => $row['O']
						,	'correo_corporativo' => $row['P']
						,	'correo_personal' => $row['Q']
						,	'estado_civil' => $row['R']
						,	'nombre_empresa' => $row['S']
						,	'fecha_de_ingreso' => $fecha_de_ingreso
						,	'fecha_terminacion' => $fecha_terminacion
						,	'cargo' => $row['V']
						,	'departamento' => $row['W']
						,	'proyecto' => $row['X']
						,	'jefe_inmediato' => $row['Y']
						,	'direccion_de_trabajo' => $row['Z']
						,	'telefono_trabajo' => $row['AA']
						,	'ext' => $row['AB']
						,	'ciudad_trabajo' => $row['AC']
						,	'pais_trabajo' => $row['AD']
						,	'eps' => $row['AE']
						,	'arl' => $row['AF']
						,	'nivel_de_riesgo' => $row['AG']
						,	'pension' => $row['AH']
						,	'caja' => $row['AI']
						,	'cesantias' => $row['AJ']
						,	'conyuge_o_contacto' => $row['AK']
						,	'documento' => $row['AL']
						,	'telefono' => $row['AM']
						,	'direccion' => $row['AN']
						,	'ciudad' => $row['AO']
						,	'mail' => $row['AP']
						,	'numero_de_hijos' => $row['AQ']
						,	'nombre' => $row['AR']
						,	'identificacion' => $row['AS']
						,	'fecha_de_nacimiento' => $fecha_de_nacimiento
						,	'nombre2' => $row['AU']
						,	'identificacion2' => $row['AV']
						,	'fecha_de_nacimiento2' => $fecha_de_nacimiento2
						,	'nombre3' => $row['AX']
						,	'identificacion3' => $row['AY']
						,	'fecha_de_nacimiento3' => $fecha_de_nacimiento3
						,	'nombre4' => $row['BA']
						,	'identificacion4' => $row['BB']
						,	'fecha_de_nacimiento4' => $fecha_de_nacimiento4
						,	'nombre5' => $row['BD']
						,	'identificacion5' => $row['BE']
						,	'fecha_de_nacimiento5' => $fecha_de_nacimiento5
						,	'salario' => $row['BG']
						,	'promedio_pagos_salariales' => $row['BH']
						,	'aux_transporte' => $row['BI']
						,	'salario_integral' => $row['BJ']
						,	'aux_alimentacion' => $row['BK']
						,	'auxilio_de_educacion' => $row['BL']
						,	'auxilio_de_vivienda' => $row['BM']
						,	'otros_auxilios' => $row['BN']
						,	'tipo_contrato' => $row['BO']
						,	'corte_de_vacaciones' => $corte_de_vacaciones
						,	'dias_tomados' => $row['BQ']
						,	'corte_cesantias' => $corte_cesantias
						,	'corte_prima' => $corte_prima
						,	'lunes_entrada' => $lunes_entrada
						,	'lunes_salida' => $lunes_salida
						,	'martes_entrada' => $martes_entrada
						,	'martes_salida' => $martes_salida
						,	'miercoles_entrada' => $miercoles_entrada
						,	'miercoles_salida' => $miercoles_salida
						,	'jueves_entrada' => $jueves_entrada
						,	'jueves_salida' => $jueves_salida
						,	'viernes_entrada' => $viernes_entrada
						,	'viernes_salida' => $viernes_salida
						,	'sabado_entrada' => $sabado_entrada
						,	'sabado_salida' => $sabado_salida
						,	'domingo_entrada' => $domingo_entrada
						,	'domingo_salida' => $domingo_salida
					);

				}else{

					$argsempleado  = array(
							'id' => $empleado->id
						,	'nombres' => $row['A']
						,	'apellidos' => $row['B']
						,	'id_empresa' => $empresa->id
						,	'numero_identificacion' => $row['D']
						,	'tipo_documento' => $row['E']
						,	'lugar_de_expedicion' => $row['F']
						,	'fecha_de_exp' => $fecha_de_exp
						,	'activo' => $activo
						,	'fecha_nacimiento' => $fecha_nacimiento
						,	'ciudad_de_nacimiento' => $row['J']
						,	'direccion_residencia' => $row['K']
						,	'ciudad_de_residencia' => $row['L']
						,	'pais_residencia' => $row['M']
						,	'telefono_residencia' => $row['N']
						,	'celular' => $row['O']
						,	'correo_corporativo' => $row['P']
						,	'correo_personal' => $row['Q']
						,	'estado_civil' => $row['R']
						,	'nombre_empresa' => $row['S']
						,	'fecha_de_ingreso' => $fecha_de_ingreso
						,	'fecha_terminacion' => $fecha_terminacion
						,	'cargo' => $row['V']
						,	'departamento' => $row['W']
						,	'proyecto' => $row['X']
						,	'jefe_inmediato' => $row['Y']
						,	'direccion_de_trabajo' => $row['Z']
						,	'telefono_trabajo' => $row['AA']
						,	'ext' => $row['AB']
						,	'ciudad_trabajo' => $row['AC']
						,	'pais_trabajo' => $row['AD']
						,	'eps' => $row['AE']
						,	'arl' => $row['AF']
						,	'nivel_de_riesgo' => $row['AG']
						,	'pension' => $row['AH']
						,	'caja' => $row['AI']
						,	'cesantias' => $row['AJ']
						,	'conyuge_o_contacto' => $row['AK']
						,	'documento' => $row['AL']
						,	'telefono' => $row['AM']
						,	'direccion' => $row['AN']
						,	'ciudad' => $row['AO']
						,	'mail' => $row['AP']
						,	'numero_de_hijos' => $row['AQ']
						,	'nombre' => $row['AR']
						,	'identificacion' => $row['AS']
						,	'fecha_de_nacimiento' => $fecha_de_nacimiento
						,	'nombre2' => $row['AU']
						,	'identificacion2' => $row['AV']
						,	'fecha_de_nacimiento2' => $fecha_de_nacimiento2
						,	'nombre3' => $row['AX']
						,	'identificacion3' => $row['AY']
						,	'fecha_de_nacimiento3' => $fecha_de_nacimiento3
						,	'nombre4' => $row['BA']
						,	'identificacion4' => $row['BB']
						,	'fecha_de_nacimiento4' => $fecha_de_nacimiento4
						,	'nombre5' => $row['BD']
						,	'identificacion5' => $row['BE']
						,	'fecha_de_nacimiento5' => $fecha_de_nacimiento5
						,	'salario' => $row['BG']
						,	'promedio_pagos_salariales' => $row['BH']
						,	'aux_transporte' => $row['BI']
						,	'salario_integral' => $row['BJ']
						,	'aux_alimentacion' => $row['BK']
						,	'auxilio_de_educacion' => $row['BL']
						,	'auxilio_de_vivienda' => $row['BM']
						,	'otros_auxilios' => $row['BN']
						,	'tipo_contrato' => $row['BO']
						,	'corte_de_vacaciones' => $corte_de_vacaciones
						,	'dias_tomados' => $row['BQ']
						,	'corte_cesantias' => $corte_cesantias
						,	'corte_prima' => $corte_prima
						,	'lunes_entrada' => $lunes_entrada
						,	'lunes_salida' => $lunes_salida
						,	'martes_entrada' => $martes_entrada
						,	'martes_salida' => $martes_salida
						,	'miercoles_entrada' => $miercoles_entrada
						,	'miercoles_salida' => $miercoles_salida
						,	'jueves_entrada' => $jueves_entrada
						,	'jueves_salida' => $jueves_salida
						,	'viernes_entrada' => $viernes_entrada
						,	'viernes_salida' => $viernes_salida
						,	'sabado_entrada' => $sabado_entrada
						,	'sabado_salida' => $sabado_salida
						,	'domingo_entrada' => $domingo_entrada
						,	'domingo_salida' => $domingo_salida
					);

				}
				
				$modelEmpleados->instance($argsempleado);

				if( ! $modelEmpleados->save( 'bool' ) ){
					$response->log .= '<tr><td style="background-color: red;">El empleado '.$row['A'].' '.$row['B'].'('.$row['D'].') no se pudo guardar</td></tr><br>';
					continue;
				}
				if (!empty($empleado)) {
					$response->log .= "<tr><td style='background-color: orange;'>El empleado ".$row['A'].' '.$row['B'].'('.$row['D'].') ya existe y sus datos fueron actualizados correctamente.</td></tr><br>';
					continue;
				} else {
					$response->log .= '<tr><td style="background-color: greenyellow;">El empleado '.$row['A'].' '.$row['B'].'('.$row['D'].') se ha guardado correctamente.</td></tr><br>';
				}

		    }
		}
 
		$response->status = 200;
		$response->message = "Archivo convertido y guardado correctamente.";

		return $response;
	}

	/**
	 * Show tpl edit to specific company
	 * @return display edit
	 */
	public function edit(){

		$id = JRequest::getVar('cid');

		$model = $this->getModel('empresas');

		$empresa = $model->getObject($id[0]);

		$view = $this->getView('empresas', 'html');

		$view->setLayout('edit');

		$view->assignRef('empresa', $empresa);

		$view->display();
	}

	/**
	 * Save edit company
	 * @return redirect to page list
	 */
	public function save(){

		$data = JFactory::getApplication()->input->post->getArray();

		$model = $this->getModel('empresas');

		$model->instance($data);

		$link = 'index.php?option=com_nomina&view=empresas&layout=list';
		$app = JFactory::getApplication();

		if (! $model->save('bool')) {
			$msg = 'Hubo un error al guardar en la base de datos por favor intente más tarde';
			return $app->redirect($link, $msg, $msgType='error');
		}

		$msg = 'El registro se actualizo correctamente';
		return $app->redirect($link, $msg, $msgType='message');

	

	}

	/**
	 *  Task that deletes the list of company
	 */
	public function delete(){
		
		//Het the items picked
		$pks = JRequest::getVar( 'cid' );

		//Instance a model for each item and delete
		foreach ( $pks as $id ) {
			
			// Call the model
			$model = $this->getModel( 'empresas' );
			
			$model->instance( $id );

			$model->delete();
			
		}

		$app = JFactory::getApplication();
		$link = 'index.php?option=com_nomina&view=empresas&layout=list';
		$msg = 'Registros eliminados';
		return $app->redirect($link, $msg, $msgType='message');

		return;
	}

	/**
	* Truncate the table
	*
	*/
	public function truncate(){

		$response = ( object )array();

		// $model = $this->getModel( 'usuarios' );

		//$model->truncate();

		$response->status = 200;
		$response->message = "";

		echo json_encode( $response );
		die();

	}

	/**
	* Deletes all temporary excel files
	*
	*/
	public function deleteTemp(){

		$response = (object)array();

		// delete excels folder
		system('/bin/rm -rf ' . escapeshellarg( '../excels' ) );

		// re create excels folder
		mkdir( '../excels' );

		$response->status = 200;
		$response->message = "Archivos temporales y antiguos borrados correctamente.";

		echo json_encode( $response );
		die();	
	}
}
?>