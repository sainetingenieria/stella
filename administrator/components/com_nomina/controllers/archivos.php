<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );



// Begining of the controller
class NominaControllerArchivos extends JControllerLegacy{


	/**
	 * Save edit company
	 * @return redirect to page list
	 */
	public function save(){

		$data = JFactory::getApplication()->input->post->getArray();

		// Get the uploaded file information.
		$input    = JFactory::getApplication()->input;

		// Do not change the filter type 'raw'. We need this to let files containing PHP code to upload. See JInputFiles::get.
		$userfile = $input->files->get('archivo', null, 'raw');


		$link = 'index.php?option=com_nomina&view=archivos&layout=edit';
		$app = JFactory::getApplication();

		if(!empty($userfile['name'])){

		

			if ($userfile['type'] != 'application/pdf'){
				$msg = 'Debe cargar un PDF';
				return $app->redirect($link, $msg, $msgType='error');
			}

			if ($userfile['size'] > 30000000){
				$msg = 'El archivo no debe pesar mas de 30MB';
				return $app->redirect($link, $msg, $msgType='error');
			}

			$filename = time() .'.pdf';
		
			$tmp_dest = JPATH_ROOT . '/pdfs/' . $filename;
			$tmp_src  = $userfile['tmp_name'];

			// Move uploaded file.
			jimport('joomla.filesystem.file');
			JFile::upload($tmp_src, $tmp_dest, false, true);

			$data['archivo'] = $filename;
		}

		$model = $this->getModel('archivos');

		$model->instance($data);

		

		if (! $model->save('bool')) {
			$msg = 'Hubo un error al guardar en la base de datos por favor intente más tarde';
			return $app->redirect($link, $msg, $msgType='error');
		}
	
		$link = 'index.php?option=com_nomina&view=archivos';
		$msg = 'El registro se ha creado correctamente';
		return $app->redirect($link, $msg, $msgType='message');
	}


	/**
	 *  Task that deletes the list of company
	 */
	public function delete(){
		
		//Het the items picked
		$pks = JRequest::getVar( 'cid' );

		//Instance a model for each item and delete
		foreach ( $pks as $id ) {
			
			// Call the model
			$model = $this->getModel( 'archivos' );
			
			$model->instance( $id );

			$model->delete();
			
		}

		$app = JFactory::getApplication();
		$link = 'index.php?option=com_nomina&view=archivos';
		$msg = 'Registros eliminados';
		return $app->redirect($link, $msg, $msgType='message');

		return;
	}

	/**
	 * Show tpl edit to specific company
	 * @return display edit
	 */
	public function edit(){

		$id = JRequest::getVar('cid');

		$model = $this->getModel('archivos');

		$archivo = $model->getObject($id[0]);

		$view = $this->getView('archivos', 'html');

		$view->setLayout('edit');

		$view->assignRef('archivo', $archivo);

		$view->display();
	}

	/**
	 * Show tpl edit to specific company
	 * @return display edit
	 */
	public function newArchivo(){

		$view = $this->getView('archivos', 'html');

		$view->setLayout('edit');

		$view->display();
	}

}
?>