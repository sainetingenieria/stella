<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );



// Begining of the controller
class NominaControllerVariables extends JControllerLegacy{


	/**
	 * Save edit company
	 * @return redirect to page list
	 */
	public function save(){

		$data = JFactory::getApplication()->input->post->getArray();

		$model = $this->getModel('variables');

		$model->instance($data);

		$link = 'index.php?option=com_nomina&view=variables';
		$app = JFactory::getApplication();

		if (! $model->save('bool')) {
			$msg = 'Hubo un error al guardar en la base de datos por favor intente más tarde';
			return $app->redirect($link, $msg, $msgType='error');
		}

		$msg = 'Variables guardadas correctamente';
		return $app->redirect($link, $msg, $msgType='message');

	

	}

}
?>