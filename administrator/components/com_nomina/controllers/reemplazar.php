<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

// Begining of the controller
class NominaControllerReemplazar extends JControllerLegacy{

	/**
	*
	* Uploads and the excels files
	*
	*/
	public function uploadExcel(){


		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'xlsx' );
		// max file size in bytes
		$sizeLimit = 8 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../excels/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}
	
	/**
	 * 
	 * @return [type]
	 */
	public function parseExcel(){

		$data = JRequest::getVar( 'data' );

		$response = ( object )array();
		$response->log = '';

		$response->status = 200;
		$response->message = 'Controlador Alcanzado';
		$response->sent = $data;
		$response->excel = $data['excel'];

		$inputFileName = '../excels/' . $data['excel'];

		// get the file uploaded
		if( ! file_exists($inputFileName) ){

			$response->status = 500;
			$response->log .= 'Archivo no encontrado.<br>';
			$response->message = 'No se puede acceder al archivo que has subido.';

			echo json_encode( $response );
			die();
		}

	    $response = $this->saveExcel( $inputFileName, $response);

	    $response->log .= '<br>';


		// delete the file
		unlink( $inputFileName );

		echo json_encode( $response );
		die();

	}


	/**
	 * @param  [type]
	 * @param  [type]
	 * @return [type]
	 */
	public function saveExcel( $inputFileName, $response ){

		// if( ! is_object( $data ) )
		// 	return false;

		$reader = ReaderFactory::create(Type::XLSX);
		$reader->open($inputFileName);

		$results = array();

		foreach ($reader->getSheetIterator() as $sheet) {
		    foreach ($sheet->getRowIterator() as $key => $rows) {

		    	if ($key == 1) {
		    		continue;
		    	}

		    	$row = array(
						'A' => $rows[0]
					,	'B' => $rows[1]
					,	'C' => $rows[2]
					,	'D' => $rows[3]
					,	'E' => $rows[4]
					,	'F' => $rows[5]
					,	'G' => $rows[6]
					,	'H' => $rows[7]
					,	'I' => $rows[8]
					,	'J' => $rows[9]
					,	'K' => $rows[10]
					,	'L' => $rows[11]
					,	'M' => $rows[12]
					,	'N' => $rows[13]
					,	'O' => $rows[14]
					,	'P' => $rows[15]
					,	'Q' => $rows[16]
					,	'R' => $rows[17]
					,	'S' => $rows[18]
					,	'T' => $rows[19]
					,	'U' => $rows[20]
					,	'V' => $rows[21]
					,	'W' => $rows[22]
					,	'X' => $rows[23]
					,	'Y' => $rows[24]
					,	'Z' => $rows[25]
					,	'AA' => $rows[26]
					,	'AB' => $rows[27]
					,	'AC' => $rows[28]
					,	'AD' => $rows[29]


				);

		    	$modelEmpleados = $this->getModel('empleados');

		    	$modelEmpresa = $this->getModel('empresas');

		    	$empleado = $modelEmpleados->getEmpleado($row['B']);

		    	$empresa = $modelEmpresa->getObject($empleado->id_empresa);

		    	if (empty($empleado)) {
					$response->log .= "<tr><td style='background-color: red;'>El empleado con identificacion ".$row['B']." de la fila ".$key." no se encuentra en la base de datos.</td></tr><br>";
					continue;
		    	}

		    	$data = array();

  				$data['periodo'] = $row['A'];
  				$data['cedula_ciudadania'] = $row['B'];
  				$data['nombres'] = $row['C'];
  				$data['sueldo_basico'] = $row['D'];
  				$data['dias_liquidados'] = $row['E'];
  				$data['basico'] = $row['F'];
		    	$data['aux_transporte'] = $row['G'];
		    	$data['horas_extras'] = $row['H'];
		    	$data['otros_auxilios'] = $row['I'];
		    	$data['comisiones'] = $row['J'];
		    	$data['total_devengado'] = $row['K'];
		    	$data['salud_descuento'] = $row['L'];
		    	$data['pension'] = $row['M'];
				$data['fondo_solidaridad'] = $row['N'];
				$data['retencion_fuente'] = $row['O'];
				$data['otras_deducciones'] = $row['P'];
				$data['total_deducciones'] = $row['Q'];
				$data['total_pagos'] = $row['R'];
				$data['neto_pagado'] = $row['S'];
				$data['salud_empresa'] = $row['T'];
				$data['riesgos_profesionales'] = $row['U'];
				$data['fondo_pensiones'] = $row['V'];
				$data['icbf'] = $row['W'];
				$data['sena'] = $row['X'];
				$data['subsidio_familiar'] = $row['Y'];
				$data['aux_alimentacion'] = $row['Z'];
				$data['cesantias'] = $row['AA'];
				$data['intereses_cesantias'] = $row['AB'];
				$data['prima'] = $row['AC'];
				$data['vacaciones'] = $row['AD'];
				$data['id_empleado'] = $empleado->id;
				$data['id_empresa'] = $empresa->id;
				$data['valor'] = $row['S'];

				array_push($results, $data);

		    }
		}
		
		$session = JFactory::getSession();
		$session->set('results', $results);		

 		$response->results = $results;
		$response->status = 200;
		$response->message = "Archivo convertido y guardado correctamente.";

		return $response;
	}

	/**
	 * Show tpl edit to specific company
	 * @return display edit
	 */
	public function edit(){

		$id = JRequest::getVar('cid');

		$model = $this->getModel('empresas');

		$empresa = $model->getObject($id[0]);

		$view = $this->getView('empresas', 'html');

		$view->setLayout('edit');

		$view->assignRef('empresa', $empresa);

		$view->display();
	}

	/**
	 * Save edit company
	 * @return redirect to page list
	 */
	public function save(){

		$data = JFactory::getApplication()->input->post->getArray();

		$model = $this->getModel('empresas');

		$model->instance($data);

		$link = 'index.php?option=com_nomina&view=empresas&layout=list';
		$app = JFactory::getApplication();

		if (! $model->save('bool')) {
			$msg = 'Hubo un error al guardar en la base de datos por favor intente más tarde';
			return $app->redirect($link, $msg, $msgType='error');
		}

		$msg = 'El registro se actualizo correctamente';
		return $app->redirect($link, $msg, $msgType='message');

	

	}

	/**
	 *  Task that deletes the list of company
	 */
	public function delete(){
		
		//Het the items picked
		$pks = JRequest::getVar( 'cid' );

		//Instance a model for each item and delete
		foreach ( $pks as $id ) {
			
			// Call the model
			$model = $this->getModel( 'empresas' );
			
			$model->instance( $id );

			$model->delete();
			
		}

		$app = JFactory::getApplication();
		$link = 'index.php?option=com_nomina&view=empresas&layout=list';
		$msg = 'Registros eliminados';
		return $app->redirect($link, $msg, $msgType='message');

		return;
	}

	/**
	* Truncate the table
	*
	*/
	public function truncate(){

		$response = ( object )array();

		// $model = $this->getModel( 'usuarios' );

		//$model->truncate();

		$response->status = 200;
		$response->message = "";

		echo json_encode( $response );
		die();

	}

	/**
	* Deletes all temporary excel files
	*
	*/
	public function deleteTemp(){

		$response = (object)array();

		// delete excels folder
		system('/bin/rm -rf ' . escapeshellarg( '../excels' ) );

		// re create excels folder
		mkdir( '../excels' );

		$response->status = 200;
		$response->message = "Archivos temporales y antiguos borrados correctamente.";

		echo json_encode( $response );
		die();	
	}


	public function sendNotificationLiquidador(){

		$config = JFactory::getConfig();
		$mail = JFactory::getMailer();

		$sender = array(
        	$config->get('mailfrom'),
        	$config->get('fromname'),
        );

        $mail->setSender($sender);
        $mail->addRecipient($config->get('mailfrom'));
        $mail->setSubject('Novedades de Liquidador de Nomina');
        $mail->Encoding = 'base64';
        $mail->isHtml($ishtml = true);

        $contenido = "<div style='font-weight: 200;font-family: Roboto, sans-serif;font-size:15px; color: #7B7B7B; width: 100%;background-image: url(".JURI::root()."images/correo/fondo-correo.png)'><div style='background: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:13px;display:inline-block;'></div><br><br><img src='".JURI::root()."images/correo/logo-correo.png'><div style='padding-left:14px;padding-right:14px;text-align:justify;'><br><br>

       	Buen día,<br><br>";

        $contenido .= "No olvides subir el archivo de novedades para realizar la liquidación del próximo periodo, a continuación podrás descargar el archivo necesario para subir las novedades al Stella.<br><br>";
        $contenido .= "Para subir las novedades ingresa a:<br><br>";
        $contenido .= "1. Ingresar al <a href='".JURI::root()."administrator'>administrador de contenidos Joomla</a><br>";
        $contenido .= "2. Iniciar sesión con un usuario y contraseña.<br>";
        $contenido .= "3. Una vez dentro del administrador ir a la parte superior en el menú Componentes.<br>";
        $contenido .= "4. Dentro del menú encontrará una opción llamada 'Nomina' y dentro de esta una opción llamada 'Generar Nomina'.<br><br>";

        $contenido .= "Te agradecemos enviarla lo antes posible.<br><br>";

        $contenido .= "Feliz día.<br><br>";

        $contenido .= "</div><br><br><img style='margin: 0 auto;display:block;' src='".JURI::root()."images/correo/datos.png'><br><div style='background-image: url(".JURI::root()."images/correo/footer2.png);background-size:cover;width: 100%;height:18px;display:inline-block;'></div>";


        $mail->setBody($contenido);

        // enviar email
        $mail->Send();

	}
}
?>