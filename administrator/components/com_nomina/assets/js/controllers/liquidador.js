/**
*
* Controller for { Tecnico }
*
*
**/

( function( $, window, document, model, view, Utilities ){
	
	var LiquidadorController = function( a ){
		
		// atributes, selector or global vars
		this.excel = '';
		this.pdf = '';
		this.excels = [];


		var _this = this;

		$(document).ready(function($) {

			if (jQuery('#piechart_3d').length){
				google.charts.load("current", {packages:["corechart"]});
				google.charts.setOnLoadCallback(_this.createChartsTorta);
				google.charts.setOnLoadCallback(_this.createChartsBarras);
			}
			
		});

		
	};
	
	LiquidadorController.prototype ={

			createChartsTorta: function(){

				var data = google.visualization.arrayToDataTable([
		          ['Departamento', 'Costo empleado'],
		          ['Desarrollo',     11],
		          ['Gestión Humana',      2],
		          ['Comercial',  2],
		          ['Servicio al Cliente', 2],
		          ['Almacén',    7]
		        ]);

		        var options = {
		          title: 'Departamentos',
		          is3D: true,
		        };

		        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));

		        google.visualization.events.addListener(chart, 'ready', function () {
			        model.imageConvert(function(data){

						// var img = '<img src="'+url+'images/graficos/' + data.filename + '">';
			        	$('#chart1').val(data.filename);

					}, chart.getImageURI());
			    }); 
			    
		        chart.draw(data, options);

			}


		,	createChartsBarras: function(){

				 // Some raw data (not necessarily accurate)
			    var data = google.visualization.arrayToDataTable([
			         ['Periodo', 'Desarrollo', 'Gestión Humana', 'Comercial', 'Servicio al Cliente', 'Almacén', 'Gerencia'],
			         ['2004/05',  165,      938,         522,             998,           450,      614.6],
			         ['2005/06',  135,      1120,        599,             1268,          288,      682],
			         ['2006/07',  157,      1167,        587,             807,           397,      623],
			         ['2007/08',  139,      1110,        615,             968,           215,      609.4],
			         ['2008/09',  136,      691,         629,             1026,          366,      569.6]
			    ]);

			    var options = {
			      title : 'Departamentos Historico',
			      vAxis: {title: 'Costo Empleado'},
			      hAxis: {title: 'Periodo'},
			      seriesType: 'bars',
			    };


				var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));

				google.visualization.events.addListener(chart, 'ready', function () {
					model.imageConvert(function(data){

						// var img = '<img src="'+url+'images/graficos/' + data.filename + '">';
						$('#chart2').val(data.filename);

					}, chart.getImageURI());
				}); 

				chart.draw(data, options);

			}
		
			/**
			* Function header when calls the model method and gives the view the response
			*
			* @param {}
			* @return {}
			*/
		,	parseExcel: function( data ){

				view.onBeforeParse( this.excels );
				this.recursiveParse( 0, data );

				// For each file in excels array, make a request to save the excel data
				// for (var i = 0; i < this.excels.length ; i++ ) 
				// }
				
			}

		,	recursiveParse: function( index, data ){

				var	_this = this
				,	_data = data;

				if( index < this.excels.length ){

					data.excel = this.excels[0];

					var excelName = data.excel.replace( /.xlsx/g, '' ).replace( /.xlsx/g, '' ).replace( / /g, '-' );
					$( '#' + excelName ).html( excelName + ': Convirtiendo' + view.loader );

					var success = function( data ){

						console.log( data );

						view.onCompleteParse( data );

						//count--;

						index++;

						_this.recursiveParse( index, _data );
					};

					var error = function ( XMLHttpRequest, textStatus, errorThrown ) {
                        view.onError( XMLHttpRequest, textStatus, errorThrown, excelName );

                        index++;

						_this.recursiveParse( index, _data );  
                    };

					model.parseExcel( success, error,  data );

					

				} else {

					_this.deleteTemp( data );
				}
			}

			/**
			* Deletes the uploaded file
			*
			*/
		,	deleteTemp: function( count, data ){

				// At the end delete the excels file
				if( count <= 0 ){
					
					view.onBeforeDeleteTemp();

					var completeDeleteTemp = function( data ){

						view.onCompleteDeleteTemp( data );
					};

					return model.deleteTemp( completeDeleteTemp, data  );
				}
			}

		,	truncate: function( _data ){

				view.onBeforeTruncate();

				var _this = this;

				var success = function( data ){

					view.onCompleteTruncate( data );

					if( data.status == 200 )
						_this.parseExcel( _data );
				};

				return model.truncate( success, _data );
			}


			/**
			* Verifies the pdf file with some data
			*
			*/
		,	verifyMatricula: function( fileName ){

				var success = function( data ){

					view.renderPDFLog( data );
				};

				return model.verifyMatricula( success, fileName );
			}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.LiquidadorController = new LiquidadorController();
	
})( jQuery, this, this.document, this.LiquidadorModel, this.LiquidadorView, this.Misc, undefined );