/**
*
* File uploading
*
*/

( function( $, window, document, tecnico ){

	var Uploader = function( a ){

		this.Excelparams = {

				element: document.getElementById( 'excel-uploader' )
			,   action: 'index.php'
			,   params: {

			    	option: 'com_financiero',
			    	task: 'comercial.uploadExcel'
			    }

			,   allowedExtensions: [ 'xlsx' ]
			,   debug: false
			,	onSubmit: this.onSubmitExcel
			,	onProgress: this.onProgressExcel
			,	onComplete: this.onCompleteExcel
			,   onError: this.onErrorExcel
		    
		};

		this.pdfQueue = 0;

		this.pdfUploadCount = 1;

		this.fileList = 0;

	};

	Uploader.prototype = {

			initialize: function(){

				this.createUploaderExcel();
			}

		,	createUploaderExcel: function(){

				var _this = this;

				var uploader = new qq.FileUploader( _this.Excelparams );

			}

		,	onSubmitExcel: function( id, fileName ){

				var li = $( '<li>' )
				,	i = $( '<i>' )
				,	spanName = $( '<span>' )
				,	spanBar = $( '<span>' )
				,	spanPerc = $( '<span>' );

				var htmlName = fileName.replace( /.xlsx/g, '' );
				htmlName = htmlName.replace( / /g, '-' );

				console.log(htmlName);

				i.addClass( 'icon-excel' );
				spanName.text( fileName );
				spanName.addClass('filename');
				spanBar.addClass( 'progress-bar' );
				spanPerc.addClass( 'percentage' );

				i.appendTo( li );
				spanName.appendTo( li );
				spanBar.appendTo( li );
				spanPerc.appendTo( li );

				li.attr( 'id', 'item-' + htmlName );

				li.appendTo( $( '.excel-list' ) );
			}

		,	onProgressExcel: function( id, fileName, loaded, total ){

				var perc = Math.floor( loaded / total * 100 );

				var htmlName = fileName.replace( /.xlsx/g, '' );
				htmlName = htmlName.replace( / /g, '-' );

				$( '#item-' + htmlName + ' .percentage' ).text( perc + '%' );
				$( '#item-' + htmlName + ' .progress-bar' ).width( perc + 'px' );

			}

		,	onCompleteExcel: function(id, fileName, responseJSON){

				$('#importar-datos').animate({'opacity':'1'});
				$('.importar-datos').css({'display':'block'});

				$('.import-label').show('slow');

				if( ! responseJSON.success ){
					$( '.qq-upload-list' ).remove();
					return;
				}

				// Save the file name in array to be processed at the end
				tecnico.excels.push( fileName );

			}

		,   onErrorExcel: function(id, fileName, xhr){
				console.log(xhr);
				var span = $('<span>');
				span.addClass('error');
				span.text( 'Falló subida de este archivo' );
				$('#item-' + fileName.replace( /.xlsx/g, '') ).append( span );
				return;
			}

		
	};

	$( document ).ready( function(){
		window.Uploader = new Uploader();
		window.Uploader.initialize();
	});

})( jQuery, this, this.document, this.ComercialController, undefined );







