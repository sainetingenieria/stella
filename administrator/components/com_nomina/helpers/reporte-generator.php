<?php
/**
* This class generates the certifies dinamically
*
*/
use Dompdf\Dompdf;

ini_set('memory_limit', '-1');
class reporteGen {

	public static function getPdf( $data ){

		// if( ! is_object( $data ) )
		// 	return false;

        $starttime = microtime(true);

		$dompdf = new DOMPDF();


		// $font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader();
		$html .= self::getContent( $data );
		$html .= self::getFooter( );

		// var_dump($html);
		// die;

		$dompdf->load_html( $html );
		$dompdf->set_paper(array(0,0,700,1050));
		$dompdf->render();

		

		$_output = $dompdf->output();

		$randomString = JPATH_COMPONENT . '/assets/pdf/'.time().'_detallado.pdf';

		file_put_contents($randomString, $_output );

		return $randomString;
	}


	/**
	* Returns the pdf header
	* 
	* @param { string } the type of document
	* @return { string } the html compiled
	*/
	protected static function getHeader(){

		$html = '';

				$html .= '<html>
				<head>
					<title>RESULTADOS CALCULO LIQUIDACION</title>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					'.self::getStyles().'
				</head>

				<body>

				<div id="header">
					<img src="'.JPATH_ROOT.'/images/pdf/encabezado.png" style="width: 100%;">
	 			</div>

	 			<div id="footer">
					<img src="'.JPATH_ROOT.'/images/pdf/pie-de-pag.png" style="width: 100%;">
				</div>

				<div style="text-align: left;padding-left: 60px;margin-top: 70px;">
					<img src="'.JPATH_ROOT.'/images/pdf/logo-gris.jpg">
	 			</div>
				';

		return $html;

	}

	protected static function getContent( $data ){

		$html = '

		<div class="main">
			<br>
			<br>
			<br>

			<div class="center">
				<table style="width: 100%;">
					<tr>								
						<td style="text-align: center; font-weight: bold;font-size: 17px;">
							'.$data[0]->razon_social.'
						</td>
					</tr>
					<tr>								
						<td style="text-align: center;color:#8D8D8D;">
							Ingreso / Pago
							<br>
							Informe Detallado de Nómina
						</td>
					</tr>
				</table>
			</div>

			<br><br>';

			$count = 1;
			$multiplo = 4;

			foreach ($data as $key => $item) {

				$key = $key + 1;

				$style = '';
		
				// if ($count == $multiplo) {
				// 	$multiplo = $multiplo + 3;
				// 	$style = 'style="margin-top: 80px;"';
				// }

				$tipo_contrato = ($item->tipo_contrato == 'i') ? 'Termino Indefinido' : 'Termino Fijo';

				$periodo = date('Y/m/d',$item->periodo);

                $periodo1 = strtotime ( '-16 day' , strtotime ( $periodo ) ) ;

                $periodo1 = date ( 'd/m/Y' , $periodo1 );

				$html .= '<div class="center" '.$style.'>
					<table style="width: 95%;margin: 0 auto;border:0;border-spacing: 0;text-align: left;">
						<tr style="background-color: #B9B9B9; color:#FFF;">								
							<td style="padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Nomina
							</td>
							<td style="padding-top:3px;padding-bottom:3px;">
								CUSI-MAN-ANADARKO
							</td>
							<td style="padding-top:3px;padding-bottom:3px;">
								Periodo de Liquidación:
							</td>
							<td style="padding-top:3px;padding-bottom:3px;">
								'.$periodo1 .' - ' .date('d/m/Y',$item->periodo).'
							</td>
							<td style="padding-top:3px;padding-bottom:3px; padding-left: 3px;">
								Fecha de Pago: '.date('y-m-d',$item->fecha).'
							</td>
						</tr>
					</table>
					<table style="width: 95%;margin: 0 auto;border:0;border-spacing: 0;text-align: left;">
						<tr>								
							<td style="border-left: 1px solid #E0E0E0;border-top: 1px solid #E0E0E0; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Código
							</td>
							<td style="border-top: 1px solid #E0E0E0; color:#636363;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								
							</td>
							<td style="border-top: 1px solid #E0E0E0; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Nombre
							</td>
							<td style="border-top: 1px solid #E0E0E0; color:#636363;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.ucfirst($item->nombres).' '.ucfirst($item->apellidos).'
							</td>
							<td style="border-top: 1px solid #E0E0E0; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Identificación
							</td>
							<td style="border-right: 1px solid #E0E0E0;border-top: 1px solid #E0E0E0; color:#636363;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.$item->numero_identificacion.'
							</td>
						</tr>

						<tr>								
							<td style="border-left: 1px solid #E0E0E0;border-top: 1px solid #E0E0E0; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Fecha Ingreso
							</td>
							<td style="border-top: 1px solid #E0E0E0; color:#636363;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.date('y-m-d',$item->fecha_de_ingreso).'
							</td>
							<td style="border-top: 1px solid #E0E0E0; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Contrato
							</td>
							<td style="border-top: 1px solid #E0E0E0; color:#636363;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.$tipo_contrato.'
							</td>
							<td style="border-top: 1px solid #E0E0E0; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Salario
							</td>
							<td style="border-right: 1px solid #E0E0E0;border-top: 1px solid #E0E0E0; color:#636363;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.Misc::numberDots($item->salario).'
							</td>
						</tr>

						<tr>								
							<td style="border-left: 1px solid #E0E0E0;border-top: 1px solid #E0E0E0; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Cargo
							</td>
							<td colspan="5" style="border-right: 1px solid #E0E0E0;border-bottom: 1px solid #E0E0E0;border-top: 1px solid #E0E0E0; color:#636363;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.ucfirst($item->cargo).'
							</td>
						</tr>
					</table>
				</div>

				<br>

				<div class="center">
					<table style="width: 95%;margin: 0 auto;border:0;border-spacing: 0;text-align: left;">
						<tr>								
							<td style="width:400px;border-left: 1px solid #FFF; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Descripción
							</td>
							<td style="border-left: 1px solid #FFF; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Pagos
							</td>
							<td style="border-left: 1px solid #FFF; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Descuentos
							</td>
							<td style="text-align:center;border-left: 1px solid #FFF; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Neto
							</td>
						</tr>
						<tr>								
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Sueldo
							</td>
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.Misc::numberDots($item->basico).'
							</td>
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								0
							</td>
							<td style="border-left:1px solid #E0E0E0;border-right:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								
							</td>
						</tr>
						<tr>								
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Aportes Salud
							</td>
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								0
							</td>
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.Misc::numberDots($item->salud_descuento).'
							</td>
							<td style="border-left:1px solid #E0E0E0;border-right:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								
							</td>
						</tr>
						<tr>								
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Aportes Pensión
							</td>
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								0
							</td>
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.Misc::numberDots($item->pension).'
							</td>
							<td style="border-left:1px solid #E0E0E0;border-right:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								
							</td>
						</tr>
						<tr>								
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								Fondo Solidaridad
							</td>
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								0
							</td>
							<td style="border-left:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.Misc::numberDots($item->fondo_solidaridad).'
							</td>
							<td style="border-left:1px solid #E0E0E0;border-right:1px solid #E0E0E0;color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								
							</td>
						</tr>';

						$total_pagos = $item->basico;
						$total_descuentos = $item->salud_descuento + $item->pension + $item->fondo_solidaridad;
						$total = $total_pagos - $total_descuentos;

				$html	.='<tr>								
							<td style="text-align:right;border-left: 1px solid #FFF; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;padding-right: 20px;">
								Totales
							</td>
							<td style="border-left: 1px solid #FFF; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.Misc::numberDots($total_pagos).'
							</td>
							<td style="border-left: 1px solid #FFF; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.Misc::numberDots($total_descuentos).'
							</td>
							<td style="text-align:center;border-left: 1px solid #FFF; background-color: #EFF0F1; color: #9D9D9D;padding-top:3px;padding-bottom:3px; padding-left: 10px;">
								'.Misc::numberDots($total).'
							</td>
						</tr>
					</table>

				</div>

				';



				if (fmod($key, 3) == 0) {
					$html .= '<p></p>';
					$html .= '
					<div style="text-align: left;padding-left: 60px;margin-top: 70px;">
						<img src="'.JPATH_ROOT.'/images/pdf/logo-gris.jpg">
		 			</div>';

		 			$html .= '<div class="center">
							<table style="width: 100%;">
								<tr>								
									<td style="text-align: center; font-weight: bold;font-size: 17px;">
										'.$data[0]->razon_social.'
									</td>
								</tr>
								<tr>								
									<td style="text-align: center;color:#8D8D8D;">
										Ingreso / Pago
										<br>
										Informe Detallado de Nómina
									</td>
								</tr>
							</table>
						</div><br><br>';
				}else{
					$html .= '
					<br>
					<div style="width:95%;margin:0 auto; border: 1px dashed #CCC;"></div>
					<br>';
				}

				$count++;


			}



		$html .= '</div>
		';

		return $html;
	}

	protected static function getFooter(){
		$html = '
		';

		return $html;
	}

	protected static function getStyles(){

		$html .= '
		<style type="text/css">

			@page { margin: 180px 50px; }
		    #header { position: fixed; left: 0px; top: 0px; right: 0px; height: 400px;}
		    #footer { position: fixed; left: 0px; bottom: 20px; right: 0px; height: 15px; }
		    #footer .page:after { content: counter(page, upper-roman); }
			*{
				font-family: "Roboto";
				margin:0;
				font-size: 14px;
			}

			p{
				page-break-after: always;
			}



		</style>';

		return $html;
	}
}
?>