<?php
/**
 * Helper class for anuncios component
 *
 * 
 */
class Helper {

    public static function getEmpleados($empresa)  {

		$db = JFactory::getDbo();

    	$query = $db->getQuery(true);
    	$query->select('id as value, concat(nombres, " ",apellidos) as text');
    	$query->from('#__nomina_empleados');
        $query->where('id_empresa = '. $empresa);

    	
    	$db->setQuery( $query );

        $result = $db->loadObjectList();

        array_unshift($result, "");

    	return $result;
    }

    public static function getCarpetas($empleado)  {

		$db = JFactory::getDbo();

    	$query = $db->getQuery(true);
    	$query->select('id as value, nombre as text');
    	$query->from('#__nomina_carpetas');
        $query->where('id_empleado = '. $empleado);

    	
    	$db->setQuery( $query );

    	$result = $db->loadObjectList();

        array_unshift($result, "");

        return $result;
    }


    public static function getEmpresas()  {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('id as value, razon_social as text');
        $query->from('#__nomina_empresas');
        
        $db->setQuery( $query );

        $result = $db->loadObjectList();


        $items = array(''=>'Seleccione la Empresa');

        foreach ($result as $key => $item) {

            $items[$item->value] = $item->text;
        }

        return $items;
    }

    public static function getEmpresaByEmpleado($empleado)  {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('b.id AS id_empresa');
        $query->from('#__nomina_empleados AS a');
        $query->innerJoin('#__nomina_empresas AS b ON b.id = a.id_empresa');
        $query->where('a.id = '. $empleado);

        $db->setQuery( $query );

        $result = $db->loadObject();

        return $result->id_empresa;
    }

        public static function getDepartamentos(){

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('departamento');
        $query->from('#__nomina_empleados');
        $query->group('departamento');

        $db->setQuery( $query );

        $result = $db->loadObjectList();

        return $result;
    }

}
?>