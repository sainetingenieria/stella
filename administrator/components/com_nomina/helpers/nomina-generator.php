<?php
/**
* This class generates the certifies dinamically
*
*/
use Dompdf\Dompdf;

ini_set('memory_limit', '-1');
class nominaGen {

	public static function getPdf( $data ){

		// if( ! is_object( $data ) )
		// 	return false;

        $starttime = microtime(true);

		$dompdf = new DOMPDF();


		// $font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader();
		$html .= self::getContent( $data );
		$html .= self::getFooter( );

		// var_dump($html);
		// die;


		$dompdf->load_html( $html );
		$dompdf->set_paper('A4','portrait');
		$dompdf->render();

		

		$_output = $dompdf->output();

		$randomString = JPATH_COMPONENT . '/assets/pdf/'.time().'_'.$data->nit_empresa_retenedora.'_desprendible.pdf';

		file_put_contents($randomString, $_output );

		return $randomString;
	}


	/**
	* Returns the pdf header
	* 
	* @param { string } the type of document
	* @return { string } the html compiled
	*/
	protected static function getHeader(){

		$html = '';

				$html .= '<html>
				<head>
					<title>RESULTADOS CALCULO LIQUIDACION</title>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					'.self::getStyles().'
				</head>

				<body>
				<img src="'.JPATH_ROOT.'/images/pdf/encabezado.png" style="width: 100%;">
				<br><br>
				<div class="header" style="text-align: left;padding-left: 60px;margin-top: 22px;">
					<img src="'.JPATH_ROOT.'/images/pdf/logo-gris.jpg">
	 			</div><br>';

		return $html;

	}

	protected static function getContent( $data ){

		$periodo = date('Y/m/d',$data['periodo']);

		$periodo1 = strtotime ( '-16 day' , strtotime ( $periodo ) ) ;

		$periodo1 = date ( 'Y/m/d' , $periodo1 );

		$data['periodo'] = $periodo1.' - '.$periodo;

		$html = '
		<div class="main">
			<div class="center">
				<table style="width: 100%;">
					<tr>								
						<td style="text-align: center; font-weight: bold;font-size: 17px;">
							DESPRENDIBLE DE NOMINA
						</td>
					</tr>
				</table>
			</div>
			<br><br><br>
			
			<div class="center">
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;text-align: left;">
					<tr>								
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F; padding-right:5px;">Razón Social: </span>
							'.$data['razon_social'].'
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F; padding-right:5px;">Nit: </span>
							'.$data['nit'].'
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F; padding-right:5px;">Periodo: </span>
							'.$data['periodo'].'
						</td>
					</tr>	
					<tr>								
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F; padding-right:5px;">Nombres y Apellidos: </span>
							'.$data['nombres'].'
						</td>	
						<td colspan="2" style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F; padding-right:5px;">Cargo del empleado: </span>
							'.$data['cargo'].'
						</td>
					</tr>									
				</table>
			</div>

			<div class="center">
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;text-align: left;">
					<tr>								
						<td style="width:50%; padding-top: 10px; text-align:center; padding-bottom: 10px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">PAGOS</span>
						</td>	
						<td style="width:50%; padding-top: 10px; text-align:center; padding-bottom: 10px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">DESCUENTOS</span>
						</td>
					</tr>							
				</table>
			</div>

			<div class="center">
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;text-align: left;">
					<tr>								
						<td style="width:220px; border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Salario del empleado: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['basico'].'
						</td>	
						<td style="width:220px; border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Salud: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['salud_descuento'].'
						</td>
					</tr>
					<tr>								
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Auxilio de transporte: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['aux_transporte'].'
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Fondo de solidaridad: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['fondo_solidaridad'].'
						</td>
					</tr>
					<tr>								
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Auxilio de alimentacion: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['aux_alimentacion'].'
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Pension: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['pension'].'
						</td>
					</tr>
					<tr>								
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Otros Auxilios: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['otros_auxilios'].'
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Retencion en la fuente: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['retencion_fuente'].'
						</td>
					</tr>
					<tr>								
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Otros Descuentos: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 2px; padding-bottom: 2px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['otras_deducciones'].'
						</td>
					</tr>
					<tr style="background-color: #F1F2F2;">								
						<td style="border: 1px solid #CCC;solid #CCC;padding-top: 8px;padding-bottom: 8px;color: #B9B9B9;padding-left:5pxfont-size: 15px;">
							<span style="color:#4F4F4F;">Total Pagos: </span>
						</td>	
						<td style="border: 1px solid #CCC;solid #CCC;padding-top: 8px;padding-bottom: 8px;color: #B9B9B9;padding-left:5pxfont-size: 15px;">
							'.$data['total_pagos'].'
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 8px; padding-bottom: 8px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							<span style="color:#4F4F4F;">Total Descuentos: </span>
						</td>	
						<td style="border: 1px solid #CCC; padding-top: 8px; padding-bottom: 8px;color: #B9B9B9;padding-left:5px;font-size: 15px;">
							'.$data['total_deducciones'].'
						</td>
					</tr>						
				</table>
			</div>



			<div class="center">
				<table style="width: 85%;margin: 0 auto;border-collapse: collapse;text-align: left;">
					<tr>								
						<td rowspan="2" style="width:250px;font-size: 21px; background-color: #F1F2F2;border: 1px solid #CCC;padding-top: 6px;padding-bottom: 6px;color: #B9B9B9;padding-left:5px;color: #4F4F4F;text-align: right;padding-right: 19px;">
							<span style="color:#323232;margin-left: 10px;text-align:left;">Neto a Pagar: </span>
							'.$data['neto_pagado'].'
						</td>
						<td style="border: 1px solid #CCC;padding-top: 20px;padding-bottom: 6px;color: #B9B9B9;padding-left:5px;font-size: 15px;border-bottom: 0;border-top: 0;text-align: center;">
							<table style="margin: 0 auto;">
								<tr>
									<td style="width:80px;">
										Nombre
									</td>
									<td>
										_________________________________
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="border: 1px solid #CCC;padding-top: 10px;padding-bottom: 20px;color: #B9B9B9;padding-left:5px;border-top: 0;font-size: 15px;text-align: center;">
							<table style="margin: 0 auto;">
								<tr>
									<td style="width:80px;">
										Firma
									</td>
									<td>
										_________________________________
									</td>
								</tr>
							</table>
						</td>
					</tr>						
				</table>
			</div>
			';



		$html .= '</div>';

		return $html;
	}

	protected static function getFooter(){
		$html = '<br>
		<div class="footer" style="width: 85%;margin: 0 auto;font-style: italic;color: #9C9C9C;">

		
		</div>

		<br><br>
		<img src="'.JPATH_ROOT.'/images/pdf/pie-de-pag.png" style="width: 100%;margin-top:24px;">';

		return $html;
	}

	protected static function getStyles(){

		$html .= '
		<style type="text/css">

			*{
				font-family: "Roboto";
				margin:0;
				padding:0;
			}

			body {
				background-color: #FFF;
				text-align: center;
				font-size: 14px;
			}

			@page document-wrapper {
				size: legal landscape; 
				margin: 1cm;
				background-color: #FFF;
				padding: 40px 0;
			}

		</style>';

		return $html;
	}
}
?>