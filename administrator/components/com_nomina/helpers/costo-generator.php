<?php
/**
* This class generates the certifies dinamically
*
*/
use Dompdf\Dompdf;

ini_set('memory_limit', '-1');
class costoGen {

	public static function getPdf( $data, $adds ){

		// if( ! is_object( $data ) )
		// 	return false;

        $starttime = microtime(true);

		$dompdf = new DOMPDF();


		// $font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader();
		$html .= self::getContent( $data, $adds );
		$html .= self::getFooter( );

		// var_dump($html);
		// die;

		$dompdf->load_html( $html );
		$dompdf->set_paper(array(0,0,1100,850));
		$dompdf->render();

		

		$_output = $dompdf->output();

		$randomString = JPATH_COMPONENT . '/assets/pdf/'.time().'_costo_periodico.pdf';

		file_put_contents($randomString, $_output );

		return $randomString;
	}


	/**
	* Returns the pdf header
	* 
	* @param { string } the type of document
	* @return { string } the html compiled
	*/
	protected static function getHeader(){

		$html = '';

				$html .= '<html>
				<head>
					<title>Reporte Costo Periodico</title>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					'.self::getStyles().'
				</head>

				<body>

				<div id="header">
					<img src="'.JPATH_ROOT.'/images/pdf/encabezado.png" style="width: 100%;">
	 			</div>

	 			<div id="footer">
					<img src="'.JPATH_ROOT.'/images/pdf/pie-de-pag.png" style="width: 100%;">
				</div>

				<div style="text-align: left;padding-left: 60px;margin-top: 65px;">
					<img src="'.JPATH_ROOT.'/images/pdf/logo-gris.jpg">
	 			</div>
				';

		return $html;

	}

	protected static function getContent( $data, $adds ){

		$html = '

		<div class="main">
			<br>

			<div class="center">
				<table style="width: 100%;">
					<tr>								
						<td style="text-align: center; font-weight: bold;font-size: 17px;">
							COSTO PERIODICO
						</td>
					</tr>
				</table>
			</div>

			<br><br>';

			$count = 1;
			$multiplo = 4;

			$html .= '

			<table style="width: 95%;margin: 0 auto;border:0;border-spacing: 0;text-align: left;">

				<tr style="background-color: #006EA5; color: #FFF;">
					<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Nit</td>
					<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px; width:200px;">Razon Social Empresa</td>
					<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Periodo</td>
					<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Número de Identidad</td>
					<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Nombres y Apellidos</td>
					<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Departamento</td>
					<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Costo Empleado</td>
				</tr>

			';


			foreach ($data as $key => $item) {


				$key = $key + 1;

				$style = '';

				if (fmod($key, 11) == 0) {
					$html .= '</table>';
					$html .= '
						<br><br>
						<table style="width: 95%;margin: 0 auto;border:0;border-spacing: 0;text-align: center;">
							<tr>
								<td><img src="'.JPATH_ROOT.'/images/graficos/'.$adds['chart1'].'" style="width: 500px;"></td>
								<td><img src="'.JPATH_ROOT.'/images/graficos/'.$adds['chart2'].'" style="width: 500px;"></td>
							</tr>
						</table>

						';
					$html .= '<p></p>';

					$html .= '
					<div style="text-align: left;padding-left: 60px;margin-top: 70px;">
						<img src="'.JPATH_ROOT.'/images/pdf/logo-gris.jpg">
		 			</div>';

		 			$html .= '<div class="center">
							<table style="width: 100%;">
								<tr>								
									<td style="text-align: center; font-weight: bold;font-size: 17px;">
										COSTO PERIODICO
									</td>
								</tr>
							</table>
						</div>

						<br><br>';
			
					$html .= '
					<table style="width: 95%;margin: 0 auto;border:0;border-spacing: 0;text-align: left;">

						<tr style="background-color: #006EA5; color: #FFF;">
							<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Nit</td>
							<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px; width:200px;">Razon Social Empresa</td>
							<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Periodo</td>
							<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Número de Identidad</td>
							<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Nombres y Apellidos</td>
							<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Departamento</td>
							<td class="test" style="padding-top: 10px; padding-bottom: 10px; padding-left:14px">Costo Empleado</td>
						</tr>

					';
				}

				$periodo = date('Y/m/d',$item->periodo);

                $periodo1 = strtotime ( '-16 day' , strtotime ( $periodo ) ) ;

                $periodo1 = date ( 'd/m/Y' , $periodo1 );

				
				$html .= '
					<tr>
						<td style="padding-top: 10px; padding-bottom: 10px; padding-left:14px; border-bottom: 1px solid #CCC;border-left: 1px solid #CCC;">'.ucwords($item->nit).'</td>
                   	 	<td style="padding-top: 10px; padding-bottom: 10px; padding-left:14px; border-bottom: 1px solid #CCC;">'.ucfirst($item->razon_social).'</td>
                   	 	<td style="padding-top: 10px; padding-bottom: 10px; padding-left:14px; border-bottom: 1px solid #CCC;">'.$periodo1 .' - ' .date('d/m/Y',$item->periodo).'</td>
                    	<td style="padding-top: 10px; padding-bottom: 10px; padding-left:14px; border-bottom: 1px solid #CCC;">'.$item->numero_identificacion.'</td>
                    	<td style="padding-top: 10px; padding-bottom: 10px; padding-left:14px; border-bottom: 1px solid #CCC;">'.ucfirst($item->nombres).' '.ucfirst($item->apellidos).'</td>
                    	<td style="padding-top: 10px; padding-bottom: 10px; padding-left:14px; border-bottom: 1px solid #CCC;">'.ucfirst($item->departamento).'</td>
                    	<td style="padding-top: 10px; padding-bottom: 10px; padding-left:14px; border-bottom: 1px solid #CCC;border-right: 1px solid #CCC;">'. Misc::numberDots($item->costo_empleado).'</td>
					</tr>
				';


			}

		$html .= '
			<br><br>
			<table style="width: 95%;margin: 0 auto;border:0;border-spacing: 0;text-align: center;">
				<tr>
					<td><img src="'.JPATH_ROOT.'/images/graficos/'.$adds['chart1'].'" style="width: 500px;"></td>
					<td><img src="'.JPATH_ROOT.'/images/graficos/'.$adds['chart2'].'" style="width: 500px;"></td>
				</tr>
			</table>

		';

		$html .= '</table></div>
		';

		return $html;
	}

	protected static function getFooter(){
		$html = '
		';

		return $html;
	}

	protected static function getStyles(){

		$html .= '
		<style type="text/css">

			@page { margin: 180px 50px; }
		    #header { position: fixed; left: 0px; top: 0px; right: 0px; height: 400px;}
		    #footer { position: fixed; left: 0px; bottom: 20px; right: 0px; height: 15px; }
		    #footer .page:after { content: counter(page, upper-roman); }
			*{
				font-family: "Roboto";
				margin:0;
				font-size: 14px;
			}

			p{
				page-break-after: always;
			}

			.test{
				position: relative;
			}

			.test:before{
				position: absolute;
				content: "";
				left: 0;
				top: 6px;
				width: 1px;
				height: 25px;
				background-color: #FFF;
			}

			.test:first-child:before{
				display: none;
			}



		</style>';

		return $html;
	}
}
?>