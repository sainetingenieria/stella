<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 */
// Joomla calls and runtimes
defined('_JEXEC') or die();

//get the hosts name
jimport('joomla.environment.uri');
// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');
$host = JURI::root();

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document = & JFactory::getDocument();
$document->addStyleSheet($host . 'administrator/components/com_nomina/assets/js/libs/fileuploader/fileuploader.css');
$document->addStyleSheet($host . 'administrator/components/com_nomina/assets/css/style.css');

$document->addScript($host . 'administrator/components/com_nomina/assets/js/libs/fileuploader/fileuploader.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/misc/misc.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/views/reemplazar.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/models/reemplazar.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/controllers/reemplazar.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/handlers/reemplazar.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/misc/reemplazar_file.js');

//fb( $this->state );
?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=liquidador&layout=list');?>" method="post" name="adminForm" id="adminForm">

	<fieldset>
		<legend>Paso 2. Resultados

		<?php $id = JRequest::getVar('id');?>

        <?php if (!empty($id)): ?>
			<a class="confirmar-nomina" href="<?php echo JRoute::_('index.php?option=com_nomina&task=liquidador.saveNomina&id='.$id);?>">Confirmar Nomina</a>
  		<?php else: ?>
			<a class="confirmar-nomina" href="<?php echo JRoute::_('index.php?option=com_nomina&task=liquidador.saveNomina');?>">Confirmar Nomina</a>
  		<?php endif; ?>

		<a class="descargar-excel" href="<?php JURI::root() ?>liquidaciones/<?php echo $this->liquidaciones['excel'] ?>">Descargar Excel Generado</a>

		</legend>

	
		<table class="table table-striped" style="width:1900px;display:inline-block;overflow-x:scroll;">
			<thead>
				<tr>
					
					<th class="center nowrap">
						Cédula de Ciudadanía
					</th>
					<th class="center nowrap">
						Nombre del empleado
					</th>
					<th class="center nowrap">
						Sueldo básico
					</th>
					<th class="center nowrap">
						Días liquidados
					</th>
					<th class="center nowrap">
						Básico
					</th>
					<th class="center nowrap">
						Auxilio de transporte
					</th>
					<th class="center nowrap">
						Horas extras		
					</th>
					<th class="center nowrap">
						Comisiones		
					</th>
					<th class="center nowrap">
						Total devengado	
					</th>
					<th class="center nowrap">
						Salud
					</th>
					<th class="center nowrap">
						Pension
					</th>
					<th class="center nowrap">
						Fondo de solidaridad
					</th>
					<th class="center nowrap">
						Retención en la fuente
					</th>
					<th class="center nowrap">
						Otras deducciones
					</th>
					<th class="center nowrap">
						Total deducciones
					</th>
					<th class="center nowrap">
						Neto pagado
					</th>
					<th class="center nowrap">
						Salud
					</th>
					<th class="center nowrap">
						Riesgos profesionales
					</th>
					<th class="center nowrap">
						Fondo de pensiones
					</th>
					<th class="center nowrap" width="20%">
						ICBF
					</th>
					<th class="center nowrap">
						SENA
					</th>
					<th class="center nowrap">
						Subsidio familiar
					</th>
					<th class="center nowrap">
						Cesantia
					</th>
					<th class="center nowrap">
						Intereses sobre cesantías
					</th>
					<th class="center nowrap">
						Prima de servicios
					</th>
					<th class="center nowrap">
						Vacaciones
					</th>
				</tr>
			</thead>
			<tbody>

			<?php foreach ($this->liquidaciones as $i => $item) :

				if (!is_array($item)) {
					continue;
				}

			?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center"><?php echo $item['cedula_ciudadania']?></td>
					<td class="center"><?php echo $item['nombres']?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['sueldo_basico'])?></td>
					<td class="center"><?php echo $item['dias_liquidados']?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['basico'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['aux_transporte'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['horas_extras'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['comisiones'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['total_devengado'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['salud_descuento'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['pension'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['fondo_solidaridad'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['retencion_fuente'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['otras_deducciones'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['total_deducciones'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['neto_pagado'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['total_deducciones'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['salud_empresa'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['riesgos_profesionales'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['icbf'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['sena'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['subsidio_familiar'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['cesantias'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['intereses_cesantias'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['prima'])?></td>
					<td class="center"><?php echo '$ '.Misc::numberDots($item['vacaciones'])?></td>
				</tr>
			<?php 
			endforeach; 
			?>
			</tbody>
		</table>

	</fieldset>

	
	<div class="clr"></div>
	<fieldset>
        <legend>Reemplazar Excel</legend>

        <p>Asegúrese que su archivo de Excel conserva el mismo formato, puesto que los campos adicionales que no coincidan con la base de datos no se guardarán.</p>
        <ul class="excel-list">
        </ul>
        <div id="excel-uploader">       
            <noscript>          
            <p>habilita javascript en tu navegador para poder subir archivos.</p>
            <!-- or put a simple form for upload here -->
            </noscript>         
        </div>

        <p class="import-label">Cuando todos los archivos indiquen 100%, haga click en "Importar" para comenzar la importación de datos.</p>
        <input type="button" href="#" id="importar-datos" class="qq-upload-button importar-datos" value="Importar datos">

                <!-- <p class="truncate-label"></p> -->
        
        <ul class="excel-parse-list"></ul>


        <div class="excel-reporte">
            <h3>Reporte</h3>
            <ul>
                <li>Errores en la conversión: <span class="excel-errors">0</span></li>
            </ul>
            <div class="excel-log">
            </div>
            <br>

            <p>Luego de Cargar el Excel puede dar clic en Confirmar Nomina</p>
        </div>

    </fieldset>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>