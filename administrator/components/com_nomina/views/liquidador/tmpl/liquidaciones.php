<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.
//JHtml::_('behavior.tooltip');
//JHtml::_('behavior.multiselect');
//JHtml::_('behavior.modal');
JHtml::_('behavior.framework');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
		var url = "'.JURI::base().'"
	' );

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

//fb( $this->state );
?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=liquidador&layout=list');?>" method="post" name="adminForm" id="adminForm">


	<div id="filter-bar" class="btn-toolbar">

		<div class="filter-search btn-group pull-left">	
			<!-- <label class="filter-search-lbl" for="nit">NIT:</label> -->
			<input type="text" placeholder="NIT" name="nit" id="nit" class="plg-datatime" value="<?php echo $this->state->get( 'filter.nit' );?>">
		</div>

		<div class="filter-search btn-group pull-left">
			<!-- <label class="filter-search-lbl" for="razon_social">Raz&oacute;n social :</label> -->
			<input type="text" placeholder="Periodo" name="periodo" id="periodo" value="<?php echo $this->state->get( 'filter.periodo' );?>">
		</div>

		<div class="filter-search btn-group pull-left">
                <?php
                    echo JHTML::calendar($this->state->get( 'filter.inicial' ),'inicial', 'date', '%Y-%m-%d',array('size'=>'8','maxlength'=>'10','class'=>' validate[\'required\']',));
                ?>
            </div>

             <div class="filter-search btn-group pull-left">
                <?php
                    echo JHTML::calendar($this->state->get( 'filter.final' ),'final', 'date1', '%Y-%m-%d',array('size'=>'8','maxlength'=>'10','class'=>' validate[\'required\']',));
                ?>
            </div>
				
		<div class="filter-search btn-group pull-left">
			<button type="submit" class="btn hasTooltip" title="" data-original-title="Buscar">
				<span class="icon-search"></span>
			</button>
			<button type="button" class="btn hasTooltip" title="" onclick="document.getElementById('nit').value='';document.getElementById('periodo').value='';document.getElementById('date').value='';document.getElementById('date1').value='';this.form.submit();" data-original-title="Limpiar">
				<span class="icon-remove"></span>
			</button>
		</div>
	</div>
	<div class="clr"> </div>

	<table class="table table-striped">
		<thead>
			<tr>
				
				<th class="center nowrap">
					<?php echo JHtml::_('grid.sort', 'Id de la nomina', 'a.id', $listDirn, $listOrder); ?>
				</th>
				<th class="center nowrap">
					NIT
				</th>
				<th class="center nowrap">
					Periodo
				</th>
				<th class="center nowrap">
					Fecha generacion
				</th>
				<th class="center nowrap">
					Valor Total
				</th>
				<th class="center nowrap">
					Descargar
				</th>
				<th class="center nowrap">
					Regenerar			
				</th>
				<th class="center nowrap">
					Enviar Desprendibles			
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="9">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) :
			$periodo = date('Y/m/d',$item->periodo);

			$periodo1 = strtotime ( '-16 day' , strtotime ( $periodo ) ) ;

			$periodo1 = date ( 'd/m/Y' , $periodo1 );
		?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center"><?php echo $item->id?></td>
				<td class="center"><?php echo ucwords($item->nit)?></td>
				<td class="center"><?php echo $periodo1?> - <?php echo date('d/m/Y',$item->periodo)?></td>
				<td class="center"><?php echo date('d/m/Y',$item->fecha)?></td>
				<td class="center"><?php echo $item->valor?></td>
				<td class="center"><a href="<?php echo JRoute::_('index.php?option=com_nomina&task=liquidador.downloadNomina&id='.$item->id);?>">Descargar</a></td>
				<td class="center"><a href="<?php echo JRoute::_('index.php?option=com_nomina&view=liquidador&id='.$item->id);?>">Regenerar</a></td>
				<td class="center"><a href="<?php echo JRoute::_('index.php?option=com_nomina&task=liquidador.enviarDesprendibles&id='.$item->id);?>">Enviar Desprendibles</a></td>
			</tr>
		<?php 
		endforeach; 
		?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>