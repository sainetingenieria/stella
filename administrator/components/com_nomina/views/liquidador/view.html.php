<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewLiquidador extends JViewLegacy {

	protected $items;
	protected $liquidaciones;
	protected $pagination;
	protected $state;

	// Function that initializes the view
	public function display( $tpl = null ){

		$session = JFactory::getSession();
		$results = $session->get('results');		

		$this->liquidaciones		= $results;
		
		$this->items		= $this->get('Objects');
		$this->pagination 	= $this->get('Pagination');
		$this->state		= $this->get('State');
	
		// Add the toolbar with the CRUD modules defined
		$this->addToolbar();
	
		parent::display( $tpl );
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){

		if ($this->getLayout() == 'list') {
			JToolBarHelper::back( "Volver" );
		}
		
		JToolBarHelper::title( "Generar Nomina", 'list' );
		
	}

}
?>