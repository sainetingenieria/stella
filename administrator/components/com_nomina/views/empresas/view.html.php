<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewEmpresas extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;
	protected $empresa;
	// Function that initializes the view
	public function display( $tpl = null ){

		$this->items		= $this->get('Objects');
		$this->pagination 	= $this->get('Pagination');
		$this->state		= $this->get('State');
	
		// Add the toolbar with the CRUD modules defined
		$this->addToolbar();
	
		parent::display( $tpl );
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
			
		if ($this->getLayout() == 'list') {
			JToolBarHelper::title( "Empresas", 'list' );
			JToolBarHelper::deleteList('', 'empresas.delete');
			JToolBarHelper::divider();
			JToolBarHelper::editList('empresas.edit');
		}elseif ($this->getLayout() == 'edit') {
			JToolBarHelper::title( "Editar empresa", 'vcard' );
			JToolbarHelper::save('empresas.save');
		}else{
			JToolBarHelper::title( "Carga Información Empresas", 'edit' );
		}


	}

}
?>