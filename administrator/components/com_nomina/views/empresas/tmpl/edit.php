<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.
JHtml::_('behavior.formvalidator');
JHtml::_('behavior.framework');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
		var url = "'.JURI::base().'"
	' );

JFactory::getDocument()->addScriptDeclaration("
    Joomla.submitbutton = function(task)
    {
        if (document.formvalidator.isValid(document.getElementById('adminForm')))
        {
            Joomla.submitform(task, document.getElementById('adminForm'));
        }
    };

  
");


$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

//fb( $this->state );
?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=empresas&layout=list');?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal">

	<div class="tab-content" id="myTabContent">
			
		<div id="details" class="tab-pane active">
			<div class="control-group">
				<div class="control-label">
					<label id="razon_social" for="razon_social" class="hasTooltip required" title="" data-original-title="<strong>Razon social</strong><br />Digite la razon social">
					Razon social
					<span class="star">&nbsp;*</span>
					</label>						
				</div>
				<div class="controls">
					<input type="text" name="razon_social" id="razon_social" value="<?php echo $this->empresa->razon_social ?>" class="required" size="30" required="required" aria-required="true">						
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<label id="nit" for="nit" class="hasTooltip required" title="" data-original-title="<strong>Razon social</strong><br />Digite la razon social">
					NIT
					<span class="star">&nbsp;*</span>
					</label>						
				</div>
				<div class="controls">
					<input type="text" name="nit" id="nit" value="<?php echo $this->empresa->nit ?>" class="required" size="30" required="required" aria-required="true">						
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<label id="digito_verificacion" for="digito_verificacion" class="hasTooltip required" title="" data-original-title="<strong>Razon social</strong><br />Digite la razon social">
					Digito Verificación
					<span class="star">&nbsp;*</span>
					</label>						
				</div>
				<div class="controls">
					<input type="text" name="digito_verificacion" id="digito_verificacion" value="<?php echo $this->empresa->digito_verificacion ?>" class="required" size="30" required="required" aria-required="true">						
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<label id="direccion" for="direccion" class="hasTooltip required" title="" data-original-title="<strong>Razon social</strong><br />Digite la razon social">
					Direccion
					<span class="star">&nbsp;*</span>
					</label>						
				</div>
				<div class="controls">
					<input type="text" name="direccion" id="direccion" value="<?php echo $this->empresa->direccion ?>" class="required" size="30" required="required" aria-required="true">						
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<label id="ciudad" for="ciudad" class="hasTooltip required" title="" data-original-title="<strong>Razon social</strong><br />Digite la razon social">
					Ciudad
					<span class="star">&nbsp;*</span>
					</label>						
				</div>
				<div class="controls">
					<input type="text" name="ciudad" id="ciudad" value="<?php echo $this->empresa->ciudad ?>" class="required" size="30" required="required" aria-required="true">						
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<label id="telefono" for="telefono" class="hasTooltip required" title="" data-original-title="<strong>Razon social</strong><br />Digite la razon social">
					Teléfono
					<span class="star">&nbsp;*</span>
					</label>						
				</div>
				<div class="controls">
					<input type="text" name="telefono" id="telefono" value="<?php echo $this->empresa->telefono ?>" class="required" size="30" required="required" aria-required="true">						
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<label id="encargado" for="encargado" class="hasTooltip required" title="" data-original-title="<strong>Razon social</strong><br />Digite la razon social">
					Encargado
					<span class="star">&nbsp;*</span>
					</label>						
				</div>
				<div class="controls">
					<input type="text" name="encargado" id="encargado" value="<?php echo $this->empresa->encargado ?>" class="required" size="30" required="required" aria-required="true">						
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<label id="cargo" for="cargo" class="hasTooltip required" title="" data-original-title="<strong>Razon social</strong><br />Digite la razon social">
					Cargo
					<span class="star">&nbsp;*</span>
					</label>						
				</div>
				<div class="controls">
					<input type="text" name="cargo" id="cargo" value="<?php echo $this->empresa->cargo ?>" class="required" size="30" required="required" aria-required="true">						
				</div>
			</div>
		</div>
	</div>

	<div>
		<input type="hidden" name="id" value="<?php echo $this->empresa->id; ?>" />

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>