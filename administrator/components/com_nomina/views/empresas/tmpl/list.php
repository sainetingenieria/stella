<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.
//JHtml::_('behavior.tooltip');
//JHtml::_('behavior.multiselect');
//JHtml::_('behavior.modal');
JHtml::_('behavior.framework');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
		var url = "'.JURI::base().'"
	' );

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

//fb( $this->state );
?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=empresas&layout=list');?>" method="post" name="adminForm" id="adminForm">


	<div id="filter-bar" class="btn-toolbar">

		<div class="filter-search btn-group pull-left">	
			<!-- <label class="filter-search-lbl" for="nit">NIT:</label> -->
			<input type="text" placeholder="NIT" name="nit" id="nit" class="plg-datatime" value="<?php echo $this->state->get( 'filter.nit' );?>">
		</div>

		<div class="filter-search btn-group pull-left">
			<!-- <label class="filter-search-lbl" for="razon_social">Raz&oacute;n social :</label> -->
			<input type="text" placeholder="Raz&oacute;n social" name="razon_social" id="razon_social" value="<?php echo $this->state->get( 'filter.razon_social' );?>">
		</div>

		<div class="filter-search btn-group pull-left">
			<!-- <label class="filter-search-lbl" for="digito_verificacion">Digito Verificaci&oacute;n:</label> -->
			<input size="70" type="text" placeholder="Digito Verificaci&oacute;n:" name="digito_verificacion" id="digito_verificacion" value="<?php echo $this->state->get( 'filter.digito_verificacion' );?>">	
		</div>
				
		<div class="filter-search btn-group pull-left">
			<button type="submit" class="btn hasTooltip" title="" data-original-title="Buscar">
				<span class="icon-search"></span>
			</button>
			<button type="button" class="btn hasTooltip" title="" onclick="document.getElementById('nit').value='';document.getElementById('razon_social').value='';document.getElementById('digito_verificacion').value='';this.form.submit();" data-original-title="Limpiar">
				<span class="icon-remove"></span>
			</button>
		</div>
	</div>
	<div class="clr"> </div>

	<table class="table table-striped">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="center nowrap">
					<?php echo JHtml::_('grid.sort', 'Id de la empresa', 'a.id', $listDirn, $listOrder); ?>
				</th>
				<th class="center nowrap">
					NIT
				</th>
				<th class="center nowrap">
					Digito verificaci&oacute;n
				</th>
				<th class="center nowrap">
					Raz&oacute;n social
				</th>
				<th class="center nowrap">
					Direcci&oacute;n
				</th>
				<th class="center nowrap">
					Ciudad
				</th>
				<th class="center nowrap">
					Tel&eacute;fono					
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="9">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) :

		?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				<td class="center"><?php echo $item->id?></td>
				<td class="center"><?php echo ucwords($item->nit)?></td>
				<td class="center"><?php echo ucfirst($item->digito_verificacion)?></td>
				<td class="center"><?php echo ucfirst($item->razon_social)?></td>
				<td class="center"><?php echo $item->direccion?></td>
				<td class="center"><?php echo $item->ciudad?></td>
				<td class="center"><?php echo $item->telefono?></td>
			</tr>
		<?php 
		endforeach; 
		?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>