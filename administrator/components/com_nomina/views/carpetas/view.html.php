<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewCarpetas extends JViewLegacy {

	// Function that initializes the view
	public function display( $tpl = null ){

		
	
		// Add the toolbar with the CRUD modules defined
		$this->addToolbar();
	
		parent::display( $tpl );
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
		
		JToolBarHelper::title( "Gestión de Carpetas", 'folder' );
		JToolbarHelper::save('carpetas.save');

		
	}

}
?>