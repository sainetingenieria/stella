<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.formvalidator');

$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
        var url = "'.JURI::base().'"
    ' );


JFactory::getDocument()->addScriptDeclaration("
    Joomla.submitbutton = function(task)
    {
        if (document.formvalidator.isValid(document.getElementById('adminForm')))
        {
            Joomla.submitform(task, document.getElementById('adminForm'));
        }
    };

  
");

$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=carpetas');?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal">
    <div class="tab-content" id="myTabContent">
            
        <div id="details" class="tab-pane active">
            <div class="control-group">
                <div class="control-label">
                    <label id="nombre" for="nombre" class="hasTooltip required" title="" data-original-title="<strong>Nombre de la Carpeta</strong><br />Digite el nombre de la carpeta">
                    Nombre carpeta
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <input type="text" name="nombre" id="nombre" value="" class="required" size="30" required="required" aria-required="true">                        
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="empresa" for="empresa" class="hasTooltip required" title="" data-original-title="<strong>Empresa</strong><br />Seleccione la empresa">
                    Empresa
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $empresa = JRequest::getVar('empresa');

                        echo JHTML::_('select.genericlist', Helper::getEmpresas(), 'empresa', 'class="inputbox" onchange="this.form.submit()"', 'value', 'text', $empresa);
                    ?>
                </div>
            </div>


            <div class="control-group">
                <div class="control-label">
                    <label id="empleado" for="empleado" class="hasTooltip required" title="" data-original-title="<strong>Empleado</strong><br />Seleccione el empleado">
                    Empleado
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php 
                        $empleado = JRequest::getVar('id_empleado');

                        if (isset($empresa)) {
                            echo JHTML::_('select.genericlist', Helper::getEmpleados($empresa), 'id_empleado', 'class="inputbox" onchange="this.form.submit()"', 'value', 'text', $empleado);
                        }
                    ?>
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="carpetas" for="carpetas" class="hasTooltip required" title="" data-original-title="<strong>Carpeta padreo</strong><br />Seleccione la carpeta padre si es necesario">
                   Carpeta Padre
                    </label>                        
                </div>
                <div class="controls">
                    <?php 

                    if (isset($empleado)) {
                        echo JHTML::_('select.genericlist', Helper::getCarpetas($empleado), 'id_padre', 'class="inputbox"', 'value', 'text', $default);
                    }
                    ?>                      
                </div>
            </div>
        </div>
    </div>

    <div>
        <input type="hidden" name="id" value="" />

        <input type="hidden" name="task" value="" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <input type="hidden" name="boxchecked" value="0" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>