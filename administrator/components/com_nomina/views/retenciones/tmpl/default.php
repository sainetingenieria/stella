<style>
.boton{
    background:#0D98FB; 
    background-image: -webkit-linear-gradient(top,#0D98FB,#1A5DB3);
    background-image: -moz-linear-gradient(top,#0D98FB,#1A5DB3);
    background-image: -o-linear-gradient(top,#0D98FB,#1A5DB3);  
    background-image: linear-gradient(to bottom,#0D98FB,#1A5DB3);   
    border: 1px solid #125CB5;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -o-border-radius: 5px;  
    border-radius: 5px;
    -moz-box-shadow: 0 1px 1px #71C0FD inset;
    -webkit-box-shadow: 0 1px 1px #71C0FD inset;
    -o-box-shadow: 0 1px 1px #71C0FD inset; 
    box-shadow: 0 1px 1px #71C0FD inset;
    padding: .8em 1.8em;
    color: white;
    font-weight:normal;
    text-decoration:none;
}
.boton:hover {
    background:#1A5DB3;
    background-image: -webkit-linear-gradient(bottom,#0D98FB,#1A5DB3);
    background-image: -moz-linear-gradient(bottom,#0D98FB,#1A5DB3);
    background-image: -o-linear-gradient(bottom,#0D98FB,#1A5DB3);   
    background-image: linear-gradient(to top,#0D98FB,#1A5DB3);
    cursor: pointer;
}
</style>

<?php
// Joomla calls and runtimes
defined('_JEXEC') or die();

//get the hosts name
jimport('joomla.environment.uri');
// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');
$host = JURI::root();

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document = & JFactory::getDocument();
$document->addStyleSheet($host . 'administrator/components/com_nomina/assets/js/libs/fileuploader/fileuploader.css');
$document->addStyleSheet($host . 'administrator/components/com_nomina/assets/css/style.css');

$document->addScript($host . 'administrator/components/com_nomina/assets/js/libs/fileuploader/fileuploader.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/misc/misc.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/views/retenciones.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/models/retenciones.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/controllers/retenciones.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/handlers/retenciones.js');
$document->addScript($host . 'administrator/components/com_nomina/assets/js/misc/retenciones_file.js');
?>

<form action="<?php echo JRoute::_(''); ?>" method="post" name="adminForm" id="adminForm">
    <fieldset>
        <legend>Carga de Excel</legend>

        <p>Asegúrese que su archivo de Excel conserva el mismo formato, puesto que los campos adicionales que no coincidan con la base de datos no se guardarán. <a href="<?php echo $host; ?>administrator/components/com_nomina/assets/examples/retenciones.xlsx">Ejemplo Formato de Carga Retenciones</a></p>
        <ul class="excel-list">
        </ul>
        <div id="excel-uploader">       
            <noscript>          
            <p>habilita javascript en tu navegador para poder subir archivos.</p>
            <!-- or put a simple form for upload here -->
            </noscript>         
        </div>

        <p class="import-label">Cuando todos los archivos indiquen 100%, haga click en "Importar" para comenzar la importación de datos.</p>
        <input type="button" href="#" id="importar-datos" class="qq-upload-button importar-datos" value="Importar datos">

                <!-- <p class="truncate-label"></p> -->
        
        <ul class="excel-parse-list"></ul>


        <div class="excel-reporte">
            <h3>Reporte</h3>
            <ul>
                <li>Errores en la conversión: <span class="excel-errors">0</span></li>
            </ul>
            <div class="excel-log">
            </div>
        </div>

    </fieldset>

    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <?php echo JHtml::_('form.token'); ?>
    </div>

</form>