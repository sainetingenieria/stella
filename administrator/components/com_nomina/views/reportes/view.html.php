<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewReportes extends JViewLegacy {

	protected $items;
	protected $asign;

	// Function that initializes the view
	public function display( $tpl = null ){

	
		// Add the toolbar with the CRUD modules defined
		$this->addToolbar();
	
		parent::display( $tpl );
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
		

		if ($this->getLayout() == 'default') {
			JToolBarHelper::title( "Reportes", 'list' );
			JToolbarHelper::apply('reportes.asignar', 'Ver Filtros');

		}elseif ($this->getLayout() == 'asignar') {
			JToolBarHelper::title( "Reportes", 'list' );
			JToolbarHelper::save('reportes.save', 'Asignar Reporte');
		}

		
	}

}
?>