<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
//get the hosts name
jimport('joomla.environment.uri');
// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');
$host = JURI::root();
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
    var url = "'.JURI::base().'"
' );

$doc->addStyleSheet($host . 'administrator/components/com_nomina/assets/js/libs/select/dist/css/select2.css');
$doc->addScript('https://www.gstatic.com/charts/loader.js');
$doc->addScript($host . 'administrator/components/com_nomina/assets/js/libs/select/dist/js/select2.full.js');
$doc->addScript($host . 'administrator/components/com_nomina/assets/js/libs/fileuploader/fileuploader.js');
$doc->addScript($host . 'administrator/components/com_nomina/assets/js/misc/misc.js');
$doc->addScript($host . 'administrator/components/com_nomina/assets/js/views/liquidador.js');
$doc->addScript($host . 'administrator/components/com_nomina/assets/js/models/liquidador.js');
$doc->addScript($host . 'administrator/components/com_nomina/assets/js/controllers/liquidador.js');
$doc->addScript($host . 'administrator/components/com_nomina/assets/js/handlers/liquidador.js');


$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

//fb( $this->state );
?>


<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=reportes&layout=asignar');?>" method="post" name="adminForm" id="adminForm">

    <?php if ($this->asign['tipo_reporte'] == '1'): ?>
        
        <div id="filter-bar" class="btn-toolbar">

            <div class="filter-search btn-group pull-left">
                <?php
                    echo JHTML::calendar($this->asign['inicial'],'inicial', 'date', '%Y-%m-%d',array('size'=>'8','maxlength'=>'10','class'=>' validate[\'required\']',));
                ?>
            </div>

             <div class="filter-search btn-group pull-left">
                <?php
                    echo JHTML::calendar($this->asign['final'],'final', 'date1', '%Y-%m-%d',array('size'=>'8','maxlength'=>'10','class'=>' validate[\'required\']',));
                ?>
            </div>

            <div class="filter-search btn-group pull-left">

                <?php 
                    $departamentos = Helper::getDepartamentos();

                ?>
                <select id="dep" multiple="multiple" name="departamento">
                    <?php foreach ($departamentos as $key => $departamento): ?>
                        <?php $select = ''; ?>
                        <?php if ($this->asign['departamento'] == $departamento->departamento): ?>
                            <?php $select = 'selected'; ?>
                        <?php endif ?>

                        <option value="<?php echo $departamento->departamento ?>" <?php echo $select; ?>><?php echo $departamento->departamento ?></option>
                    <?php endforeach ?>

                </select>
            </div>


            <div class="filter-search btn-group pull-left">
                 <!-- <label class="filter-search-lbl" for="razon_social">Departamentos:</label> -->
                <input type="text" placeholder="Rango de Pesos Minimo" name="min" id="min" value="<?php echo $this->asign['min'] ?>">

            </div>

            <div class="filter-search btn-group pull-left">
                 <!-- <label class="filter-search-lbl" for="razon_social">Departamentos:</label> -->
                <input type="text" placeholder="Rango de Pesos Maximo" name="max" id="max" value="<?php echo $this->asign['max'] ?>">

            </div>

                    
            <div class="filter-search btn-group pull-left">
                <button type="submit" class="btn hasTooltip" title="" data-original-title="Buscar">
                    <span class="icon-search"></span>
                </button>
                <button type="button" class="btn hasTooltip" title="" onclick="document.getElementById('min').value='';document.getElementById('max').value='';document.getElementById('date').value='';document.getElementById('date1').value='';document.getElementById('dep').value='';this.form.submit();" data-original-title="Limpiar">
                    <span class="icon-remove"></span>
                </button>
            </div>
        </div>
        <div class="clr"> </div>

        <table class="table table-striped">
            <thead>
                <tr>         
                    <th class="center nowrap">
                        Raz&oacute;n social
                    </th>
                    <th class="center nowrap">
                        NIT
                    </th>
                    <th class="center nowrap">
                        Departamento
                    </th>
                    <th class="center nowrap">
                        Periodo
                    </th>
                    <th class="center nowrap">
                        C&eacute;dula
                    </th>
                    <th class="center nowrap">
                        Nombres y Apellidos               
                    </th>
                    <th class="center nowrap">
                        Fecha Ingreso              
                    </th>
                    <th class="center nowrap">
                        Tipo de Contrato
                    </th>
                    <th class="center ">
                        Salario
                    </th>
                    <th class="center ">
                        Cargo
                    </th>
                    <th class="center ">
                        Sueldo
                    </th>
                    <th class="center ">
                        Auxilio de Transporte
                    </th>
                    <th class="center ">
                        Auxilio de Alimentaci&oacute;n
                    </th>
                    <th class="center ">
                        Auxilio de Educaci&oacute;n
                    </th>
                    <th class="center ">
                        Auxilio de Vivienda
                    </th>
                    <th class="center ">
                        Otros Auxilios
                    </th>
                    <th class="center ">
                        Salud
                    </th>
                    <th class="center ">
                        Pensi&oacute;n
                    </th>
                    <th class="center ">
                        Retenci&oacute;n en la fuente
                    </th>
                    <th class="center ">
                        Total Pagos
                    </th>
                    <th class="center ">
                        Total descuentos
                    </th>
                    <th class="center ">
                        Neto a Pagar
                    </th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($this->items as $i => $item) :

                $periodo = date('Y/m/d',$item->periodo);

                $periodo1 = strtotime ( '-16 day' , strtotime ( $periodo ) ) ;

                $periodo1 = date ( 'd/m/Y' , $periodo1 );

            ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="center"><?php echo ucfirst($item->razon_social)?></td>
                    <td class="center"><?php echo $item->nit?></td>
                    <td class="center"><?php echo ucfirst($item->departamento)?></td>
                    <td class="center"><?php echo $periodo1?> - <?php echo date('d/m/Y',$item->periodo)?></td>
                    <td class="center"><?php echo $item->numero_identificacion?></td>
                    <td class="center"><?php echo ucfirst($item->nombres)?> <?php echo ucfirst($item->apellidos)?></td>
                    <td class="center"><?php echo date('y-m-d',$item->fecha_de_ingreso)?></td>
                    <td class="center"><?php $tipo_contrato = ($item->tipo_contrato == 'i') ? 'Termino Indefinido' : 'Termino Fijo'; echo $tipo_contrato?></td>
                    <td class="center"><?php echo $item->sueldo_basico?></td>
                    <td class="center"><?php echo ucfirst($item->cargo)?></td>
                    <td class="center"><?php echo $item->basico?></td>
                    <td class="center"><?php echo $item->aux_transporte?></td>
                    <td class="center"><?php echo $item->aux_alimentacion?></td>
                    <td class="center"><?php echo $item->auxilio_de_educacion?></td>
                    <td class="center"><?php echo $item->auxilio_de_vivienda?></td>
                    <td class="center"><?php echo $item->otros_auxilios?></td>
                    <td class="center"><?php echo $item->salud_descuento?></td>
                    <td class="center"><?php echo $item->pension?></td>
                    <td class="center"><?php echo $item->retencion_fuente?></td>
                    <td class="center"><?php echo $item->total_pagos?></td>
                    <td class="center"><?php echo $item->total_deducciones?></td>
                    <td class="center"><?php echo $item->neto_pagado?></td>
                </tr>
            <?php 
            endforeach; 
            ?>
            </tbody>
        </table>
    <?php endif ?>

    <?php if ($this->asign['tipo_reporte'] == '3'): ?>

        <div id="filter-bar" class="btn-toolbar">

             <div class="filter-search btn-group pull-left">
                <?php
                    echo JHTML::calendar($this->asign['inicial'],'inicial', 'date', '%Y-%m-%d',array('size'=>'8','maxlength'=>'10','class'=>' validate[\'required\']',));
                ?>
            </div>

             <div class="filter-search btn-group pull-left">
                <?php
                    echo JHTML::calendar($this->asign['final'],'final', 'date1', '%Y-%m-%d',array('size'=>'8','maxlength'=>'10','class'=>' validate[\'required\']',));
                ?>
            </div>

            <div class="filter-search btn-group pull-left">

                <?php 
                    $departamentos = Helper::getDepartamentos();

                ?>
                <select id="dep" multiple="multiple" name="departamento">
                    <?php foreach ($departamentos as $key => $departamento): ?>
                        <?php $select = ''; ?>
                        <?php if ($this->asign['departamento'] == $departamento->departamento): ?>
                            <?php $select = 'selected'; ?>
                        <?php endif ?>

                        <option value="<?php echo $departamento->departamento ?>" <?php echo $select; ?>><?php echo $departamento->departamento ?></option>
                    <?php endforeach ?>

                </select>
            </div>


            <div class="filter-search btn-group pull-left">
                 <!-- <label class="filter-search-lbl" for="razon_social">Departamentos:</label> -->
                <input type="text" placeholder="Costo del Empleado en Pesos Minimo" name="min" id="min" value="<?php echo $this->asign['min'] ?>">

            </div>

            <div class="filter-search btn-group pull-left">
                 <!-- <label class="filter-search-lbl" for="razon_social">Departamentos:</label> -->
                <input type="text" placeholder="Costo del Empleado en Pesos Maximo" name="max" id="max" value="<?php echo $this->asign['max'] ?>">

            </div>

                    
            <div class="filter-search btn-group pull-left">
                <button type="submit" class="btn hasTooltip" title="" data-original-title="Buscar">
                    <span class="icon-search"></span>
                </button>
                <button type="button" class="btn hasTooltip" title="" onclick="document.getElementById('min').value='';document.getElementById('max').value='';document.getElementById('date').value='';document.getElementById('date1').value='';document.getElementById('dep').value='';this.form.submit();" data-original-title="Limpiar">
                    <span class="icon-remove"></span>
                </button>
            </div>
        </div>
        <div class="clr"> </div>

        <table class="table table-striped">
            <thead>
                <tr>         
                    <th class="center nowrap">
                        NIT
                    </th>
                    <th class="center nowrap">
                        Raz&oacute;n social
                    </th>
                    <th class="center nowrap">
                        Periodo
                    </th>
                    <th class="center nowrap">
                        N&uacute;mero de identidad
                    </th>
                    <th class="center nowrap">
                        Nombres y Apellidos               
                    </th>
                    <th class="center nowrap">
                        Departamento
                    </th>
                    <th class="center nowrap">
                        Costo del Empleado            
                    </th>
                  
                </tr>
            </thead>
            <tbody>
            <?php foreach ($this->items as $i => $item) :

            ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="center"><?php echo ucwords($item->nit)?></td>
                    <td class="center"><?php echo ucfirst($item->razon_social)?></td>
                    <td class="center"><?php echo $item->periodo?></td>
                    <td class="center"><?php echo $item->numero_identificacion?></td>
                    <td class="center"><?php echo ucfirst($item->nombres)?> <?php echo ucfirst($item->apellidos)?></td>
                    <td class="center"><?php echo ucfirst($item->departamento)?></td>

                    <td class="center"><?php echo Misc::numberDots($item->costo_empleado)?></td>
                </tr>
            <?php 
            endforeach; 
            ?>
            </tbody>
        </table>

        <table style="margin: 0 auto;">
            <tr>
                <td>
                    <div id="piechart_3d" style="width: 600px; height: 400px;"></div>
                </td>
                <td>
                    <div id="columnchart_values" style="width: 700px; height: 500px;"></div>
                </td>
            </tr>
        </table>
        <input type="hidden" name="chart1" id="chart1" value="" />
        <input type="hidden" name="chart2" id="chart2" value="" />


        
    <?php endif ?>


    <div>
        <input type="hidden" name="empresa" value="<?php echo $this->asign['empresa']; ?>" />
        <input type="hidden" name="tipo_reporte" value="<?php echo $this->asign['tipo_reporte']; ?>" />
        <input type="hidden" name="nombre" value="<?php echo $this->asign['nombre']; ?>" />



        <input type="hidden" name="task" value="reportes.filter" />
        <input type="hidden" name="boxchecked" value="0" />
        <?php echo JHtml::_('form.token'); ?>
    </div>

</form>

<script type="text/javascript">

    jQuery('#dep').select2({
      placeholder: "Seleccione un departamento"
    });

</script>