<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.formvalidator');

$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
        var url = "'.JURI::base().'"
    ' );


JFactory::getDocument()->addScriptDeclaration("
    Joomla.submitbutton = function(task)
    {
        if (document.formvalidator.isValid(document.getElementById('adminForm')))
        {
            Joomla.submitform(task, document.getElementById('adminForm'));
        }
    };

  
");

$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=reportes');?>" method="post" name="adminForm" enctype="multipart/form-data" id="adminForm" class="form-validate form-horizontal">
    <div class="tab-content" id="myTabContent">
            
        <div id="details" class="tab-pane active">
            

            <div class="control-group">
                <div class="control-label">
                    <label id="empresa" for="empresa" class="hasTooltip required" title="" data-original-title="<strong>Empresa</strong><br />Seleccione la empresa">
                    Empresa
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php

                        $empresa = JRequest::getVar('empresa');

                        echo JHTML::_('select.genericlist', Helper::getEmpresas(), 'empresa', 'class="inputbox required" required="required" aria-required="true"', 'value', 'text', $empresa);
                    ?>
                </div>
            </div>


            <div class="control-group">
                <div class="control-label">
                    <label id="tipo_reporte" for="tipo_reporte" class="hasTooltip required" title="" data-original-title="<strong>Tipo de reporte</strong><br />Seleccione el tipo de reporte">
                    Tipo de reporte
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php 

                        $tipo_reporte = JRequest::getVar('tipo_reporte');
                        echo JHTML::_('select.genericlist', array('' => 'Seleccione el tipo de reporte','1'=>'Informe detallado de Nómina', '3'=>'Costo Periódico'), 'tipo_reporte', 'class="inputbox required" required="required" aria-required="true"', 'value', 'text', $tipo_reporte);
                    ?>
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="nombre" for="nombre" class="hasTooltip required" title="" data-original-title="<strong>Nombre del reporte</strong><br />Digite el nombre del reporte">
                    Nombre
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php $nombre = JRequest::getVar('nombre'); ?>
                    <input type="text" name="nombre" id="nombre" value="<?php echo $nombre ?>" class="required" size="30" required="required" aria-required="true">                     
                </div>
            </div>

          

        </div>
    </div>

    <div>
        <input type="hidden" name="id" value="<?php echo $this->archivo->id ?>" />

        <input type="hidden" name="task" value="" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <input type="hidden" name="boxchecked" value="0" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>