<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewArchivos extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;
	protected $archivo;
	// Function that initializes the view
	public function display( $tpl = null ){

		$this->items		= $this->get('Objects');
		$this->pagination 	= $this->get('Pagination');
		$this->state		= $this->get('State');

		
	
		// Add the toolbar with the CRUD modules defined
		$this->addToolbar();
	
		parent::display( $tpl );
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
		

		if ($this->getLayout() == 'default') {
			JToolBarHelper::title( "Gestión de archivo", 'list' );
			JToolBarHelper::deleteList('', 'archivos.delete');
			JToolBarHelper::divider();
			JToolBarHelper::editList('archivos.edit');
			JToolBarHelper::divider();
			JToolBarHelper::addNew('archivos.newArchivo');
		}elseif ($this->getLayout() == 'edit') {
			JToolBarHelper::title( "Editar archivo", 'vcard' );
			JToolbarHelper::save('archivos.save');
		}

		
	}

}
?>