<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.
//JHtml::_('behavior.tooltip');
//JHtml::_('behavior.multiselect');
//JHtml::_('behavior.modal');
JHtml::_('behavior.framework');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
        var url = "'.JURI::base().'"
    ' );

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

//fb( $this->state );
?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=archivos');?>" method="post" name="adminForm" id="adminForm">


    <div id="filter-bar" class="btn-toolbar">

        <div class="filter-search btn-group pull-left"> 
            <!-- <label class="filter-search-lbl" for="nit">NIT:</label> -->
            <input type="text" placeholder="NIT" name="nit" id="nit" class="plg-datatime" value="<?php echo $this->state->get( 'filter.nit' );?>">
        </div>

        <div class="filter-search btn-group pull-left">
            <!-- <label class="filter-search-lbl" for="razon_social">Raz&oacute;n social :</label> -->
            <input type="text" placeholder="Nombre Archivo" name="nombre" id="nombre" value="<?php echo $this->state->get( 'filter.nombre' );?>">
        </div>

        <div class="filter-search btn-group pull-left">
            <!-- <label class="filter-search-lbl" for="digito_verificacion">Digito Verificaci&oacute;n:</label> -->
            <input size="70" type="text" placeholder="Identificacion Empleado" name="numero_identificacion" id="numero_identificacion" value="<?php echo $this->state->get( 'filter.numero_identificacion' );?>"> 
        </div>
                
        <div class="filter-search btn-group pull-left">
            <button type="submit" class="btn hasTooltip" title="" data-original-title="Buscar">
                <span class="icon-search"></span>
            </button>
            <button type="button" class="btn hasTooltip" title="" onclick="document.getElementById('nit').value='';document.getElementById('nombre').value='';document.getElementById('numero_identificacion').value='';this.form.submit();" data-original-title="Limpiar">
                <span class="icon-remove"></span>
            </button>
        </div>
    </div>
    <div class="clr"> </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th width="1%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th class="center nowrap">
                    <?php echo JHtml::_('grid.sort', 'Id del archivo', 'a.id', $listDirn, $listOrder); ?>
                </th>
                <th class="center nowrap">
                    Nombre del archivo
                </th>
                <th class="center nowrap">
                    Descripci&oacute;n
                </th>
                <th class="center nowrap">
                    Link
                </th>
                <th class="center nowrap">
                    Raz&oacute;n social del empleado
                </th>
                <th class="center nowrap">
                    Nombres y Apellidos del Empleado
                </th>
                <th class="center nowrap">
                    N&uacute;mero de identificacion del empleado              
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="9">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
        <?php foreach ($this->items as $i => $item) :

        ?>
            <tr class="row<?php echo $i % 2; ?>">
                <td class="center">
                    <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                </td>
                <td class="center"><?php echo $item->id?></td>
                <td class="center"><?php echo ucwords($item->nombre)?></td>
                <td class="center"><?php echo ucfirst($item->descripcion)?></td>
                <td class="center"><a href="<?php echo JURI::root() ?>pdfs/<?php echo $item->archivo; ?>"><?php echo ucfirst($item->nombre)?></a></td>
                <td class="center"><?php echo ucfirst($item->razon_social)?></td>
                <td class="center"><?php echo ucfirst($item->nombres)?> <?php echo ucfirst($item->apellidos)?></td>
                <td class="center"><?php echo $item->numero_identificacion?></td>
            </tr>
        <?php 
        endforeach; 
        ?>
        </tbody>
    </table>

    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <input type="hidden" name="boxchecked" value="0" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>