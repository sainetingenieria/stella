<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.formvalidator');

$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
        var url = "'.JURI::base().'"
    ' );


JFactory::getDocument()->addScriptDeclaration("
    Joomla.submitbutton = function(task)
    {
        if (document.formvalidator.isValid(document.getElementById('adminForm')))
        {
            Joomla.submitform(task, document.getElementById('adminForm'));
        }
    };

  
");

$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=archivos&layout=edit');?>" method="post" name="adminForm" enctype="multipart/form-data" id="adminForm" class="form-validate form-horizontal">
    <div class="tab-content" id="myTabContent">
            
        <div id="details" class="tab-pane active">
            <div class="control-group">
                <div class="control-label">
                    <label id="nombre" for="nombre" class="hasTooltip required" title="" data-original-title="<strong>Nombre archivo</strong><br />Digite el nombre del archivo">
                    Nombre archivo
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php 
                        
                        if(empty($this->archivo->nombre)){
                            $nombre = JRequest::getVar('nombre');
                        }else{
                            $nombre = $this->archivo->nombre;
                        }
                        

                    ?>
                    <input type="text" name="nombre" id="nombre" value="<?php echo $nombre; ?>" class="required" size="30" required="required" aria-required="true">                
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="empresa" for="empresa" class="hasTooltip required" title="" data-original-title="<strong>Empresa</strong><br />Seleccione la empresa">
                    Empresa
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php

                        if(empty($this->archivo->id_empleado)){
                            $empresa = JRequest::getVar('empresa');
                        }else{
                            $empresa = Helper::getEmpresaByEmpleado($this->archivo->id_empleado);
                        }

                        echo JHTML::_('select.genericlist', Helper::getEmpresas(), 'empresa', 'class="inputbox required" required="required" aria-required="true" onchange="this.form.submit()"', 'value', 'text', $empresa);
                    ?>
                </div>
            </div>


            <div class="control-group">
                <div class="control-label">
                    <label id="empleado" for="empleado" class="hasTooltip required" title="" data-original-title="<strong>Empleado</strong><br />Seleccione el empleado">
                    Empleado
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php 
                        if(empty($this->archivo->id_empleado)){
                            $empleado = JRequest::getVar('id_empleado');
                        }else{
                            $empleado = $this->archivo->id_empleado;
                        }

                        if (isset($empresa)) {
                            echo JHTML::_('select.genericlist', Helper::getEmpleados($empresa), 'id_empleado', 'class="inputbox required" required="required" aria-required="true" onchange="this.form.submit()"', 'value', 'text', $empleado);
                        }
                    ?>
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="carpetas" for="carpetas" class="hasTooltip required" title="" data-original-title="<strong>Carpeta padre</strong><br />Seleccione la carpeta padre si es necesario">
                   Carpeta Padre
                    </label>                        
                </div>
                <div class="controls">
                    <?php 

                    if (isset($empleado)) {

                        $default = $this->archivo->id_carpeta;
                        echo JHTML::_('select.genericlist', Helper::getCarpetas($empleado), 'id_carpeta', 'class="inputbox required" required="required" aria-required="true"', 'value', 'text', $default);
                    }
                    ?>                      
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="archivo" for="archivo" class="hasTooltip required" title="" data-original-title="<strong>Archivo a cargar</strong><br />Seleccione un archivo PDF">
                    Archivo
                    </label>                        
                </div>
                <div class="controls">
                    <?php 

                    if(empty($this->archivo->archivo)){
                    
                    ?>
                    <input class="input_box" id="archivo" name="archivo" type="file" size="57" class="required" required="required" aria-required="true" />
                    <?php
                    }else{
                    ?>
                    <input class="input_box" id="archivo" name="archivo" type="file" size="57" value="<?php echo $this->archivo->archivo ?>" />
                    <?php
                    }

                    ?>
                    
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="descripcion" for="descripcion" class="hasTooltip required" title="" data-original-title="<strong>Carpeta padreo</strong><br />Seleccione la carpeta padre si es necesario">
                    Descripción
                    </label>                  
                </div>
                <div class="controls">
                    <textarea name="descripcion"><?php echo $this->archivo->descripcion ?></textarea>
                </div>
            </div>
        </div>
    </div>

    <div>
        <input type="hidden" name="id" value="<?php echo $this->archivo->id ?>" />

        <input type="hidden" name="task" value="" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <input type="hidden" name="boxchecked" value="0" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>