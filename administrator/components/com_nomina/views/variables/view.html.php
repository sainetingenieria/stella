<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewVariables extends JViewLegacy {

	protected $variable;

	// Function that initializes the view
	public function display( $tpl = null ){

		$this->variable = $this->get('Objects');
	
		// Add the toolbar with the CRUD modules defined
		$this->addToolbar();
	
		parent::display( $tpl );
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
		
		JToolBarHelper::title( "Variables del Sistema", 'list' );
		JToolbarHelper::save('variables.save');
		
	}

}
?>