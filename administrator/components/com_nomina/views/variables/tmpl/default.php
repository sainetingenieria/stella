<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.formvalidator');

$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
        var url = "'.JURI::base().'"
    ' );

// $doc->addStyleSheet($host . 'components/com_nomina/assets/js/libs/multidatepicker/css/mdp.css');
$doc->addStyleSheet($host . 'components/com_nomina/assets/css/style.css');


$doc->addScript($host . 'components/com_nomina/assets/js/libs/multidatepicker/js/jquery-ui-1.11.1.js');
$doc->addScript($host . 'components/com_nomina/assets/js/libs/multidatepicker/jquery-ui.multidatespicker.js');
$doc->addScript($host . 'components/com_nomina/assets/js/views/liquidador.js');

JFactory::getDocument()->addScriptDeclaration("
    Joomla.submitbutton = function(task)
    {
        if (document.formvalidator.isValid(document.getElementById('adminForm')))
        {
            Joomla.submitform(task, document.getElementById('adminForm'));
        }
    };
");

$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

?>

<form action="<?php echo JRoute::_('index.php?option=com_nomina&view=variables&layout=edit');?>" method="post" name="adminForm" enctype="multipart/form-data" id="adminForm" class="form-validate form-horizontal">
    <div class="tab-content" id="myTabContent">
            
        <div id="details" class="tab-pane active">
            <div class="control-group">
                <div class="control-label">
                    <label id="clase_riesgo1" for="clase_riesgo1" class="hasTooltip required" title="" data-original-title="<strong>Clase Riesgo I</strong><br />Digite la tarifa de la clase de riesgo I">
                    Clase de Riesgo I
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $clase_riesgo1 = $this->variable[0]->clase_riesgo1;
                    ?>
                    <input type="text" name="clase_riesgo1" id="clase_riesgo1" value="<?php echo $clase_riesgo1; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="clase_riesgo2" for="clase_riesgo2" class="hasTooltip required" title="" data-original-title="<strong>Clase Riesgo II</strong><br />Digite la tarifa de la clase de riesgo II">
                    Clase de Riesgo II
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $clase_riesgo2 = $this->variable[0]->clase_riesgo2;
                    ?>
                    <input type="text" name="clase_riesgo2" id="clase_riesgo2" value="<?php echo $clase_riesgo2; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="clase_riesgo3" for="clase_riesgo3" class="hasTooltip required" title="" data-original-title="<strong>Clase Riesgo III</strong><br />Digite la tarifa de la clase de riesgo III">
                    Clase de Riesgo III
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $clase_riesgo3 = $this->variable[0]->clase_riesgo3;
                    ?>
                    <input type="text" name="clase_riesgo3" id="clase_riesgo3" value="<?php echo $clase_riesgo3; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="clase_riesgo4" for="clase_riesgo4" class="hasTooltip required" title="" data-original-title="<strong>Clase Riesgo IV</strong><br />Digite la tarifa de la clase de riesgo IV">
                    Clase de Riesgo IV
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $clase_riesgo4 = $this->variable[0]->clase_riesgo4;
                    ?>
                    <input type="text" name="clase_riesgo4" id="clase_riesgo4" value="<?php echo $clase_riesgo4; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="clase_riesgo5" for="clase_riesgo5" class="hasTooltip required" title="" data-original-title="<strong>Clase Riesgo V</strong><br />Digite la tarifa de la clase de riesgo V">
                    Clase de Riesgo V
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $clase_riesgo5 = $this->variable[0]->clase_riesgo5;
                    ?>
                    <input type="text" name="clase_riesgo5" id="clase_riesgo5" value="<?php echo $clase_riesgo5; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="rangos_fondo1" for="rangos_fondo1" class="hasTooltip required" title="" data-original-title="<strong>>= 4 a > 16</strong><br />Digite el porcentaje adicional">
                    >= 4 a > 16
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $rangos_fondo1 = $this->variable[0]->rangos_fondo1;
                    ?>
                    <input type="text" name="rangos_fondo1" id="rangos_fondo1" value="<?php echo $rangos_fondo1; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="rangos_fondo2" for="rangos_fondo2" class="hasTooltip required" title="" data-original-title="<strong>>= 16 a > 17</strong><br />Digite el porcentaje adicional">
                    >= 16 a > 17
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $rangos_fondo2 = $this->variable[0]->rangos_fondo2;
                    ?>
                    <input type="text" name="rangos_fondo2" id="rangos_fondo2" value="<?php echo $rangos_fondo2; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="rangos_fondo3" for="rangos_fondo3" class="hasTooltip required" title="" data-original-title="<strong>De 16 a 18</strong><br />Digite el porcentaje adicional">
                    De 17 a 18
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $rangos_fondo3 = $this->variable[0]->rangos_fondo3;
                    ?>
                    <input type="text" name="rangos_fondo3" id="rangos_fondo3" value="<?php echo $rangos_fondo3; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="rangos_fondo4" for="rangos_fondo4" class="hasTooltip required" title="" data-original-title="<strong>De 18 a 19</strong><br />Digite el porcentaje adicional">
                    De 18 a 19
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $rangos_fondo4 = $this->variable[0]->rangos_fondo4;
                    ?>
                    <input type="text" name="rangos_fondo4" id="rangos_fondo4" value="<?php echo $rangos_fondo4; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="rangos_fondo5" for="rangos_fondo5" class="hasTooltip required" title="" data-original-title="<strong>De 19 a 20</strong><br />Digite el porcentaje adicional">
                    De 19 a 20
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $rangos_fondo5 = $this->variable[0]->rangos_fondo5;
                    ?>
                    <input type="text" name="rangos_fondo5" id="rangos_fondo5" value="<?php echo $rangos_fondo5; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="rangos_fondo6" for="rangos_fondo6" class="hasTooltip required" title="" data-original-title="<strong>Superiores a 20</strong><br />Digite el porcentaje adicional">
                    Superiores a 20
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $rangos_fondo6 = $this->variable[0]->rangos_fondo6;
                    ?>
                    <input type="text" name="rangos_fondo6" id="rangos_fondo6" value="<?php echo $rangos_fondo6; ?>" class="required" size="30" required="required" aria-required="true">      
                    <span>%</span>          
                </div>
            </div>


            <div class="control-group">
                <div class="control-label">
                    <label id="salario_minimo" for="salario_minimo" class="hasTooltip required" title="" data-original-title="<strong>Salario Mínimo</strong><br />Digite el Salario Mínimo">
                    Salario Mínimo
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $salario_minimo = $this->variable[0]->salario_minimo;
                    ?>
                    <input type="text" name="salario_minimo" id="salario_minimo" value="<?php echo $salario_minimo; ?>" class="required" size="30" required="required" aria-required="true">      
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">
                    <label id="aux_transporte" for="aux_transporte" class="hasTooltip required" title="" data-original-title="<strong> Auxilio de Transporte</strong><br />Digite el  Auxilio de Transporte">
                    Auxilio de Transporte
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $aux_transporte = $this->variable[0]->aux_transporte;
                    ?>
                    <input type="text" name="aux_transporte" id="aux_transporte" value="<?php echo $aux_transporte; ?>" class="required" size="30" required="required" aria-required="true">      
                </div>
            </div>
          
            <div class="control-group">

                <div class="control-label">
                    <label for="festivos" class="hasTooltip required" title="" data-original-title="<strong>Festivos</strong><br />Seleccione los festivos del año">
                    Festivos
                    <span class="star">&nbsp;*</span>
                    </label>                        
                </div>
                <div class="controls">
                    <?php
                        $festivos = $this->variable[0]->festivos;
                    ?>
                    <textarea id="festivos" name="festivos" cols="20"><?php echo $festivos; ?></textarea> 
                    <input type="hidden" id="festivos_selected" value="<?php echo $festivos; ?>" />

                </div>

                <div id="full-year" class="box"></div>
            </div>
        </div>
    </div>

    <div>
        <input type="hidden" name="id" value="<?php echo $this->variable[0]->id ?>" />

        <input type="hidden" name="task" value="" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <input type="hidden" name="boxchecked" value="0" />
        <?php echo JHtml::_('form.token'); ?>
    </div>

    <script type="text/javascript">

        var dates = jQuery('#festivos_selected').val().split(',');


        if (jQuery('#system-message-container .alert-success').length){
            jQuery('#full-year').css('top', '10%');
        }

        jQuery('#full-year').multiDatesPicker({
            numberOfMonths: [3,4],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            addDates: dates,
            onSelect: function(date) {

                if (jQuery('#festivos').val() != '0'){
                    var val = jQuery('#festivos').val();

                    jQuery('#festivos').val(val+','+date);
                }else{
                    jQuery('#festivos').val(date);

                }
                
            },
        });
    </script>
</form>